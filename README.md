# About #

This project will be the backend service API for the entire system.

## How do I get set up? ##

* Summary of set up: I´m building the API from scratch without any framework.
* Configuration: Make sure you run directly on localhost without any port.
* Dependencies: Using only an ORM for database models: "propel/propel": "~2.0@dev" (you can check on [composer.json](https://bitbucket.org/maximilian02/cp_api/src/76ac1c3c76db7a90886abd6b7d71156c2c461db3/composer.json?at=master&fileviewer=file-view-default) file)
* Database configuration

### Steps and good to Go! ###

1. Clone this repo: https://maximilian02@bitbucket.org/maximilian02/cp_api.git
2. Move to the folder and run: composer install
3. Let´s get the party started

## Contribution guidelines ##

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ##

* Repo owner or admin