<?

namespace Api;

class Helper {
	// This static method will include all the resources needed according to the
	// entities passed as parameters
	public static function load($dependencies) {
		foreach ($dependencies as $d) {
		 	require_once 'model/classes/map/' . $d . 'TableMap.php';
			require_once 'model/classes/om/Base' . $d . '.php';
			require_once 'model/classes/om/Base' . $d . 'Peer.php';
			require_once 'model/classes/' . $d . '.php';
			require_once 'model/classes/' . $d . 'Peer.php';
		}
    }
}


?>