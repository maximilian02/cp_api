<?php

namespace Api;

require_once 'API.class.php';
require_once 'Helper.class.php';
require_once 'config/apiKeyModel.class.php';

// Propel set up
include 'propel_init.php';

// Including models will be used here
$models = ['Comercio'];
Helper::load($models);


class MyAPI extends API {
    private $apiKeyReceived;

    public function __construct($request, $origin) {
        parent::__construct($request);

        // Abstracted out for example
        $APIKey = new Config\APIKey();

        if (!$this->findApiKey()) {
            throw new \Exception('No API Key provided');
        } else if (!$APIKey->verifyKey($this->apiKeyReceived)) {
            throw new \Exception('Invalid API Key');
        }  else if (!$APIKey->verifyOrigin($origin)) {
            throw new \Exception('Origin not Allowed');
        }

        /*
        * Not going to use user token authentication
        * TODO: When implement this, remember to user properly the models
        * Code Snippet:
            else if (array_key_exists('token', $this->request) && !$User->get('token', $this->request['token'])) {
                throw new \Exception('Invalid User Token');
            }
        *
        */
    }

    /**
     * Get apiKey from headers
     * Description: Will get the apiKey field in the headers.
     * Note: This means that to call the api with all the methods (GET, POST, PUT or DELETE)
     *       we need to add the header with the proper apiKey parameter included.
     */

    private function findApiKey() {
        $headers = getallheaders();
        if(array_key_exists('apiKey', $headers)) {
            $this->apiKeyReceived = $headers['apiKey'];
            return true;
        }

        return false;
    }

    /**
     * Endpoint: Products
     */
    protected function products() {
        /*
            For: GET
            $this->args will contain all url parameters

            For: POST
            $this->request will contain all the data

            For: PUT
            $this->args will contain all url parameters
            $this->file will contain all the data

            For: DELETE
            $this->args will contain all the data
        */


        if ($this->method == 'GET') {
            $res = array( 'args' => $this->args, 'message' => 'USANDO GET');
            return $res;
        }

        if ($this->method == 'POST') {
            $res = array( 'request' => $this->request, 'message' => 'USANDO POST');


            $comercio = new \Comercio();
            $comercio->setNombre('Test');
            $comercio->save(); // save the record to the table

            return $res;
        }

        if ($this->method == 'PUT') {
            $res = array(
                          'args' => $this->args,
                          // Use decode to get the values from file
                          'data' => json_decode($this->file),
                          'message' => 'USANDO PUT'
                        );

            return $res;
        }

        if ($this->method == 'DELETE') {
             $res = array( 'args' => $this->args, 'message' => 'USANDO DELETE');
            return $res;
        }
    }
 }

 ?>
