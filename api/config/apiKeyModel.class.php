<?php

namespace Api\Config;


class APIKey {
    private $key;
    private $allowOrigins;

    public function __construct() {
        $this->key = 'vH97e0k3ual8r1MYza90l39BPzu2Sbo6';
        $this->allowOrigins = array(
                                      'localhost',
                                      'http://localhost:9000',
                                      'chrome-extension://fdmmgilgnpjigdojojpjoooidkmcomcm',
                                      'http://evil.com/'
                                    );
    }

    public function verifyKey($apiKey) {
        return $this->key == $apiKey;
    }

    public function verifyOrigin($origin) {
        return in_array($origin, $this->allowOrigins);
    }
}

?>
