<?php



/**
 * This class defines the structure of the 'categoria_productos' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.cp.map
 */
class CategoriaProductosTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'cp.map.CategoriaProductosTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('categoria_productos');
        $this->setPhpName('CategoriaProductos');
        $this->setClassname('CategoriaProductos');
        $this->setPackage('cp');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 50, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RProductoCategoriaproductos', 'RProductoCategoriaproductos', RelationMap::ONE_TO_MANY, array('id' => 'categoria_productos_id', ), 'CASCADE', 'CASCADE', 'RProductoCategoriaproductoss');
        $this->addRelation('RProveedorCategoriaproductos', 'RProveedorCategoriaproductos', RelationMap::ONE_TO_MANY, array('id' => 'caterogia_productos_id', ), 'CASCADE', 'CASCADE', 'RProveedorCategoriaproductoss');
    } // buildRelations()

} // CategoriaProductosTableMap
