<?php



/**
 * This class defines the structure of the 'comercio' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.cp.map
 */
class ComercioTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'cp.map.ComercioTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('comercio');
        $this->setPhpName('Comercio');
        $this->setClassname('Comercio');
        $this->setPackage('cp');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 50, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 50, null);
        $this->addColumn('razon_social', 'RazonSocial', 'VARCHAR', false, 50, null);
        $this->addColumn('cuit_cuil', 'CuitCuil', 'VARCHAR', false, 30, null);
        $this->addColumn('direccion', 'Direccion', 'VARCHAR', true, 100, null);
        $this->addColumn('telefonos', 'Telefonos', 'VARCHAR', false, 255, null);
        $this->addColumn('imagen_perfil', 'ImagenPerfil', 'VARCHAR', false, 255, null);
        $this->addColumn('estado', 'Estado', 'BOOLEAN', false, 1, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RComercioProducto', 'RComercioProducto', RelationMap::ONE_TO_MANY, array('id' => 'comercio_id', ), 'CASCADE', 'CASCADE', 'RComercioProductos');
        $this->addRelation('RComercioProveedor', 'RComercioProveedor', RelationMap::ONE_TO_MANY, array('id' => 'comercio_id', ), 'CASCADE', 'CASCADE', 'RComercioProveedors');
        $this->addRelation('RUsuarioComercio', 'RUsuarioComercio', RelationMap::ONE_TO_MANY, array('id' => 'comercio_id', ), 'CASCADE', 'CASCADE', 'RUsuarioComercios');
    } // buildRelations()

} // ComercioTableMap
