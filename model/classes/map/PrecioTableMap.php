<?php



/**
 * This class defines the structure of the 'precio' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.cp.map
 */
class PrecioTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'cp.map.PrecioTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('precio');
        $this->setPhpName('Precio');
        $this->setClassname('Precio');
        $this->setPackage('cp');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('precio', 'Precio', 'FLOAT', true, null, null);
        $this->addColumn('fecha_actualizacion', 'FechaActualizacion', 'DATE', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RListapreciosPrecio', 'RListapreciosPrecio', RelationMap::ONE_TO_MANY, array('id' => 'precio_id', ), 'CASCADE', 'CASCADE', 'RListapreciosPrecios');
        $this->addRelation('RProductoPrecio', 'RProductoPrecio', RelationMap::ONE_TO_MANY, array('id' => 'precio_id', ), 'CASCADE', 'CASCADE', 'RProductoPrecios');
    } // buildRelations()

} // PrecioTableMap
