<?php



/**
 * This class defines the structure of the 'producto' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.cp.map
 */
class ProductoTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'cp.map.ProductoTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('producto');
        $this->setPhpName('Producto');
        $this->setClassname('Producto');
        $this->setPackage('cp');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('nombre', 'Nombre', 'VARCHAR', true, 100, null);
        $this->addColumn('descripcion', 'Descripcion', 'VARCHAR', false, 255, null);
        $this->addColumn('codigo', 'Codigo', 'VARCHAR', false, 255, null);
        $this->addColumn('imagen', 'Imagen', 'VARCHAR', false, 255, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RComercioProducto', 'RComercioProducto', RelationMap::ONE_TO_MANY, array('id' => 'producto_id', ), 'CASCADE', 'CASCADE', 'RComercioProductos');
        $this->addRelation('RProductoCategoriaproductos', 'RProductoCategoriaproductos', RelationMap::ONE_TO_MANY, array('id' => 'producto_id', ), 'CASCADE', 'CASCADE', 'RProductoCategoriaproductoss');
        $this->addRelation('RProductoEmpaque', 'RProductoEmpaque', RelationMap::ONE_TO_MANY, array('id' => 'producto_id', ), 'CASCADE', 'CASCADE', 'RProductoEmpaques');
        $this->addRelation('RProductoEnvase', 'RProductoEnvase', RelationMap::ONE_TO_MANY, array('id' => 'producto_id', ), 'CASCADE', 'CASCADE', 'RProductoEnvases');
        $this->addRelation('RProductoMarca', 'RProductoMarca', RelationMap::ONE_TO_MANY, array('id' => 'producto_id', ), 'CASCADE', 'CASCADE', 'RProductoMarcas');
        $this->addRelation('RProductoPrecio', 'RProductoPrecio', RelationMap::ONE_TO_MANY, array('id' => 'producto_id', ), 'CASCADE', 'CASCADE', 'RProductoPrecios');
        $this->addRelation('RProveedorProducto', 'RProveedorProducto', RelationMap::ONE_TO_MANY, array('id' => 'producto_id', ), 'CASCADE', 'CASCADE', 'RProveedorProductos');
        $this->addRelation('RUsuarioProducto', 'RUsuarioProducto', RelationMap::ONE_TO_MANY, array('id' => 'producto_id', ), 'CASCADE', 'CASCADE', 'RUsuarioProductos');
    } // buildRelations()

} // ProductoTableMap
