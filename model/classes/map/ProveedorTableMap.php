<?php



/**
 * This class defines the structure of the 'proveedor' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.cp.map
 */
class ProveedorTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'cp.map.ProveedorTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('proveedor');
        $this->setPhpName('Proveedor');
        $this->setClassname('Proveedor');
        $this->setPackage('cp');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('email', 'Email', 'VARCHAR', true, 50, null);
        $this->addColumn('nombre_contacto', 'NombreContacto', 'VARCHAR', false, 30, null);
        $this->addColumn('preferencia_pago', 'PreferenciaPago', 'VARCHAR', false, 15, null);
        $this->addColumn('pedido_minimo', 'PedidoMinimo', 'INTEGER', false, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RComercioProveedor', 'RComercioProveedor', RelationMap::ONE_TO_MANY, array('id' => 'proveedor_id', ), 'CASCADE', 'CASCADE', 'RComercioProveedors');
        $this->addRelation('RProveedorCategoriaproductos', 'RProveedorCategoriaproductos', RelationMap::ONE_TO_MANY, array('id' => 'proveedor_id', ), 'CASCADE', 'CASCADE', 'RProveedorCategoriaproductoss');
        $this->addRelation('RProveedorListaprecios', 'RProveedorListaprecios', RelationMap::ONE_TO_MANY, array('id' => 'proveedor_id', ), 'CASCADE', 'CASCADE', 'RProveedorListaprecioss');
        $this->addRelation('RProveedorProducto', 'RProveedorProducto', RelationMap::ONE_TO_MANY, array('id' => 'proveedor_id', ), 'CASCADE', 'CASCADE', 'RProveedorProductos');
        $this->addRelation('RProveedorReputacion', 'RProveedorReputacion', RelationMap::ONE_TO_MANY, array('id' => 'proveedor_id', ), 'CASCADE', 'CASCADE', 'RProveedorReputacions');
        $this->addRelation('RProveedorZonaentrega', 'RProveedorZonaentrega', RelationMap::ONE_TO_MANY, array('id' => 'proveedor_id', ), 'CASCADE', 'CASCADE', 'RProveedorZonaentregas');
        $this->addRelation('RUsuarioProveedor', 'RUsuarioProveedor', RelationMap::ONE_TO_MANY, array('id' => 'proveedor_id', ), 'CASCADE', 'CASCADE', 'RUsuarioProveedors');
    } // buildRelations()

} // ProveedorTableMap
