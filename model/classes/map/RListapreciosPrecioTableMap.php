<?php



/**
 * This class defines the structure of the 'r_listaprecios_precio' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.cp.map
 */
class RListapreciosPrecioTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'cp.map.RListapreciosPrecioTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('r_listaprecios_precio');
        $this->setPhpName('RListapreciosPrecio');
        $this->setClassname('RListapreciosPrecio');
        $this->setPackage('cp');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('lista_precios_id', 'ListaPreciosId', 'INTEGER', 'lista_precios', 'id', true, null, null);
        $this->addForeignKey('precio_id', 'PrecioId', 'INTEGER', 'precio', 'id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ListaPrecios', 'ListaPrecios', RelationMap::MANY_TO_ONE, array('lista_precios_id' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('Precio', 'Precio', RelationMap::MANY_TO_ONE, array('precio_id' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // RListapreciosPrecioTableMap
