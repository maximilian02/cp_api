<?php



/**
 * This class defines the structure of the 'r_proveedor_listaprecios' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.cp.map
 */
class RProveedorListapreciosTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'cp.map.RProveedorListapreciosTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('r_proveedor_listaprecios');
        $this->setPhpName('RProveedorListaprecios');
        $this->setClassname('RProveedorListaprecios');
        $this->setPackage('cp');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('proveedor_id', 'ProveedorId', 'INTEGER', 'proveedor', 'id', true, null, null);
        $this->addForeignKey('lista_precios_id', 'ListaPreciosId', 'INTEGER', 'lista_precios', 'id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('ListaPrecios', 'ListaPrecios', RelationMap::MANY_TO_ONE, array('lista_precios_id' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('Proveedor', 'Proveedor', RelationMap::MANY_TO_ONE, array('proveedor_id' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // RProveedorListapreciosTableMap
