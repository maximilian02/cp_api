<?php



/**
 * This class defines the structure of the 'r_usuario_tipousuario' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.cp.map
 */
class RUsuarioTipousuarioTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'cp.map.RUsuarioTipousuarioTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('r_usuario_tipousuario');
        $this->setPhpName('RUsuarioTipousuario');
        $this->setClassname('RUsuarioTipousuario');
        $this->setPackage('cp');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addForeignKey('usuario_id', 'UsuarioId', 'INTEGER', 'usuario', 'id', true, null, null);
        $this->addForeignKey('tipo_usuario_id', 'TipoUsuarioId', 'INTEGER', 'tipo_usuario', 'id', true, null, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('TipoUsuario', 'TipoUsuario', RelationMap::MANY_TO_ONE, array('tipo_usuario_id' => 'id', ), 'CASCADE', 'CASCADE');
        $this->addRelation('Usuario', 'Usuario', RelationMap::MANY_TO_ONE, array('usuario_id' => 'id', ), 'CASCADE', 'CASCADE');
    } // buildRelations()

} // RUsuarioTipousuarioTableMap
