<?php



/**
 * This class defines the structure of the 'reputacion' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.cp.map
 */
class ReputacionTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'cp.map.ReputacionTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('reputacion');
        $this->setPhpName('Reputacion');
        $this->setClassname('Reputacion');
        $this->setPackage('cp');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('usuario_id', 'UsuarioId', 'INTEGER', true, null, null);
        $this->addColumn('puntos', 'Puntos', 'INTEGER', false, null, null);
        $this->addColumn('comentario', 'Comentario', 'VARCHAR', true, 100, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RProveedorReputacion', 'RProveedorReputacion', RelationMap::ONE_TO_MANY, array('id' => 'reputacion_id', ), 'CASCADE', 'CASCADE', 'RProveedorReputacions');
    } // buildRelations()

} // ReputacionTableMap
