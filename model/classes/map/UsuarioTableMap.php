<?php



/**
 * This class defines the structure of the 'usuario' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.cp.map
 */
class UsuarioTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'cp.map.UsuarioTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('usuario');
        $this->setPhpName('Usuario');
        $this->setClassname('Usuario');
        $this->setPackage('cp');
        $this->setUseIdGenerator(true);
        // columns
        $this->addPrimaryKey('id', 'Id', 'INTEGER', true, null, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 30, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 50, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('RUsuarioComercio', 'RUsuarioComercio', RelationMap::ONE_TO_MANY, array('id' => 'usuario_id', ), 'CASCADE', 'CASCADE', 'RUsuarioComercios');
        $this->addRelation('RUsuarioProducto', 'RUsuarioProducto', RelationMap::ONE_TO_MANY, array('id' => 'usuario_id', ), 'CASCADE', 'CASCADE', 'RUsuarioProductos');
        $this->addRelation('RUsuarioProveedor', 'RUsuarioProveedor', RelationMap::ONE_TO_MANY, array('id' => 'usuario_id', ), 'CASCADE', 'CASCADE', 'RUsuarioProveedors');
        $this->addRelation('RUsuarioTipousuario', 'RUsuarioTipousuario', RelationMap::ONE_TO_MANY, array('id' => 'usuario_id', ), 'CASCADE', 'CASCADE', 'RUsuarioTipousuarios');
    } // buildRelations()

} // UsuarioTableMap
