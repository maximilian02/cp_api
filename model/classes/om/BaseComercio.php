<?php


/**
 * Base class that represents a row from the 'comercio' table.
 *
 *
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseComercio extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'ComercioPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        ComercioPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the nombre field.
     * @var        string
     */
    protected $nombre;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the razon_social field.
     * @var        string
     */
    protected $razon_social;

    /**
     * The value for the cuit_cuil field.
     * @var        string
     */
    protected $cuit_cuil;

    /**
     * The value for the direccion field.
     * @var        string
     */
    protected $direccion;

    /**
     * The value for the telefonos field.
     * @var        string
     */
    protected $telefonos;

    /**
     * The value for the imagen_perfil field.
     * @var        string
     */
    protected $imagen_perfil;

    /**
     * The value for the estado field.
     * @var        boolean
     */
    protected $estado;

    /**
     * @var        PropelObjectCollection|RComercioProducto[] Collection to store aggregation of RComercioProducto objects.
     */
    protected $collRComercioProductos;
    protected $collRComercioProductosPartial;

    /**
     * @var        PropelObjectCollection|RComercioProveedor[] Collection to store aggregation of RComercioProveedor objects.
     */
    protected $collRComercioProveedors;
    protected $collRComercioProveedorsPartial;

    /**
     * @var        PropelObjectCollection|RUsuarioComercio[] Collection to store aggregation of RUsuarioComercio objects.
     */
    protected $collRUsuarioComercios;
    protected $collRUsuarioComerciosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rComercioProductosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rComercioProveedorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rUsuarioComerciosScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [nombre] column value.
     *
     * @return string
     */
    public function getNombre()
    {

        return $this->nombre;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [razon_social] column value.
     *
     * @return string
     */
    public function getRazonSocial()
    {

        return $this->razon_social;
    }

    /**
     * Get the [cuit_cuil] column value.
     *
     * @return string
     */
    public function getCuitCuil()
    {

        return $this->cuit_cuil;
    }

    /**
     * Get the [direccion] column value.
     *
     * @return string
     */
    public function getDireccion()
    {

        return $this->direccion;
    }

    /**
     * Get the [telefonos] column value.
     *
     * @return string
     */
    public function getTelefonos()
    {

        return $this->telefonos;
    }

    /**
     * Get the [imagen_perfil] column value.
     *
     * @return string
     */
    public function getImagenPerfil()
    {

        return $this->imagen_perfil;
    }

    /**
     * Get the [estado] column value.
     *
     * @return boolean
     */
    public function getEstado()
    {

        return $this->estado;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Comercio The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = ComercioPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [nombre] column.
     *
     * @param  string $v new value
     * @return Comercio The current object (for fluent API support)
     */
    public function setNombre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre !== $v) {
            $this->nombre = $v;
            $this->modifiedColumns[] = ComercioPeer::NOMBRE;
        }


        return $this;
    } // setNombre()

    /**
     * Set the value of [email] column.
     *
     * @param  string $v new value
     * @return Comercio The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = ComercioPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [razon_social] column.
     *
     * @param  string $v new value
     * @return Comercio The current object (for fluent API support)
     */
    public function setRazonSocial($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->razon_social !== $v) {
            $this->razon_social = $v;
            $this->modifiedColumns[] = ComercioPeer::RAZON_SOCIAL;
        }


        return $this;
    } // setRazonSocial()

    /**
     * Set the value of [cuit_cuil] column.
     *
     * @param  string $v new value
     * @return Comercio The current object (for fluent API support)
     */
    public function setCuitCuil($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->cuit_cuil !== $v) {
            $this->cuit_cuil = $v;
            $this->modifiedColumns[] = ComercioPeer::CUIT_CUIL;
        }


        return $this;
    } // setCuitCuil()

    /**
     * Set the value of [direccion] column.
     *
     * @param  string $v new value
     * @return Comercio The current object (for fluent API support)
     */
    public function setDireccion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->direccion !== $v) {
            $this->direccion = $v;
            $this->modifiedColumns[] = ComercioPeer::DIRECCION;
        }


        return $this;
    } // setDireccion()

    /**
     * Set the value of [telefonos] column.
     *
     * @param  string $v new value
     * @return Comercio The current object (for fluent API support)
     */
    public function setTelefonos($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->telefonos !== $v) {
            $this->telefonos = $v;
            $this->modifiedColumns[] = ComercioPeer::TELEFONOS;
        }


        return $this;
    } // setTelefonos()

    /**
     * Set the value of [imagen_perfil] column.
     *
     * @param  string $v new value
     * @return Comercio The current object (for fluent API support)
     */
    public function setImagenPerfil($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->imagen_perfil !== $v) {
            $this->imagen_perfil = $v;
            $this->modifiedColumns[] = ComercioPeer::IMAGEN_PERFIL;
        }


        return $this;
    } // setImagenPerfil()

    /**
     * Sets the value of the [estado] column.
     * Non-boolean arguments are converted using the following rules:
     *   * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *   * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     * Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     *
     * @param boolean|integer|string $v The new value
     * @return Comercio The current object (for fluent API support)
     */
    public function setEstado($v)
    {
        if ($v !== null) {
            if (is_string($v)) {
                $v = in_array(strtolower($v), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
            } else {
                $v = (boolean) $v;
            }
        }

        if ($this->estado !== $v) {
            $this->estado = $v;
            $this->modifiedColumns[] = ComercioPeer::ESTADO;
        }


        return $this;
    } // setEstado()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->nombre = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->email = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->razon_social = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->cuit_cuil = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->direccion = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->telefonos = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->imagen_perfil = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->estado = ($row[$startcol + 8] !== null) ? (boolean) $row[$startcol + 8] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 9; // 9 = ComercioPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Comercio object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ComercioPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ComercioPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collRComercioProductos = null;

            $this->collRComercioProveedors = null;

            $this->collRUsuarioComercios = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ComercioPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ComercioQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ComercioPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ComercioPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->rComercioProductosScheduledForDeletion !== null) {
                if (!$this->rComercioProductosScheduledForDeletion->isEmpty()) {
                    RComercioProductoQuery::create()
                        ->filterByPrimaryKeys($this->rComercioProductosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rComercioProductosScheduledForDeletion = null;
                }
            }

            if ($this->collRComercioProductos !== null) {
                foreach ($this->collRComercioProductos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rComercioProveedorsScheduledForDeletion !== null) {
                if (!$this->rComercioProveedorsScheduledForDeletion->isEmpty()) {
                    RComercioProveedorQuery::create()
                        ->filterByPrimaryKeys($this->rComercioProveedorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rComercioProveedorsScheduledForDeletion = null;
                }
            }

            if ($this->collRComercioProveedors !== null) {
                foreach ($this->collRComercioProveedors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rUsuarioComerciosScheduledForDeletion !== null) {
                if (!$this->rUsuarioComerciosScheduledForDeletion->isEmpty()) {
                    RUsuarioComercioQuery::create()
                        ->filterByPrimaryKeys($this->rUsuarioComerciosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rUsuarioComerciosScheduledForDeletion = null;
                }
            }

            if ($this->collRUsuarioComercios !== null) {
                foreach ($this->collRUsuarioComercios as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ComercioPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ComercioPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ComercioPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(ComercioPeer::NOMBRE)) {
            $modifiedColumns[':p' . $index++]  = '`nombre`';
        }
        if ($this->isColumnModified(ComercioPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(ComercioPeer::RAZON_SOCIAL)) {
            $modifiedColumns[':p' . $index++]  = '`razon_social`';
        }
        if ($this->isColumnModified(ComercioPeer::CUIT_CUIL)) {
            $modifiedColumns[':p' . $index++]  = '`cuit_cuil`';
        }
        if ($this->isColumnModified(ComercioPeer::DIRECCION)) {
            $modifiedColumns[':p' . $index++]  = '`direccion`';
        }
        if ($this->isColumnModified(ComercioPeer::TELEFONOS)) {
            $modifiedColumns[':p' . $index++]  = '`telefonos`';
        }
        if ($this->isColumnModified(ComercioPeer::IMAGEN_PERFIL)) {
            $modifiedColumns[':p' . $index++]  = '`imagen_perfil`';
        }
        if ($this->isColumnModified(ComercioPeer::ESTADO)) {
            $modifiedColumns[':p' . $index++]  = '`estado`';
        }

        $sql = sprintf(
            'INSERT INTO `comercio` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`nombre`':
                        $stmt->bindValue($identifier, $this->nombre, PDO::PARAM_STR);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`razon_social`':
                        $stmt->bindValue($identifier, $this->razon_social, PDO::PARAM_STR);
                        break;
                    case '`cuit_cuil`':
                        $stmt->bindValue($identifier, $this->cuit_cuil, PDO::PARAM_STR);
                        break;
                    case '`direccion`':
                        $stmt->bindValue($identifier, $this->direccion, PDO::PARAM_STR);
                        break;
                    case '`telefonos`':
                        $stmt->bindValue($identifier, $this->telefonos, PDO::PARAM_STR);
                        break;
                    case '`imagen_perfil`':
                        $stmt->bindValue($identifier, $this->imagen_perfil, PDO::PARAM_STR);
                        break;
                    case '`estado`':
                        $stmt->bindValue($identifier, (int) $this->estado, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = ComercioPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collRComercioProductos !== null) {
                    foreach ($this->collRComercioProductos as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRComercioProveedors !== null) {
                    foreach ($this->collRComercioProveedors as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRUsuarioComercios !== null) {
                    foreach ($this->collRUsuarioComercios as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ComercioPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNombre();
                break;
            case 2:
                return $this->getEmail();
                break;
            case 3:
                return $this->getRazonSocial();
                break;
            case 4:
                return $this->getCuitCuil();
                break;
            case 5:
                return $this->getDireccion();
                break;
            case 6:
                return $this->getTelefonos();
                break;
            case 7:
                return $this->getImagenPerfil();
                break;
            case 8:
                return $this->getEstado();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Comercio'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Comercio'][$this->getPrimaryKey()] = true;
        $keys = ComercioPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNombre(),
            $keys[2] => $this->getEmail(),
            $keys[3] => $this->getRazonSocial(),
            $keys[4] => $this->getCuitCuil(),
            $keys[5] => $this->getDireccion(),
            $keys[6] => $this->getTelefonos(),
            $keys[7] => $this->getImagenPerfil(),
            $keys[8] => $this->getEstado(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collRComercioProductos) {
                $result['RComercioProductos'] = $this->collRComercioProductos->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRComercioProveedors) {
                $result['RComercioProveedors'] = $this->collRComercioProveedors->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRUsuarioComercios) {
                $result['RUsuarioComercios'] = $this->collRUsuarioComercios->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ComercioPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNombre($value);
                break;
            case 2:
                $this->setEmail($value);
                break;
            case 3:
                $this->setRazonSocial($value);
                break;
            case 4:
                $this->setCuitCuil($value);
                break;
            case 5:
                $this->setDireccion($value);
                break;
            case 6:
                $this->setTelefonos($value);
                break;
            case 7:
                $this->setImagenPerfil($value);
                break;
            case 8:
                $this->setEstado($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ComercioPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setEmail($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setRazonSocial($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setCuitCuil($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setDireccion($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setTelefonos($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setImagenPerfil($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setEstado($arr[$keys[8]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ComercioPeer::DATABASE_NAME);

        if ($this->isColumnModified(ComercioPeer::ID)) $criteria->add(ComercioPeer::ID, $this->id);
        if ($this->isColumnModified(ComercioPeer::NOMBRE)) $criteria->add(ComercioPeer::NOMBRE, $this->nombre);
        if ($this->isColumnModified(ComercioPeer::EMAIL)) $criteria->add(ComercioPeer::EMAIL, $this->email);
        if ($this->isColumnModified(ComercioPeer::RAZON_SOCIAL)) $criteria->add(ComercioPeer::RAZON_SOCIAL, $this->razon_social);
        if ($this->isColumnModified(ComercioPeer::CUIT_CUIL)) $criteria->add(ComercioPeer::CUIT_CUIL, $this->cuit_cuil);
        if ($this->isColumnModified(ComercioPeer::DIRECCION)) $criteria->add(ComercioPeer::DIRECCION, $this->direccion);
        if ($this->isColumnModified(ComercioPeer::TELEFONOS)) $criteria->add(ComercioPeer::TELEFONOS, $this->telefonos);
        if ($this->isColumnModified(ComercioPeer::IMAGEN_PERFIL)) $criteria->add(ComercioPeer::IMAGEN_PERFIL, $this->imagen_perfil);
        if ($this->isColumnModified(ComercioPeer::ESTADO)) $criteria->add(ComercioPeer::ESTADO, $this->estado);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ComercioPeer::DATABASE_NAME);
        $criteria->add(ComercioPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Comercio (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNombre($this->getNombre());
        $copyObj->setEmail($this->getEmail());
        $copyObj->setRazonSocial($this->getRazonSocial());
        $copyObj->setCuitCuil($this->getCuitCuil());
        $copyObj->setDireccion($this->getDireccion());
        $copyObj->setTelefonos($this->getTelefonos());
        $copyObj->setImagenPerfil($this->getImagenPerfil());
        $copyObj->setEstado($this->getEstado());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getRComercioProductos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRComercioProducto($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRComercioProveedors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRComercioProveedor($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRUsuarioComercios() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRUsuarioComercio($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Comercio Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ComercioPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ComercioPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('RComercioProducto' == $relationName) {
            $this->initRComercioProductos();
        }
        if ('RComercioProveedor' == $relationName) {
            $this->initRComercioProveedors();
        }
        if ('RUsuarioComercio' == $relationName) {
            $this->initRUsuarioComercios();
        }
    }

    /**
     * Clears out the collRComercioProductos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Comercio The current object (for fluent API support)
     * @see        addRComercioProductos()
     */
    public function clearRComercioProductos()
    {
        $this->collRComercioProductos = null; // important to set this to null since that means it is uninitialized
        $this->collRComercioProductosPartial = null;

        return $this;
    }

    /**
     * reset is the collRComercioProductos collection loaded partially
     *
     * @return void
     */
    public function resetPartialRComercioProductos($v = true)
    {
        $this->collRComercioProductosPartial = $v;
    }

    /**
     * Initializes the collRComercioProductos collection.
     *
     * By default this just sets the collRComercioProductos collection to an empty array (like clearcollRComercioProductos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRComercioProductos($overrideExisting = true)
    {
        if (null !== $this->collRComercioProductos && !$overrideExisting) {
            return;
        }
        $this->collRComercioProductos = new PropelObjectCollection();
        $this->collRComercioProductos->setModel('RComercioProducto');
    }

    /**
     * Gets an array of RComercioProducto objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Comercio is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RComercioProducto[] List of RComercioProducto objects
     * @throws PropelException
     */
    public function getRComercioProductos($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRComercioProductosPartial && !$this->isNew();
        if (null === $this->collRComercioProductos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRComercioProductos) {
                // return empty collection
                $this->initRComercioProductos();
            } else {
                $collRComercioProductos = RComercioProductoQuery::create(null, $criteria)
                    ->filterByComercio($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRComercioProductosPartial && count($collRComercioProductos)) {
                      $this->initRComercioProductos(false);

                      foreach ($collRComercioProductos as $obj) {
                        if (false == $this->collRComercioProductos->contains($obj)) {
                          $this->collRComercioProductos->append($obj);
                        }
                      }

                      $this->collRComercioProductosPartial = true;
                    }

                    $collRComercioProductos->getInternalIterator()->rewind();

                    return $collRComercioProductos;
                }

                if ($partial && $this->collRComercioProductos) {
                    foreach ($this->collRComercioProductos as $obj) {
                        if ($obj->isNew()) {
                            $collRComercioProductos[] = $obj;
                        }
                    }
                }

                $this->collRComercioProductos = $collRComercioProductos;
                $this->collRComercioProductosPartial = false;
            }
        }

        return $this->collRComercioProductos;
    }

    /**
     * Sets a collection of RComercioProducto objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rComercioProductos A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Comercio The current object (for fluent API support)
     */
    public function setRComercioProductos(PropelCollection $rComercioProductos, PropelPDO $con = null)
    {
        $rComercioProductosToDelete = $this->getRComercioProductos(new Criteria(), $con)->diff($rComercioProductos);


        $this->rComercioProductosScheduledForDeletion = $rComercioProductosToDelete;

        foreach ($rComercioProductosToDelete as $rComercioProductoRemoved) {
            $rComercioProductoRemoved->setComercio(null);
        }

        $this->collRComercioProductos = null;
        foreach ($rComercioProductos as $rComercioProducto) {
            $this->addRComercioProducto($rComercioProducto);
        }

        $this->collRComercioProductos = $rComercioProductos;
        $this->collRComercioProductosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RComercioProducto objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RComercioProducto objects.
     * @throws PropelException
     */
    public function countRComercioProductos(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRComercioProductosPartial && !$this->isNew();
        if (null === $this->collRComercioProductos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRComercioProductos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRComercioProductos());
            }
            $query = RComercioProductoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByComercio($this)
                ->count($con);
        }

        return count($this->collRComercioProductos);
    }

    /**
     * Method called to associate a RComercioProducto object to this object
     * through the RComercioProducto foreign key attribute.
     *
     * @param    RComercioProducto $l RComercioProducto
     * @return Comercio The current object (for fluent API support)
     */
    public function addRComercioProducto(RComercioProducto $l)
    {
        if ($this->collRComercioProductos === null) {
            $this->initRComercioProductos();
            $this->collRComercioProductosPartial = true;
        }

        if (!in_array($l, $this->collRComercioProductos->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRComercioProducto($l);

            if ($this->rComercioProductosScheduledForDeletion and $this->rComercioProductosScheduledForDeletion->contains($l)) {
                $this->rComercioProductosScheduledForDeletion->remove($this->rComercioProductosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RComercioProducto $rComercioProducto The rComercioProducto object to add.
     */
    protected function doAddRComercioProducto($rComercioProducto)
    {
        $this->collRComercioProductos[]= $rComercioProducto;
        $rComercioProducto->setComercio($this);
    }

    /**
     * @param	RComercioProducto $rComercioProducto The rComercioProducto object to remove.
     * @return Comercio The current object (for fluent API support)
     */
    public function removeRComercioProducto($rComercioProducto)
    {
        if ($this->getRComercioProductos()->contains($rComercioProducto)) {
            $this->collRComercioProductos->remove($this->collRComercioProductos->search($rComercioProducto));
            if (null === $this->rComercioProductosScheduledForDeletion) {
                $this->rComercioProductosScheduledForDeletion = clone $this->collRComercioProductos;
                $this->rComercioProductosScheduledForDeletion->clear();
            }
            $this->rComercioProductosScheduledForDeletion[]= clone $rComercioProducto;
            $rComercioProducto->setComercio(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Comercio is new, it will return
     * an empty collection; or if this Comercio has previously
     * been saved, it will retrieve related RComercioProductos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Comercio.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RComercioProducto[] List of RComercioProducto objects
     */
    public function getRComercioProductosJoinProducto($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RComercioProductoQuery::create(null, $criteria);
        $query->joinWith('Producto', $join_behavior);

        return $this->getRComercioProductos($query, $con);
    }

    /**
     * Clears out the collRComercioProveedors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Comercio The current object (for fluent API support)
     * @see        addRComercioProveedors()
     */
    public function clearRComercioProveedors()
    {
        $this->collRComercioProveedors = null; // important to set this to null since that means it is uninitialized
        $this->collRComercioProveedorsPartial = null;

        return $this;
    }

    /**
     * reset is the collRComercioProveedors collection loaded partially
     *
     * @return void
     */
    public function resetPartialRComercioProveedors($v = true)
    {
        $this->collRComercioProveedorsPartial = $v;
    }

    /**
     * Initializes the collRComercioProveedors collection.
     *
     * By default this just sets the collRComercioProveedors collection to an empty array (like clearcollRComercioProveedors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRComercioProveedors($overrideExisting = true)
    {
        if (null !== $this->collRComercioProveedors && !$overrideExisting) {
            return;
        }
        $this->collRComercioProveedors = new PropelObjectCollection();
        $this->collRComercioProveedors->setModel('RComercioProveedor');
    }

    /**
     * Gets an array of RComercioProveedor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Comercio is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RComercioProveedor[] List of RComercioProveedor objects
     * @throws PropelException
     */
    public function getRComercioProveedors($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRComercioProveedorsPartial && !$this->isNew();
        if (null === $this->collRComercioProveedors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRComercioProveedors) {
                // return empty collection
                $this->initRComercioProveedors();
            } else {
                $collRComercioProveedors = RComercioProveedorQuery::create(null, $criteria)
                    ->filterByComercio($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRComercioProveedorsPartial && count($collRComercioProveedors)) {
                      $this->initRComercioProveedors(false);

                      foreach ($collRComercioProveedors as $obj) {
                        if (false == $this->collRComercioProveedors->contains($obj)) {
                          $this->collRComercioProveedors->append($obj);
                        }
                      }

                      $this->collRComercioProveedorsPartial = true;
                    }

                    $collRComercioProveedors->getInternalIterator()->rewind();

                    return $collRComercioProveedors;
                }

                if ($partial && $this->collRComercioProveedors) {
                    foreach ($this->collRComercioProveedors as $obj) {
                        if ($obj->isNew()) {
                            $collRComercioProveedors[] = $obj;
                        }
                    }
                }

                $this->collRComercioProveedors = $collRComercioProveedors;
                $this->collRComercioProveedorsPartial = false;
            }
        }

        return $this->collRComercioProveedors;
    }

    /**
     * Sets a collection of RComercioProveedor objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rComercioProveedors A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Comercio The current object (for fluent API support)
     */
    public function setRComercioProveedors(PropelCollection $rComercioProveedors, PropelPDO $con = null)
    {
        $rComercioProveedorsToDelete = $this->getRComercioProveedors(new Criteria(), $con)->diff($rComercioProveedors);


        $this->rComercioProveedorsScheduledForDeletion = $rComercioProveedorsToDelete;

        foreach ($rComercioProveedorsToDelete as $rComercioProveedorRemoved) {
            $rComercioProveedorRemoved->setComercio(null);
        }

        $this->collRComercioProveedors = null;
        foreach ($rComercioProveedors as $rComercioProveedor) {
            $this->addRComercioProveedor($rComercioProveedor);
        }

        $this->collRComercioProveedors = $rComercioProveedors;
        $this->collRComercioProveedorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RComercioProveedor objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RComercioProveedor objects.
     * @throws PropelException
     */
    public function countRComercioProveedors(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRComercioProveedorsPartial && !$this->isNew();
        if (null === $this->collRComercioProveedors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRComercioProveedors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRComercioProveedors());
            }
            $query = RComercioProveedorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByComercio($this)
                ->count($con);
        }

        return count($this->collRComercioProveedors);
    }

    /**
     * Method called to associate a RComercioProveedor object to this object
     * through the RComercioProveedor foreign key attribute.
     *
     * @param    RComercioProveedor $l RComercioProveedor
     * @return Comercio The current object (for fluent API support)
     */
    public function addRComercioProveedor(RComercioProveedor $l)
    {
        if ($this->collRComercioProveedors === null) {
            $this->initRComercioProveedors();
            $this->collRComercioProveedorsPartial = true;
        }

        if (!in_array($l, $this->collRComercioProveedors->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRComercioProveedor($l);

            if ($this->rComercioProveedorsScheduledForDeletion and $this->rComercioProveedorsScheduledForDeletion->contains($l)) {
                $this->rComercioProveedorsScheduledForDeletion->remove($this->rComercioProveedorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RComercioProveedor $rComercioProveedor The rComercioProveedor object to add.
     */
    protected function doAddRComercioProveedor($rComercioProveedor)
    {
        $this->collRComercioProveedors[]= $rComercioProveedor;
        $rComercioProveedor->setComercio($this);
    }

    /**
     * @param	RComercioProveedor $rComercioProveedor The rComercioProveedor object to remove.
     * @return Comercio The current object (for fluent API support)
     */
    public function removeRComercioProveedor($rComercioProveedor)
    {
        if ($this->getRComercioProveedors()->contains($rComercioProveedor)) {
            $this->collRComercioProveedors->remove($this->collRComercioProveedors->search($rComercioProveedor));
            if (null === $this->rComercioProveedorsScheduledForDeletion) {
                $this->rComercioProveedorsScheduledForDeletion = clone $this->collRComercioProveedors;
                $this->rComercioProveedorsScheduledForDeletion->clear();
            }
            $this->rComercioProveedorsScheduledForDeletion[]= clone $rComercioProveedor;
            $rComercioProveedor->setComercio(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Comercio is new, it will return
     * an empty collection; or if this Comercio has previously
     * been saved, it will retrieve related RComercioProveedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Comercio.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RComercioProveedor[] List of RComercioProveedor objects
     */
    public function getRComercioProveedorsJoinProveedor($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RComercioProveedorQuery::create(null, $criteria);
        $query->joinWith('Proveedor', $join_behavior);

        return $this->getRComercioProveedors($query, $con);
    }

    /**
     * Clears out the collRUsuarioComercios collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Comercio The current object (for fluent API support)
     * @see        addRUsuarioComercios()
     */
    public function clearRUsuarioComercios()
    {
        $this->collRUsuarioComercios = null; // important to set this to null since that means it is uninitialized
        $this->collRUsuarioComerciosPartial = null;

        return $this;
    }

    /**
     * reset is the collRUsuarioComercios collection loaded partially
     *
     * @return void
     */
    public function resetPartialRUsuarioComercios($v = true)
    {
        $this->collRUsuarioComerciosPartial = $v;
    }

    /**
     * Initializes the collRUsuarioComercios collection.
     *
     * By default this just sets the collRUsuarioComercios collection to an empty array (like clearcollRUsuarioComercios());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRUsuarioComercios($overrideExisting = true)
    {
        if (null !== $this->collRUsuarioComercios && !$overrideExisting) {
            return;
        }
        $this->collRUsuarioComercios = new PropelObjectCollection();
        $this->collRUsuarioComercios->setModel('RUsuarioComercio');
    }

    /**
     * Gets an array of RUsuarioComercio objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Comercio is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RUsuarioComercio[] List of RUsuarioComercio objects
     * @throws PropelException
     */
    public function getRUsuarioComercios($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioComerciosPartial && !$this->isNew();
        if (null === $this->collRUsuarioComercios || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioComercios) {
                // return empty collection
                $this->initRUsuarioComercios();
            } else {
                $collRUsuarioComercios = RUsuarioComercioQuery::create(null, $criteria)
                    ->filterByComercio($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRUsuarioComerciosPartial && count($collRUsuarioComercios)) {
                      $this->initRUsuarioComercios(false);

                      foreach ($collRUsuarioComercios as $obj) {
                        if (false == $this->collRUsuarioComercios->contains($obj)) {
                          $this->collRUsuarioComercios->append($obj);
                        }
                      }

                      $this->collRUsuarioComerciosPartial = true;
                    }

                    $collRUsuarioComercios->getInternalIterator()->rewind();

                    return $collRUsuarioComercios;
                }

                if ($partial && $this->collRUsuarioComercios) {
                    foreach ($this->collRUsuarioComercios as $obj) {
                        if ($obj->isNew()) {
                            $collRUsuarioComercios[] = $obj;
                        }
                    }
                }

                $this->collRUsuarioComercios = $collRUsuarioComercios;
                $this->collRUsuarioComerciosPartial = false;
            }
        }

        return $this->collRUsuarioComercios;
    }

    /**
     * Sets a collection of RUsuarioComercio objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rUsuarioComercios A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Comercio The current object (for fluent API support)
     */
    public function setRUsuarioComercios(PropelCollection $rUsuarioComercios, PropelPDO $con = null)
    {
        $rUsuarioComerciosToDelete = $this->getRUsuarioComercios(new Criteria(), $con)->diff($rUsuarioComercios);


        $this->rUsuarioComerciosScheduledForDeletion = $rUsuarioComerciosToDelete;

        foreach ($rUsuarioComerciosToDelete as $rUsuarioComercioRemoved) {
            $rUsuarioComercioRemoved->setComercio(null);
        }

        $this->collRUsuarioComercios = null;
        foreach ($rUsuarioComercios as $rUsuarioComercio) {
            $this->addRUsuarioComercio($rUsuarioComercio);
        }

        $this->collRUsuarioComercios = $rUsuarioComercios;
        $this->collRUsuarioComerciosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RUsuarioComercio objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RUsuarioComercio objects.
     * @throws PropelException
     */
    public function countRUsuarioComercios(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioComerciosPartial && !$this->isNew();
        if (null === $this->collRUsuarioComercios || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioComercios) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRUsuarioComercios());
            }
            $query = RUsuarioComercioQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByComercio($this)
                ->count($con);
        }

        return count($this->collRUsuarioComercios);
    }

    /**
     * Method called to associate a RUsuarioComercio object to this object
     * through the RUsuarioComercio foreign key attribute.
     *
     * @param    RUsuarioComercio $l RUsuarioComercio
     * @return Comercio The current object (for fluent API support)
     */
    public function addRUsuarioComercio(RUsuarioComercio $l)
    {
        if ($this->collRUsuarioComercios === null) {
            $this->initRUsuarioComercios();
            $this->collRUsuarioComerciosPartial = true;
        }

        if (!in_array($l, $this->collRUsuarioComercios->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRUsuarioComercio($l);

            if ($this->rUsuarioComerciosScheduledForDeletion and $this->rUsuarioComerciosScheduledForDeletion->contains($l)) {
                $this->rUsuarioComerciosScheduledForDeletion->remove($this->rUsuarioComerciosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RUsuarioComercio $rUsuarioComercio The rUsuarioComercio object to add.
     */
    protected function doAddRUsuarioComercio($rUsuarioComercio)
    {
        $this->collRUsuarioComercios[]= $rUsuarioComercio;
        $rUsuarioComercio->setComercio($this);
    }

    /**
     * @param	RUsuarioComercio $rUsuarioComercio The rUsuarioComercio object to remove.
     * @return Comercio The current object (for fluent API support)
     */
    public function removeRUsuarioComercio($rUsuarioComercio)
    {
        if ($this->getRUsuarioComercios()->contains($rUsuarioComercio)) {
            $this->collRUsuarioComercios->remove($this->collRUsuarioComercios->search($rUsuarioComercio));
            if (null === $this->rUsuarioComerciosScheduledForDeletion) {
                $this->rUsuarioComerciosScheduledForDeletion = clone $this->collRUsuarioComercios;
                $this->rUsuarioComerciosScheduledForDeletion->clear();
            }
            $this->rUsuarioComerciosScheduledForDeletion[]= clone $rUsuarioComercio;
            $rUsuarioComercio->setComercio(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Comercio is new, it will return
     * an empty collection; or if this Comercio has previously
     * been saved, it will retrieve related RUsuarioComercios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Comercio.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RUsuarioComercio[] List of RUsuarioComercio objects
     */
    public function getRUsuarioComerciosJoinUsuario($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RUsuarioComercioQuery::create(null, $criteria);
        $query->joinWith('Usuario', $join_behavior);

        return $this->getRUsuarioComercios($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->nombre = null;
        $this->email = null;
        $this->razon_social = null;
        $this->cuit_cuil = null;
        $this->direccion = null;
        $this->telefonos = null;
        $this->imagen_perfil = null;
        $this->estado = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collRComercioProductos) {
                foreach ($this->collRComercioProductos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRComercioProveedors) {
                foreach ($this->collRComercioProveedors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRUsuarioComercios) {
                foreach ($this->collRUsuarioComercios as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collRComercioProductos instanceof PropelCollection) {
            $this->collRComercioProductos->clearIterator();
        }
        $this->collRComercioProductos = null;
        if ($this->collRComercioProveedors instanceof PropelCollection) {
            $this->collRComercioProveedors->clearIterator();
        }
        $this->collRComercioProveedors = null;
        if ($this->collRUsuarioComercios instanceof PropelCollection) {
            $this->collRUsuarioComercios->clearIterator();
        }
        $this->collRUsuarioComercios = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ComercioPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
