<?php


/**
 * Base class that represents a query for the 'comercio' table.
 *
 *
 *
 * @method ComercioQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ComercioQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method ComercioQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method ComercioQuery orderByRazonSocial($order = Criteria::ASC) Order by the razon_social column
 * @method ComercioQuery orderByCuitCuil($order = Criteria::ASC) Order by the cuit_cuil column
 * @method ComercioQuery orderByDireccion($order = Criteria::ASC) Order by the direccion column
 * @method ComercioQuery orderByTelefonos($order = Criteria::ASC) Order by the telefonos column
 * @method ComercioQuery orderByImagenPerfil($order = Criteria::ASC) Order by the imagen_perfil column
 * @method ComercioQuery orderByEstado($order = Criteria::ASC) Order by the estado column
 *
 * @method ComercioQuery groupById() Group by the id column
 * @method ComercioQuery groupByNombre() Group by the nombre column
 * @method ComercioQuery groupByEmail() Group by the email column
 * @method ComercioQuery groupByRazonSocial() Group by the razon_social column
 * @method ComercioQuery groupByCuitCuil() Group by the cuit_cuil column
 * @method ComercioQuery groupByDireccion() Group by the direccion column
 * @method ComercioQuery groupByTelefonos() Group by the telefonos column
 * @method ComercioQuery groupByImagenPerfil() Group by the imagen_perfil column
 * @method ComercioQuery groupByEstado() Group by the estado column
 *
 * @method ComercioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ComercioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ComercioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ComercioQuery leftJoinRComercioProducto($relationAlias = null) Adds a LEFT JOIN clause to the query using the RComercioProducto relation
 * @method ComercioQuery rightJoinRComercioProducto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RComercioProducto relation
 * @method ComercioQuery innerJoinRComercioProducto($relationAlias = null) Adds a INNER JOIN clause to the query using the RComercioProducto relation
 *
 * @method ComercioQuery leftJoinRComercioProveedor($relationAlias = null) Adds a LEFT JOIN clause to the query using the RComercioProveedor relation
 * @method ComercioQuery rightJoinRComercioProveedor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RComercioProveedor relation
 * @method ComercioQuery innerJoinRComercioProveedor($relationAlias = null) Adds a INNER JOIN clause to the query using the RComercioProveedor relation
 *
 * @method ComercioQuery leftJoinRUsuarioComercio($relationAlias = null) Adds a LEFT JOIN clause to the query using the RUsuarioComercio relation
 * @method ComercioQuery rightJoinRUsuarioComercio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RUsuarioComercio relation
 * @method ComercioQuery innerJoinRUsuarioComercio($relationAlias = null) Adds a INNER JOIN clause to the query using the RUsuarioComercio relation
 *
 * @method Comercio findOne(PropelPDO $con = null) Return the first Comercio matching the query
 * @method Comercio findOneOrCreate(PropelPDO $con = null) Return the first Comercio matching the query, or a new Comercio object populated from the query conditions when no match is found
 *
 * @method Comercio findOneByNombre(string $nombre) Return the first Comercio filtered by the nombre column
 * @method Comercio findOneByEmail(string $email) Return the first Comercio filtered by the email column
 * @method Comercio findOneByRazonSocial(string $razon_social) Return the first Comercio filtered by the razon_social column
 * @method Comercio findOneByCuitCuil(string $cuit_cuil) Return the first Comercio filtered by the cuit_cuil column
 * @method Comercio findOneByDireccion(string $direccion) Return the first Comercio filtered by the direccion column
 * @method Comercio findOneByTelefonos(string $telefonos) Return the first Comercio filtered by the telefonos column
 * @method Comercio findOneByImagenPerfil(string $imagen_perfil) Return the first Comercio filtered by the imagen_perfil column
 * @method Comercio findOneByEstado(boolean $estado) Return the first Comercio filtered by the estado column
 *
 * @method array findById(int $id) Return Comercio objects filtered by the id column
 * @method array findByNombre(string $nombre) Return Comercio objects filtered by the nombre column
 * @method array findByEmail(string $email) Return Comercio objects filtered by the email column
 * @method array findByRazonSocial(string $razon_social) Return Comercio objects filtered by the razon_social column
 * @method array findByCuitCuil(string $cuit_cuil) Return Comercio objects filtered by the cuit_cuil column
 * @method array findByDireccion(string $direccion) Return Comercio objects filtered by the direccion column
 * @method array findByTelefonos(string $telefonos) Return Comercio objects filtered by the telefonos column
 * @method array findByImagenPerfil(string $imagen_perfil) Return Comercio objects filtered by the imagen_perfil column
 * @method array findByEstado(boolean $estado) Return Comercio objects filtered by the estado column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseComercioQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseComercioQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'Comercio';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ComercioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ComercioQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ComercioQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ComercioQuery) {
            return $criteria;
        }
        $query = new ComercioQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Comercio|Comercio[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ComercioPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ComercioPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Comercio A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Comercio A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `nombre`, `email`, `razon_social`, `cuit_cuil`, `direccion`, `telefonos`, `imagen_perfil`, `estado` FROM `comercio` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Comercio();
            $obj->hydrate($row);
            ComercioPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Comercio|Comercio[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Comercio[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ComercioPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ComercioPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ComercioPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ComercioPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ComercioPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%'); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nombre)) {
                $nombre = str_replace('*', '%', $nombre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComercioPeer::NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComercioPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the razon_social column
     *
     * Example usage:
     * <code>
     * $query->filterByRazonSocial('fooValue');   // WHERE razon_social = 'fooValue'
     * $query->filterByRazonSocial('%fooValue%'); // WHERE razon_social LIKE '%fooValue%'
     * </code>
     *
     * @param     string $razonSocial The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function filterByRazonSocial($razonSocial = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($razonSocial)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $razonSocial)) {
                $razonSocial = str_replace('*', '%', $razonSocial);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComercioPeer::RAZON_SOCIAL, $razonSocial, $comparison);
    }

    /**
     * Filter the query on the cuit_cuil column
     *
     * Example usage:
     * <code>
     * $query->filterByCuitCuil('fooValue');   // WHERE cuit_cuil = 'fooValue'
     * $query->filterByCuitCuil('%fooValue%'); // WHERE cuit_cuil LIKE '%fooValue%'
     * </code>
     *
     * @param     string $cuitCuil The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function filterByCuitCuil($cuitCuil = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($cuitCuil)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $cuitCuil)) {
                $cuitCuil = str_replace('*', '%', $cuitCuil);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComercioPeer::CUIT_CUIL, $cuitCuil, $comparison);
    }

    /**
     * Filter the query on the direccion column
     *
     * Example usage:
     * <code>
     * $query->filterByDireccion('fooValue');   // WHERE direccion = 'fooValue'
     * $query->filterByDireccion('%fooValue%'); // WHERE direccion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $direccion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function filterByDireccion($direccion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($direccion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $direccion)) {
                $direccion = str_replace('*', '%', $direccion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComercioPeer::DIRECCION, $direccion, $comparison);
    }

    /**
     * Filter the query on the telefonos column
     *
     * Example usage:
     * <code>
     * $query->filterByTelefonos('fooValue');   // WHERE telefonos = 'fooValue'
     * $query->filterByTelefonos('%fooValue%'); // WHERE telefonos LIKE '%fooValue%'
     * </code>
     *
     * @param     string $telefonos The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function filterByTelefonos($telefonos = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($telefonos)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $telefonos)) {
                $telefonos = str_replace('*', '%', $telefonos);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComercioPeer::TELEFONOS, $telefonos, $comparison);
    }

    /**
     * Filter the query on the imagen_perfil column
     *
     * Example usage:
     * <code>
     * $query->filterByImagenPerfil('fooValue');   // WHERE imagen_perfil = 'fooValue'
     * $query->filterByImagenPerfil('%fooValue%'); // WHERE imagen_perfil LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imagenPerfil The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function filterByImagenPerfil($imagenPerfil = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imagenPerfil)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $imagenPerfil)) {
                $imagenPerfil = str_replace('*', '%', $imagenPerfil);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ComercioPeer::IMAGEN_PERFIL, $imagenPerfil, $comparison);
    }

    /**
     * Filter the query on the estado column
     *
     * Example usage:
     * <code>
     * $query->filterByEstado(true); // WHERE estado = true
     * $query->filterByEstado('yes'); // WHERE estado = true
     * </code>
     *
     * @param     boolean|string $estado The value to use as filter.
     *              Non-boolean arguments are converted using the following rules:
     *                * 1, '1', 'true',  'on',  and 'yes' are converted to boolean true
     *                * 0, '0', 'false', 'off', and 'no'  are converted to boolean false
     *              Check on string values is case insensitive (so 'FaLsE' is seen as 'false').
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function filterByEstado($estado = null, $comparison = null)
    {
        if (is_string($estado)) {
            $estado = in_array(strtolower($estado), array('false', 'off', '-', 'no', 'n', '0', '')) ? false : true;
        }

        return $this->addUsingAlias(ComercioPeer::ESTADO, $estado, $comparison);
    }

    /**
     * Filter the query by a related RComercioProducto object
     *
     * @param   RComercioProducto|PropelObjectCollection $rComercioProducto  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ComercioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRComercioProducto($rComercioProducto, $comparison = null)
    {
        if ($rComercioProducto instanceof RComercioProducto) {
            return $this
                ->addUsingAlias(ComercioPeer::ID, $rComercioProducto->getComercioId(), $comparison);
        } elseif ($rComercioProducto instanceof PropelObjectCollection) {
            return $this
                ->useRComercioProductoQuery()
                ->filterByPrimaryKeys($rComercioProducto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRComercioProducto() only accepts arguments of type RComercioProducto or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RComercioProducto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function joinRComercioProducto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RComercioProducto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RComercioProducto');
        }

        return $this;
    }

    /**
     * Use the RComercioProducto relation RComercioProducto object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RComercioProductoQuery A secondary query class using the current class as primary query
     */
    public function useRComercioProductoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRComercioProducto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RComercioProducto', 'RComercioProductoQuery');
    }

    /**
     * Filter the query by a related RComercioProveedor object
     *
     * @param   RComercioProveedor|PropelObjectCollection $rComercioProveedor  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ComercioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRComercioProveedor($rComercioProveedor, $comparison = null)
    {
        if ($rComercioProveedor instanceof RComercioProveedor) {
            return $this
                ->addUsingAlias(ComercioPeer::ID, $rComercioProveedor->getComercioId(), $comparison);
        } elseif ($rComercioProveedor instanceof PropelObjectCollection) {
            return $this
                ->useRComercioProveedorQuery()
                ->filterByPrimaryKeys($rComercioProveedor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRComercioProveedor() only accepts arguments of type RComercioProveedor or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RComercioProveedor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function joinRComercioProveedor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RComercioProveedor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RComercioProveedor');
        }

        return $this;
    }

    /**
     * Use the RComercioProveedor relation RComercioProveedor object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RComercioProveedorQuery A secondary query class using the current class as primary query
     */
    public function useRComercioProveedorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRComercioProveedor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RComercioProveedor', 'RComercioProveedorQuery');
    }

    /**
     * Filter the query by a related RUsuarioComercio object
     *
     * @param   RUsuarioComercio|PropelObjectCollection $rUsuarioComercio  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ComercioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRUsuarioComercio($rUsuarioComercio, $comparison = null)
    {
        if ($rUsuarioComercio instanceof RUsuarioComercio) {
            return $this
                ->addUsingAlias(ComercioPeer::ID, $rUsuarioComercio->getComercioId(), $comparison);
        } elseif ($rUsuarioComercio instanceof PropelObjectCollection) {
            return $this
                ->useRUsuarioComercioQuery()
                ->filterByPrimaryKeys($rUsuarioComercio->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRUsuarioComercio() only accepts arguments of type RUsuarioComercio or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RUsuarioComercio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function joinRUsuarioComercio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RUsuarioComercio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RUsuarioComercio');
        }

        return $this;
    }

    /**
     * Use the RUsuarioComercio relation RUsuarioComercio object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RUsuarioComercioQuery A secondary query class using the current class as primary query
     */
    public function useRUsuarioComercioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRUsuarioComercio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RUsuarioComercio', 'RUsuarioComercioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Comercio $comercio Object to remove from the list of results
     *
     * @return ComercioQuery The current query, for fluid interface
     */
    public function prune($comercio = null)
    {
        if ($comercio) {
            $this->addUsingAlias(ComercioPeer::ID, $comercio->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
