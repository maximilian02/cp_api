<?php


/**
 * Base class that represents a row from the 'lista_precios' table.
 *
 *
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseListaPrecios extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'ListaPreciosPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        ListaPreciosPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the nombre field.
     * @var        string
     */
    protected $nombre;

    /**
     * The value for the fecha_creacion field.
     * @var        string
     */
    protected $fecha_creacion;

    /**
     * The value for the fecha_vencimiento field.
     * @var        string
     */
    protected $fecha_vencimiento;

    /**
     * @var        PropelObjectCollection|RListapreciosPrecio[] Collection to store aggregation of RListapreciosPrecio objects.
     */
    protected $collRListapreciosPrecios;
    protected $collRListapreciosPreciosPartial;

    /**
     * @var        PropelObjectCollection|RProveedorListaprecios[] Collection to store aggregation of RProveedorListaprecios objects.
     */
    protected $collRProveedorListaprecioss;
    protected $collRProveedorListapreciossPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rListapreciosPreciosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rProveedorListapreciossScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [nombre] column value.
     *
     * @return string
     */
    public function getNombre()
    {

        return $this->nombre;
    }

    /**
     * Get the [optionally formatted] temporal [fecha_creacion] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaCreacion($format = '%x')
    {
        if ($this->fecha_creacion === null) {
            return null;
        }

        if ($this->fecha_creacion === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_creacion);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_creacion, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Get the [optionally formatted] temporal [fecha_vencimiento] column value.
     *
     *
     * @param string $format The date/time format string (either date()-style or strftime()-style).
     *				 If format is null, then the raw DateTime object will be returned.
     * @return mixed Formatted date/time value as string or DateTime object (if format is null), null if column is null, and 0 if column value is 0000-00-00
     * @throws PropelException - if unable to parse/validate the date/time value.
     */
    public function getFechaVencimiento($format = '%x')
    {
        if ($this->fecha_vencimiento === null) {
            return null;
        }

        if ($this->fecha_vencimiento === '0000-00-00') {
            // while technically this is not a default value of null,
            // this seems to be closest in meaning.
            return null;
        }

        try {
            $dt = new DateTime($this->fecha_vencimiento);
        } catch (Exception $x) {
            throw new PropelException("Internally stored date/time/timestamp value could not be converted to DateTime: " . var_export($this->fecha_vencimiento, true), $x);
        }

        if ($format === null) {
            // Because propel.useDateTimeClass is true, we return a DateTime object.
            return $dt;
        }

        if (strpos($format, '%') !== false) {
            return strftime($format, $dt->format('U'));
        }

        return $dt->format($format);

    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return ListaPrecios The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = ListaPreciosPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [nombre] column.
     *
     * @param  string $v new value
     * @return ListaPrecios The current object (for fluent API support)
     */
    public function setNombre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre !== $v) {
            $this->nombre = $v;
            $this->modifiedColumns[] = ListaPreciosPeer::NOMBRE;
        }


        return $this;
    } // setNombre()

    /**
     * Sets the value of [fecha_creacion] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return ListaPrecios The current object (for fluent API support)
     */
    public function setFechaCreacion($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_creacion !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_creacion !== null && $tmpDt = new DateTime($this->fecha_creacion)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_creacion = $newDateAsString;
                $this->modifiedColumns[] = ListaPreciosPeer::FECHA_CREACION;
            }
        } // if either are not null


        return $this;
    } // setFechaCreacion()

    /**
     * Sets the value of [fecha_vencimiento] column to a normalized version of the date/time value specified.
     *
     * @param mixed $v string, integer (timestamp), or DateTime value.
     *               Empty strings are treated as null.
     * @return ListaPrecios The current object (for fluent API support)
     */
    public function setFechaVencimiento($v)
    {
        $dt = PropelDateTime::newInstance($v, null, 'DateTime');
        if ($this->fecha_vencimiento !== null || $dt !== null) {
            $currentDateAsString = ($this->fecha_vencimiento !== null && $tmpDt = new DateTime($this->fecha_vencimiento)) ? $tmpDt->format('Y-m-d') : null;
            $newDateAsString = $dt ? $dt->format('Y-m-d') : null;
            if ($currentDateAsString !== $newDateAsString) {
                $this->fecha_vencimiento = $newDateAsString;
                $this->modifiedColumns[] = ListaPreciosPeer::FECHA_VENCIMIENTO;
            }
        } // if either are not null


        return $this;
    } // setFechaVencimiento()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->nombre = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->fecha_creacion = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->fecha_vencimiento = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 4; // 4 = ListaPreciosPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating ListaPrecios object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ListaPreciosPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ListaPreciosPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collRListapreciosPrecios = null;

            $this->collRProveedorListaprecioss = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ListaPreciosPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ListaPreciosQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ListaPreciosPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ListaPreciosPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->rListapreciosPreciosScheduledForDeletion !== null) {
                if (!$this->rListapreciosPreciosScheduledForDeletion->isEmpty()) {
                    RListapreciosPrecioQuery::create()
                        ->filterByPrimaryKeys($this->rListapreciosPreciosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rListapreciosPreciosScheduledForDeletion = null;
                }
            }

            if ($this->collRListapreciosPrecios !== null) {
                foreach ($this->collRListapreciosPrecios as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rProveedorListapreciossScheduledForDeletion !== null) {
                if (!$this->rProveedorListapreciossScheduledForDeletion->isEmpty()) {
                    RProveedorListapreciosQuery::create()
                        ->filterByPrimaryKeys($this->rProveedorListapreciossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rProveedorListapreciossScheduledForDeletion = null;
                }
            }

            if ($this->collRProveedorListaprecioss !== null) {
                foreach ($this->collRProveedorListaprecioss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ListaPreciosPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ListaPreciosPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ListaPreciosPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(ListaPreciosPeer::NOMBRE)) {
            $modifiedColumns[':p' . $index++]  = '`nombre`';
        }
        if ($this->isColumnModified(ListaPreciosPeer::FECHA_CREACION)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_creacion`';
        }
        if ($this->isColumnModified(ListaPreciosPeer::FECHA_VENCIMIENTO)) {
            $modifiedColumns[':p' . $index++]  = '`fecha_vencimiento`';
        }

        $sql = sprintf(
            'INSERT INTO `lista_precios` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`nombre`':
                        $stmt->bindValue($identifier, $this->nombre, PDO::PARAM_STR);
                        break;
                    case '`fecha_creacion`':
                        $stmt->bindValue($identifier, $this->fecha_creacion, PDO::PARAM_STR);
                        break;
                    case '`fecha_vencimiento`':
                        $stmt->bindValue($identifier, $this->fecha_vencimiento, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = ListaPreciosPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collRListapreciosPrecios !== null) {
                    foreach ($this->collRListapreciosPrecios as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRProveedorListaprecioss !== null) {
                    foreach ($this->collRProveedorListaprecioss as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ListaPreciosPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNombre();
                break;
            case 2:
                return $this->getFechaCreacion();
                break;
            case 3:
                return $this->getFechaVencimiento();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['ListaPrecios'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['ListaPrecios'][$this->getPrimaryKey()] = true;
        $keys = ListaPreciosPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNombre(),
            $keys[2] => $this->getFechaCreacion(),
            $keys[3] => $this->getFechaVencimiento(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collRListapreciosPrecios) {
                $result['RListapreciosPrecios'] = $this->collRListapreciosPrecios->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRProveedorListaprecioss) {
                $result['RProveedorListaprecioss'] = $this->collRProveedorListaprecioss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ListaPreciosPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNombre($value);
                break;
            case 2:
                $this->setFechaCreacion($value);
                break;
            case 3:
                $this->setFechaVencimiento($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ListaPreciosPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setFechaCreacion($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setFechaVencimiento($arr[$keys[3]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ListaPreciosPeer::DATABASE_NAME);

        if ($this->isColumnModified(ListaPreciosPeer::ID)) $criteria->add(ListaPreciosPeer::ID, $this->id);
        if ($this->isColumnModified(ListaPreciosPeer::NOMBRE)) $criteria->add(ListaPreciosPeer::NOMBRE, $this->nombre);
        if ($this->isColumnModified(ListaPreciosPeer::FECHA_CREACION)) $criteria->add(ListaPreciosPeer::FECHA_CREACION, $this->fecha_creacion);
        if ($this->isColumnModified(ListaPreciosPeer::FECHA_VENCIMIENTO)) $criteria->add(ListaPreciosPeer::FECHA_VENCIMIENTO, $this->fecha_vencimiento);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ListaPreciosPeer::DATABASE_NAME);
        $criteria->add(ListaPreciosPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of ListaPrecios (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNombre($this->getNombre());
        $copyObj->setFechaCreacion($this->getFechaCreacion());
        $copyObj->setFechaVencimiento($this->getFechaVencimiento());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getRListapreciosPrecios() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRListapreciosPrecio($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRProveedorListaprecioss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRProveedorListaprecios($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return ListaPrecios Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ListaPreciosPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ListaPreciosPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('RListapreciosPrecio' == $relationName) {
            $this->initRListapreciosPrecios();
        }
        if ('RProveedorListaprecios' == $relationName) {
            $this->initRProveedorListaprecioss();
        }
    }

    /**
     * Clears out the collRListapreciosPrecios collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return ListaPrecios The current object (for fluent API support)
     * @see        addRListapreciosPrecios()
     */
    public function clearRListapreciosPrecios()
    {
        $this->collRListapreciosPrecios = null; // important to set this to null since that means it is uninitialized
        $this->collRListapreciosPreciosPartial = null;

        return $this;
    }

    /**
     * reset is the collRListapreciosPrecios collection loaded partially
     *
     * @return void
     */
    public function resetPartialRListapreciosPrecios($v = true)
    {
        $this->collRListapreciosPreciosPartial = $v;
    }

    /**
     * Initializes the collRListapreciosPrecios collection.
     *
     * By default this just sets the collRListapreciosPrecios collection to an empty array (like clearcollRListapreciosPrecios());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRListapreciosPrecios($overrideExisting = true)
    {
        if (null !== $this->collRListapreciosPrecios && !$overrideExisting) {
            return;
        }
        $this->collRListapreciosPrecios = new PropelObjectCollection();
        $this->collRListapreciosPrecios->setModel('RListapreciosPrecio');
    }

    /**
     * Gets an array of RListapreciosPrecio objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ListaPrecios is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RListapreciosPrecio[] List of RListapreciosPrecio objects
     * @throws PropelException
     */
    public function getRListapreciosPrecios($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRListapreciosPreciosPartial && !$this->isNew();
        if (null === $this->collRListapreciosPrecios || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRListapreciosPrecios) {
                // return empty collection
                $this->initRListapreciosPrecios();
            } else {
                $collRListapreciosPrecios = RListapreciosPrecioQuery::create(null, $criteria)
                    ->filterByListaPrecios($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRListapreciosPreciosPartial && count($collRListapreciosPrecios)) {
                      $this->initRListapreciosPrecios(false);

                      foreach ($collRListapreciosPrecios as $obj) {
                        if (false == $this->collRListapreciosPrecios->contains($obj)) {
                          $this->collRListapreciosPrecios->append($obj);
                        }
                      }

                      $this->collRListapreciosPreciosPartial = true;
                    }

                    $collRListapreciosPrecios->getInternalIterator()->rewind();

                    return $collRListapreciosPrecios;
                }

                if ($partial && $this->collRListapreciosPrecios) {
                    foreach ($this->collRListapreciosPrecios as $obj) {
                        if ($obj->isNew()) {
                            $collRListapreciosPrecios[] = $obj;
                        }
                    }
                }

                $this->collRListapreciosPrecios = $collRListapreciosPrecios;
                $this->collRListapreciosPreciosPartial = false;
            }
        }

        return $this->collRListapreciosPrecios;
    }

    /**
     * Sets a collection of RListapreciosPrecio objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rListapreciosPrecios A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return ListaPrecios The current object (for fluent API support)
     */
    public function setRListapreciosPrecios(PropelCollection $rListapreciosPrecios, PropelPDO $con = null)
    {
        $rListapreciosPreciosToDelete = $this->getRListapreciosPrecios(new Criteria(), $con)->diff($rListapreciosPrecios);


        $this->rListapreciosPreciosScheduledForDeletion = $rListapreciosPreciosToDelete;

        foreach ($rListapreciosPreciosToDelete as $rListapreciosPrecioRemoved) {
            $rListapreciosPrecioRemoved->setListaPrecios(null);
        }

        $this->collRListapreciosPrecios = null;
        foreach ($rListapreciosPrecios as $rListapreciosPrecio) {
            $this->addRListapreciosPrecio($rListapreciosPrecio);
        }

        $this->collRListapreciosPrecios = $rListapreciosPrecios;
        $this->collRListapreciosPreciosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RListapreciosPrecio objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RListapreciosPrecio objects.
     * @throws PropelException
     */
    public function countRListapreciosPrecios(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRListapreciosPreciosPartial && !$this->isNew();
        if (null === $this->collRListapreciosPrecios || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRListapreciosPrecios) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRListapreciosPrecios());
            }
            $query = RListapreciosPrecioQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByListaPrecios($this)
                ->count($con);
        }

        return count($this->collRListapreciosPrecios);
    }

    /**
     * Method called to associate a RListapreciosPrecio object to this object
     * through the RListapreciosPrecio foreign key attribute.
     *
     * @param    RListapreciosPrecio $l RListapreciosPrecio
     * @return ListaPrecios The current object (for fluent API support)
     */
    public function addRListapreciosPrecio(RListapreciosPrecio $l)
    {
        if ($this->collRListapreciosPrecios === null) {
            $this->initRListapreciosPrecios();
            $this->collRListapreciosPreciosPartial = true;
        }

        if (!in_array($l, $this->collRListapreciosPrecios->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRListapreciosPrecio($l);

            if ($this->rListapreciosPreciosScheduledForDeletion and $this->rListapreciosPreciosScheduledForDeletion->contains($l)) {
                $this->rListapreciosPreciosScheduledForDeletion->remove($this->rListapreciosPreciosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RListapreciosPrecio $rListapreciosPrecio The rListapreciosPrecio object to add.
     */
    protected function doAddRListapreciosPrecio($rListapreciosPrecio)
    {
        $this->collRListapreciosPrecios[]= $rListapreciosPrecio;
        $rListapreciosPrecio->setListaPrecios($this);
    }

    /**
     * @param	RListapreciosPrecio $rListapreciosPrecio The rListapreciosPrecio object to remove.
     * @return ListaPrecios The current object (for fluent API support)
     */
    public function removeRListapreciosPrecio($rListapreciosPrecio)
    {
        if ($this->getRListapreciosPrecios()->contains($rListapreciosPrecio)) {
            $this->collRListapreciosPrecios->remove($this->collRListapreciosPrecios->search($rListapreciosPrecio));
            if (null === $this->rListapreciosPreciosScheduledForDeletion) {
                $this->rListapreciosPreciosScheduledForDeletion = clone $this->collRListapreciosPrecios;
                $this->rListapreciosPreciosScheduledForDeletion->clear();
            }
            $this->rListapreciosPreciosScheduledForDeletion[]= clone $rListapreciosPrecio;
            $rListapreciosPrecio->setListaPrecios(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ListaPrecios is new, it will return
     * an empty collection; or if this ListaPrecios has previously
     * been saved, it will retrieve related RListapreciosPrecios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ListaPrecios.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RListapreciosPrecio[] List of RListapreciosPrecio objects
     */
    public function getRListapreciosPreciosJoinPrecio($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RListapreciosPrecioQuery::create(null, $criteria);
        $query->joinWith('Precio', $join_behavior);

        return $this->getRListapreciosPrecios($query, $con);
    }

    /**
     * Clears out the collRProveedorListaprecioss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return ListaPrecios The current object (for fluent API support)
     * @see        addRProveedorListaprecioss()
     */
    public function clearRProveedorListaprecioss()
    {
        $this->collRProveedorListaprecioss = null; // important to set this to null since that means it is uninitialized
        $this->collRProveedorListapreciossPartial = null;

        return $this;
    }

    /**
     * reset is the collRProveedorListaprecioss collection loaded partially
     *
     * @return void
     */
    public function resetPartialRProveedorListaprecioss($v = true)
    {
        $this->collRProveedorListapreciossPartial = $v;
    }

    /**
     * Initializes the collRProveedorListaprecioss collection.
     *
     * By default this just sets the collRProveedorListaprecioss collection to an empty array (like clearcollRProveedorListaprecioss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRProveedorListaprecioss($overrideExisting = true)
    {
        if (null !== $this->collRProveedorListaprecioss && !$overrideExisting) {
            return;
        }
        $this->collRProveedorListaprecioss = new PropelObjectCollection();
        $this->collRProveedorListaprecioss->setModel('RProveedorListaprecios');
    }

    /**
     * Gets an array of RProveedorListaprecios objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this ListaPrecios is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RProveedorListaprecios[] List of RProveedorListaprecios objects
     * @throws PropelException
     */
    public function getRProveedorListaprecioss($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorListapreciossPartial && !$this->isNew();
        if (null === $this->collRProveedorListaprecioss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRProveedorListaprecioss) {
                // return empty collection
                $this->initRProveedorListaprecioss();
            } else {
                $collRProveedorListaprecioss = RProveedorListapreciosQuery::create(null, $criteria)
                    ->filterByListaPrecios($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRProveedorListapreciossPartial && count($collRProveedorListaprecioss)) {
                      $this->initRProveedorListaprecioss(false);

                      foreach ($collRProveedorListaprecioss as $obj) {
                        if (false == $this->collRProveedorListaprecioss->contains($obj)) {
                          $this->collRProveedorListaprecioss->append($obj);
                        }
                      }

                      $this->collRProveedorListapreciossPartial = true;
                    }

                    $collRProveedorListaprecioss->getInternalIterator()->rewind();

                    return $collRProveedorListaprecioss;
                }

                if ($partial && $this->collRProveedorListaprecioss) {
                    foreach ($this->collRProveedorListaprecioss as $obj) {
                        if ($obj->isNew()) {
                            $collRProveedorListaprecioss[] = $obj;
                        }
                    }
                }

                $this->collRProveedorListaprecioss = $collRProveedorListaprecioss;
                $this->collRProveedorListapreciossPartial = false;
            }
        }

        return $this->collRProveedorListaprecioss;
    }

    /**
     * Sets a collection of RProveedorListaprecios objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rProveedorListaprecioss A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return ListaPrecios The current object (for fluent API support)
     */
    public function setRProveedorListaprecioss(PropelCollection $rProveedorListaprecioss, PropelPDO $con = null)
    {
        $rProveedorListapreciossToDelete = $this->getRProveedorListaprecioss(new Criteria(), $con)->diff($rProveedorListaprecioss);


        $this->rProveedorListapreciossScheduledForDeletion = $rProveedorListapreciossToDelete;

        foreach ($rProveedorListapreciossToDelete as $rProveedorListapreciosRemoved) {
            $rProveedorListapreciosRemoved->setListaPrecios(null);
        }

        $this->collRProveedorListaprecioss = null;
        foreach ($rProveedorListaprecioss as $rProveedorListaprecios) {
            $this->addRProveedorListaprecios($rProveedorListaprecios);
        }

        $this->collRProveedorListaprecioss = $rProveedorListaprecioss;
        $this->collRProveedorListapreciossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RProveedorListaprecios objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RProveedorListaprecios objects.
     * @throws PropelException
     */
    public function countRProveedorListaprecioss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorListapreciossPartial && !$this->isNew();
        if (null === $this->collRProveedorListaprecioss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRProveedorListaprecioss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRProveedorListaprecioss());
            }
            $query = RProveedorListapreciosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByListaPrecios($this)
                ->count($con);
        }

        return count($this->collRProveedorListaprecioss);
    }

    /**
     * Method called to associate a RProveedorListaprecios object to this object
     * through the RProveedorListaprecios foreign key attribute.
     *
     * @param    RProveedorListaprecios $l RProveedorListaprecios
     * @return ListaPrecios The current object (for fluent API support)
     */
    public function addRProveedorListaprecios(RProveedorListaprecios $l)
    {
        if ($this->collRProveedorListaprecioss === null) {
            $this->initRProveedorListaprecioss();
            $this->collRProveedorListapreciossPartial = true;
        }

        if (!in_array($l, $this->collRProveedorListaprecioss->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRProveedorListaprecios($l);

            if ($this->rProveedorListapreciossScheduledForDeletion and $this->rProveedorListapreciossScheduledForDeletion->contains($l)) {
                $this->rProveedorListapreciossScheduledForDeletion->remove($this->rProveedorListapreciossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RProveedorListaprecios $rProveedorListaprecios The rProveedorListaprecios object to add.
     */
    protected function doAddRProveedorListaprecios($rProveedorListaprecios)
    {
        $this->collRProveedorListaprecioss[]= $rProveedorListaprecios;
        $rProveedorListaprecios->setListaPrecios($this);
    }

    /**
     * @param	RProveedorListaprecios $rProveedorListaprecios The rProveedorListaprecios object to remove.
     * @return ListaPrecios The current object (for fluent API support)
     */
    public function removeRProveedorListaprecios($rProveedorListaprecios)
    {
        if ($this->getRProveedorListaprecioss()->contains($rProveedorListaprecios)) {
            $this->collRProveedorListaprecioss->remove($this->collRProveedorListaprecioss->search($rProveedorListaprecios));
            if (null === $this->rProveedorListapreciossScheduledForDeletion) {
                $this->rProveedorListapreciossScheduledForDeletion = clone $this->collRProveedorListaprecioss;
                $this->rProveedorListapreciossScheduledForDeletion->clear();
            }
            $this->rProveedorListapreciossScheduledForDeletion[]= clone $rProveedorListaprecios;
            $rProveedorListaprecios->setListaPrecios(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this ListaPrecios is new, it will return
     * an empty collection; or if this ListaPrecios has previously
     * been saved, it will retrieve related RProveedorListaprecioss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in ListaPrecios.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RProveedorListaprecios[] List of RProveedorListaprecios objects
     */
    public function getRProveedorListapreciossJoinProveedor($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RProveedorListapreciosQuery::create(null, $criteria);
        $query->joinWith('Proveedor', $join_behavior);

        return $this->getRProveedorListaprecioss($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->nombre = null;
        $this->fecha_creacion = null;
        $this->fecha_vencimiento = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collRListapreciosPrecios) {
                foreach ($this->collRListapreciosPrecios as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRProveedorListaprecioss) {
                foreach ($this->collRProveedorListaprecioss as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collRListapreciosPrecios instanceof PropelCollection) {
            $this->collRListapreciosPrecios->clearIterator();
        }
        $this->collRListapreciosPrecios = null;
        if ($this->collRProveedorListaprecioss instanceof PropelCollection) {
            $this->collRProveedorListaprecioss->clearIterator();
        }
        $this->collRProveedorListaprecioss = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ListaPreciosPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
