<?php


/**
 * Base class that represents a query for the 'lista_precios' table.
 *
 *
 *
 * @method ListaPreciosQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ListaPreciosQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method ListaPreciosQuery orderByFechaCreacion($order = Criteria::ASC) Order by the fecha_creacion column
 * @method ListaPreciosQuery orderByFechaVencimiento($order = Criteria::ASC) Order by the fecha_vencimiento column
 *
 * @method ListaPreciosQuery groupById() Group by the id column
 * @method ListaPreciosQuery groupByNombre() Group by the nombre column
 * @method ListaPreciosQuery groupByFechaCreacion() Group by the fecha_creacion column
 * @method ListaPreciosQuery groupByFechaVencimiento() Group by the fecha_vencimiento column
 *
 * @method ListaPreciosQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ListaPreciosQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ListaPreciosQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ListaPreciosQuery leftJoinRListapreciosPrecio($relationAlias = null) Adds a LEFT JOIN clause to the query using the RListapreciosPrecio relation
 * @method ListaPreciosQuery rightJoinRListapreciosPrecio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RListapreciosPrecio relation
 * @method ListaPreciosQuery innerJoinRListapreciosPrecio($relationAlias = null) Adds a INNER JOIN clause to the query using the RListapreciosPrecio relation
 *
 * @method ListaPreciosQuery leftJoinRProveedorListaprecios($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProveedorListaprecios relation
 * @method ListaPreciosQuery rightJoinRProveedorListaprecios($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProveedorListaprecios relation
 * @method ListaPreciosQuery innerJoinRProveedorListaprecios($relationAlias = null) Adds a INNER JOIN clause to the query using the RProveedorListaprecios relation
 *
 * @method ListaPrecios findOne(PropelPDO $con = null) Return the first ListaPrecios matching the query
 * @method ListaPrecios findOneOrCreate(PropelPDO $con = null) Return the first ListaPrecios matching the query, or a new ListaPrecios object populated from the query conditions when no match is found
 *
 * @method ListaPrecios findOneByNombre(string $nombre) Return the first ListaPrecios filtered by the nombre column
 * @method ListaPrecios findOneByFechaCreacion(string $fecha_creacion) Return the first ListaPrecios filtered by the fecha_creacion column
 * @method ListaPrecios findOneByFechaVencimiento(string $fecha_vencimiento) Return the first ListaPrecios filtered by the fecha_vencimiento column
 *
 * @method array findById(int $id) Return ListaPrecios objects filtered by the id column
 * @method array findByNombre(string $nombre) Return ListaPrecios objects filtered by the nombre column
 * @method array findByFechaCreacion(string $fecha_creacion) Return ListaPrecios objects filtered by the fecha_creacion column
 * @method array findByFechaVencimiento(string $fecha_vencimiento) Return ListaPrecios objects filtered by the fecha_vencimiento column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseListaPreciosQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseListaPreciosQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'ListaPrecios';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ListaPreciosQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ListaPreciosQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ListaPreciosQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ListaPreciosQuery) {
            return $criteria;
        }
        $query = new ListaPreciosQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ListaPrecios|ListaPrecios[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ListaPreciosPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ListaPreciosPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ListaPrecios A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ListaPrecios A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `nombre`, `fecha_creacion`, `fecha_vencimiento` FROM `lista_precios` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ListaPrecios();
            $obj->hydrate($row);
            ListaPreciosPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ListaPrecios|ListaPrecios[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ListaPrecios[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ListaPreciosQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ListaPreciosPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ListaPreciosQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ListaPreciosPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ListaPreciosQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ListaPreciosPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ListaPreciosPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ListaPreciosPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%'); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ListaPreciosQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nombre)) {
                $nombre = str_replace('*', '%', $nombre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ListaPreciosPeer::NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the fecha_creacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaCreacion('2011-03-14'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion('now'); // WHERE fecha_creacion = '2011-03-14'
     * $query->filterByFechaCreacion(array('max' => 'yesterday')); // WHERE fecha_creacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaCreacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ListaPreciosQuery The current query, for fluid interface
     */
    public function filterByFechaCreacion($fechaCreacion = null, $comparison = null)
    {
        if (is_array($fechaCreacion)) {
            $useMinMax = false;
            if (isset($fechaCreacion['min'])) {
                $this->addUsingAlias(ListaPreciosPeer::FECHA_CREACION, $fechaCreacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaCreacion['max'])) {
                $this->addUsingAlias(ListaPreciosPeer::FECHA_CREACION, $fechaCreacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ListaPreciosPeer::FECHA_CREACION, $fechaCreacion, $comparison);
    }

    /**
     * Filter the query on the fecha_vencimiento column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaVencimiento('2011-03-14'); // WHERE fecha_vencimiento = '2011-03-14'
     * $query->filterByFechaVencimiento('now'); // WHERE fecha_vencimiento = '2011-03-14'
     * $query->filterByFechaVencimiento(array('max' => 'yesterday')); // WHERE fecha_vencimiento < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaVencimiento The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ListaPreciosQuery The current query, for fluid interface
     */
    public function filterByFechaVencimiento($fechaVencimiento = null, $comparison = null)
    {
        if (is_array($fechaVencimiento)) {
            $useMinMax = false;
            if (isset($fechaVencimiento['min'])) {
                $this->addUsingAlias(ListaPreciosPeer::FECHA_VENCIMIENTO, $fechaVencimiento['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaVencimiento['max'])) {
                $this->addUsingAlias(ListaPreciosPeer::FECHA_VENCIMIENTO, $fechaVencimiento['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ListaPreciosPeer::FECHA_VENCIMIENTO, $fechaVencimiento, $comparison);
    }

    /**
     * Filter the query by a related RListapreciosPrecio object
     *
     * @param   RListapreciosPrecio|PropelObjectCollection $rListapreciosPrecio  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ListaPreciosQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRListapreciosPrecio($rListapreciosPrecio, $comparison = null)
    {
        if ($rListapreciosPrecio instanceof RListapreciosPrecio) {
            return $this
                ->addUsingAlias(ListaPreciosPeer::ID, $rListapreciosPrecio->getListaPreciosId(), $comparison);
        } elseif ($rListapreciosPrecio instanceof PropelObjectCollection) {
            return $this
                ->useRListapreciosPrecioQuery()
                ->filterByPrimaryKeys($rListapreciosPrecio->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRListapreciosPrecio() only accepts arguments of type RListapreciosPrecio or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RListapreciosPrecio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ListaPreciosQuery The current query, for fluid interface
     */
    public function joinRListapreciosPrecio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RListapreciosPrecio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RListapreciosPrecio');
        }

        return $this;
    }

    /**
     * Use the RListapreciosPrecio relation RListapreciosPrecio object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RListapreciosPrecioQuery A secondary query class using the current class as primary query
     */
    public function useRListapreciosPrecioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRListapreciosPrecio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RListapreciosPrecio', 'RListapreciosPrecioQuery');
    }

    /**
     * Filter the query by a related RProveedorListaprecios object
     *
     * @param   RProveedorListaprecios|PropelObjectCollection $rProveedorListaprecios  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ListaPreciosQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProveedorListaprecios($rProveedorListaprecios, $comparison = null)
    {
        if ($rProveedorListaprecios instanceof RProveedorListaprecios) {
            return $this
                ->addUsingAlias(ListaPreciosPeer::ID, $rProveedorListaprecios->getListaPreciosId(), $comparison);
        } elseif ($rProveedorListaprecios instanceof PropelObjectCollection) {
            return $this
                ->useRProveedorListapreciosQuery()
                ->filterByPrimaryKeys($rProveedorListaprecios->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProveedorListaprecios() only accepts arguments of type RProveedorListaprecios or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProveedorListaprecios relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ListaPreciosQuery The current query, for fluid interface
     */
    public function joinRProveedorListaprecios($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProveedorListaprecios');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProveedorListaprecios');
        }

        return $this;
    }

    /**
     * Use the RProveedorListaprecios relation RProveedorListaprecios object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProveedorListapreciosQuery A secondary query class using the current class as primary query
     */
    public function useRProveedorListapreciosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProveedorListaprecios($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProveedorListaprecios', 'RProveedorListapreciosQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ListaPrecios $listaPrecios Object to remove from the list of results
     *
     * @return ListaPreciosQuery The current query, for fluid interface
     */
    public function prune($listaPrecios = null)
    {
        if ($listaPrecios) {
            $this->addUsingAlias(ListaPreciosPeer::ID, $listaPrecios->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
