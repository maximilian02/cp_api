<?php


/**
 * Base class that represents a query for the 'precio' table.
 *
 *
 *
 * @method PrecioQuery orderById($order = Criteria::ASC) Order by the id column
 * @method PrecioQuery orderByPrecio($order = Criteria::ASC) Order by the precio column
 * @method PrecioQuery orderByFechaActualizacion($order = Criteria::ASC) Order by the fecha_actualizacion column
 *
 * @method PrecioQuery groupById() Group by the id column
 * @method PrecioQuery groupByPrecio() Group by the precio column
 * @method PrecioQuery groupByFechaActualizacion() Group by the fecha_actualizacion column
 *
 * @method PrecioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method PrecioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method PrecioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method PrecioQuery leftJoinRListapreciosPrecio($relationAlias = null) Adds a LEFT JOIN clause to the query using the RListapreciosPrecio relation
 * @method PrecioQuery rightJoinRListapreciosPrecio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RListapreciosPrecio relation
 * @method PrecioQuery innerJoinRListapreciosPrecio($relationAlias = null) Adds a INNER JOIN clause to the query using the RListapreciosPrecio relation
 *
 * @method PrecioQuery leftJoinRProductoPrecio($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProductoPrecio relation
 * @method PrecioQuery rightJoinRProductoPrecio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProductoPrecio relation
 * @method PrecioQuery innerJoinRProductoPrecio($relationAlias = null) Adds a INNER JOIN clause to the query using the RProductoPrecio relation
 *
 * @method Precio findOne(PropelPDO $con = null) Return the first Precio matching the query
 * @method Precio findOneOrCreate(PropelPDO $con = null) Return the first Precio matching the query, or a new Precio object populated from the query conditions when no match is found
 *
 * @method Precio findOneByPrecio(double $precio) Return the first Precio filtered by the precio column
 * @method Precio findOneByFechaActualizacion(string $fecha_actualizacion) Return the first Precio filtered by the fecha_actualizacion column
 *
 * @method array findById(int $id) Return Precio objects filtered by the id column
 * @method array findByPrecio(double $precio) Return Precio objects filtered by the precio column
 * @method array findByFechaActualizacion(string $fecha_actualizacion) Return Precio objects filtered by the fecha_actualizacion column
 *
 * @package    propel.generator.cp.om
 */
abstract class BasePrecioQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BasePrecioQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'Precio';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new PrecioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   PrecioQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return PrecioQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof PrecioQuery) {
            return $criteria;
        }
        $query = new PrecioQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Precio|Precio[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = PrecioPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(PrecioPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Precio A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Precio A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `precio`, `fecha_actualizacion` FROM `precio` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Precio();
            $obj->hydrate($row);
            PrecioPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Precio|Precio[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Precio[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return PrecioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(PrecioPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return PrecioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(PrecioPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrecioQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(PrecioPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(PrecioPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrecioPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the precio column
     *
     * Example usage:
     * <code>
     * $query->filterByPrecio(1234); // WHERE precio = 1234
     * $query->filterByPrecio(array(12, 34)); // WHERE precio IN (12, 34)
     * $query->filterByPrecio(array('min' => 12)); // WHERE precio >= 12
     * $query->filterByPrecio(array('max' => 12)); // WHERE precio <= 12
     * </code>
     *
     * @param     mixed $precio The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrecioQuery The current query, for fluid interface
     */
    public function filterByPrecio($precio = null, $comparison = null)
    {
        if (is_array($precio)) {
            $useMinMax = false;
            if (isset($precio['min'])) {
                $this->addUsingAlias(PrecioPeer::PRECIO, $precio['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($precio['max'])) {
                $this->addUsingAlias(PrecioPeer::PRECIO, $precio['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrecioPeer::PRECIO, $precio, $comparison);
    }

    /**
     * Filter the query on the fecha_actualizacion column
     *
     * Example usage:
     * <code>
     * $query->filterByFechaActualizacion('2011-03-14'); // WHERE fecha_actualizacion = '2011-03-14'
     * $query->filterByFechaActualizacion('now'); // WHERE fecha_actualizacion = '2011-03-14'
     * $query->filterByFechaActualizacion(array('max' => 'yesterday')); // WHERE fecha_actualizacion < '2011-03-13'
     * </code>
     *
     * @param     mixed $fechaActualizacion The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return PrecioQuery The current query, for fluid interface
     */
    public function filterByFechaActualizacion($fechaActualizacion = null, $comparison = null)
    {
        if (is_array($fechaActualizacion)) {
            $useMinMax = false;
            if (isset($fechaActualizacion['min'])) {
                $this->addUsingAlias(PrecioPeer::FECHA_ACTUALIZACION, $fechaActualizacion['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($fechaActualizacion['max'])) {
                $this->addUsingAlias(PrecioPeer::FECHA_ACTUALIZACION, $fechaActualizacion['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(PrecioPeer::FECHA_ACTUALIZACION, $fechaActualizacion, $comparison);
    }

    /**
     * Filter the query by a related RListapreciosPrecio object
     *
     * @param   RListapreciosPrecio|PropelObjectCollection $rListapreciosPrecio  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrecioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRListapreciosPrecio($rListapreciosPrecio, $comparison = null)
    {
        if ($rListapreciosPrecio instanceof RListapreciosPrecio) {
            return $this
                ->addUsingAlias(PrecioPeer::ID, $rListapreciosPrecio->getPrecioId(), $comparison);
        } elseif ($rListapreciosPrecio instanceof PropelObjectCollection) {
            return $this
                ->useRListapreciosPrecioQuery()
                ->filterByPrimaryKeys($rListapreciosPrecio->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRListapreciosPrecio() only accepts arguments of type RListapreciosPrecio or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RListapreciosPrecio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrecioQuery The current query, for fluid interface
     */
    public function joinRListapreciosPrecio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RListapreciosPrecio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RListapreciosPrecio');
        }

        return $this;
    }

    /**
     * Use the RListapreciosPrecio relation RListapreciosPrecio object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RListapreciosPrecioQuery A secondary query class using the current class as primary query
     */
    public function useRListapreciosPrecioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRListapreciosPrecio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RListapreciosPrecio', 'RListapreciosPrecioQuery');
    }

    /**
     * Filter the query by a related RProductoPrecio object
     *
     * @param   RProductoPrecio|PropelObjectCollection $rProductoPrecio  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 PrecioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProductoPrecio($rProductoPrecio, $comparison = null)
    {
        if ($rProductoPrecio instanceof RProductoPrecio) {
            return $this
                ->addUsingAlias(PrecioPeer::ID, $rProductoPrecio->getPrecioId(), $comparison);
        } elseif ($rProductoPrecio instanceof PropelObjectCollection) {
            return $this
                ->useRProductoPrecioQuery()
                ->filterByPrimaryKeys($rProductoPrecio->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProductoPrecio() only accepts arguments of type RProductoPrecio or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProductoPrecio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return PrecioQuery The current query, for fluid interface
     */
    public function joinRProductoPrecio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProductoPrecio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProductoPrecio');
        }

        return $this;
    }

    /**
     * Use the RProductoPrecio relation RProductoPrecio object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProductoPrecioQuery A secondary query class using the current class as primary query
     */
    public function useRProductoPrecioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProductoPrecio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProductoPrecio', 'RProductoPrecioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Precio $precio Object to remove from the list of results
     *
     * @return PrecioQuery The current query, for fluid interface
     */
    public function prune($precio = null)
    {
        if ($precio) {
            $this->addUsingAlias(PrecioPeer::ID, $precio->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
