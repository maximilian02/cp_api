<?php


/**
 * Base class that represents a row from the 'producto' table.
 *
 *
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseProducto extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'ProductoPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        ProductoPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the nombre field.
     * @var        string
     */
    protected $nombre;

    /**
     * The value for the descripcion field.
     * @var        string
     */
    protected $descripcion;

    /**
     * The value for the codigo field.
     * @var        string
     */
    protected $codigo;

    /**
     * The value for the imagen field.
     * @var        string
     */
    protected $imagen;

    /**
     * @var        PropelObjectCollection|RComercioProducto[] Collection to store aggregation of RComercioProducto objects.
     */
    protected $collRComercioProductos;
    protected $collRComercioProductosPartial;

    /**
     * @var        PropelObjectCollection|RProductoCategoriaproductos[] Collection to store aggregation of RProductoCategoriaproductos objects.
     */
    protected $collRProductoCategoriaproductoss;
    protected $collRProductoCategoriaproductossPartial;

    /**
     * @var        PropelObjectCollection|RProductoEmpaque[] Collection to store aggregation of RProductoEmpaque objects.
     */
    protected $collRProductoEmpaques;
    protected $collRProductoEmpaquesPartial;

    /**
     * @var        PropelObjectCollection|RProductoEnvase[] Collection to store aggregation of RProductoEnvase objects.
     */
    protected $collRProductoEnvases;
    protected $collRProductoEnvasesPartial;

    /**
     * @var        PropelObjectCollection|RProductoMarca[] Collection to store aggregation of RProductoMarca objects.
     */
    protected $collRProductoMarcas;
    protected $collRProductoMarcasPartial;

    /**
     * @var        PropelObjectCollection|RProductoPrecio[] Collection to store aggregation of RProductoPrecio objects.
     */
    protected $collRProductoPrecios;
    protected $collRProductoPreciosPartial;

    /**
     * @var        PropelObjectCollection|RProveedorProducto[] Collection to store aggregation of RProveedorProducto objects.
     */
    protected $collRProveedorProductos;
    protected $collRProveedorProductosPartial;

    /**
     * @var        PropelObjectCollection|RUsuarioProducto[] Collection to store aggregation of RUsuarioProducto objects.
     */
    protected $collRUsuarioProductos;
    protected $collRUsuarioProductosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rComercioProductosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rProductoCategoriaproductossScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rProductoEmpaquesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rProductoEnvasesScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rProductoMarcasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rProductoPreciosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rProveedorProductosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rUsuarioProductosScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [nombre] column value.
     *
     * @return string
     */
    public function getNombre()
    {

        return $this->nombre;
    }

    /**
     * Get the [descripcion] column value.
     *
     * @return string
     */
    public function getDescripcion()
    {

        return $this->descripcion;
    }

    /**
     * Get the [codigo] column value.
     *
     * @return string
     */
    public function getCodigo()
    {

        return $this->codigo;
    }

    /**
     * Get the [imagen] column value.
     *
     * @return string
     */
    public function getImagen()
    {

        return $this->imagen;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Producto The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = ProductoPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [nombre] column.
     *
     * @param  string $v new value
     * @return Producto The current object (for fluent API support)
     */
    public function setNombre($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre !== $v) {
            $this->nombre = $v;
            $this->modifiedColumns[] = ProductoPeer::NOMBRE;
        }


        return $this;
    } // setNombre()

    /**
     * Set the value of [descripcion] column.
     *
     * @param  string $v new value
     * @return Producto The current object (for fluent API support)
     */
    public function setDescripcion($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->descripcion !== $v) {
            $this->descripcion = $v;
            $this->modifiedColumns[] = ProductoPeer::DESCRIPCION;
        }


        return $this;
    } // setDescripcion()

    /**
     * Set the value of [codigo] column.
     *
     * @param  string $v new value
     * @return Producto The current object (for fluent API support)
     */
    public function setCodigo($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->codigo !== $v) {
            $this->codigo = $v;
            $this->modifiedColumns[] = ProductoPeer::CODIGO;
        }


        return $this;
    } // setCodigo()

    /**
     * Set the value of [imagen] column.
     *
     * @param  string $v new value
     * @return Producto The current object (for fluent API support)
     */
    public function setImagen($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->imagen !== $v) {
            $this->imagen = $v;
            $this->modifiedColumns[] = ProductoPeer::IMAGEN;
        }


        return $this;
    } // setImagen()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->nombre = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->descripcion = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->codigo = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->imagen = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 5; // 5 = ProductoPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Producto object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ProductoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ProductoPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collRComercioProductos = null;

            $this->collRProductoCategoriaproductoss = null;

            $this->collRProductoEmpaques = null;

            $this->collRProductoEnvases = null;

            $this->collRProductoMarcas = null;

            $this->collRProductoPrecios = null;

            $this->collRProveedorProductos = null;

            $this->collRUsuarioProductos = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ProductoPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ProductoQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ProductoPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ProductoPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->rComercioProductosScheduledForDeletion !== null) {
                if (!$this->rComercioProductosScheduledForDeletion->isEmpty()) {
                    RComercioProductoQuery::create()
                        ->filterByPrimaryKeys($this->rComercioProductosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rComercioProductosScheduledForDeletion = null;
                }
            }

            if ($this->collRComercioProductos !== null) {
                foreach ($this->collRComercioProductos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rProductoCategoriaproductossScheduledForDeletion !== null) {
                if (!$this->rProductoCategoriaproductossScheduledForDeletion->isEmpty()) {
                    RProductoCategoriaproductosQuery::create()
                        ->filterByPrimaryKeys($this->rProductoCategoriaproductossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rProductoCategoriaproductossScheduledForDeletion = null;
                }
            }

            if ($this->collRProductoCategoriaproductoss !== null) {
                foreach ($this->collRProductoCategoriaproductoss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rProductoEmpaquesScheduledForDeletion !== null) {
                if (!$this->rProductoEmpaquesScheduledForDeletion->isEmpty()) {
                    RProductoEmpaqueQuery::create()
                        ->filterByPrimaryKeys($this->rProductoEmpaquesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rProductoEmpaquesScheduledForDeletion = null;
                }
            }

            if ($this->collRProductoEmpaques !== null) {
                foreach ($this->collRProductoEmpaques as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rProductoEnvasesScheduledForDeletion !== null) {
                if (!$this->rProductoEnvasesScheduledForDeletion->isEmpty()) {
                    RProductoEnvaseQuery::create()
                        ->filterByPrimaryKeys($this->rProductoEnvasesScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rProductoEnvasesScheduledForDeletion = null;
                }
            }

            if ($this->collRProductoEnvases !== null) {
                foreach ($this->collRProductoEnvases as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rProductoMarcasScheduledForDeletion !== null) {
                if (!$this->rProductoMarcasScheduledForDeletion->isEmpty()) {
                    RProductoMarcaQuery::create()
                        ->filterByPrimaryKeys($this->rProductoMarcasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rProductoMarcasScheduledForDeletion = null;
                }
            }

            if ($this->collRProductoMarcas !== null) {
                foreach ($this->collRProductoMarcas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rProductoPreciosScheduledForDeletion !== null) {
                if (!$this->rProductoPreciosScheduledForDeletion->isEmpty()) {
                    RProductoPrecioQuery::create()
                        ->filterByPrimaryKeys($this->rProductoPreciosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rProductoPreciosScheduledForDeletion = null;
                }
            }

            if ($this->collRProductoPrecios !== null) {
                foreach ($this->collRProductoPrecios as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rProveedorProductosScheduledForDeletion !== null) {
                if (!$this->rProveedorProductosScheduledForDeletion->isEmpty()) {
                    RProveedorProductoQuery::create()
                        ->filterByPrimaryKeys($this->rProveedorProductosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rProveedorProductosScheduledForDeletion = null;
                }
            }

            if ($this->collRProveedorProductos !== null) {
                foreach ($this->collRProveedorProductos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rUsuarioProductosScheduledForDeletion !== null) {
                if (!$this->rUsuarioProductosScheduledForDeletion->isEmpty()) {
                    RUsuarioProductoQuery::create()
                        ->filterByPrimaryKeys($this->rUsuarioProductosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rUsuarioProductosScheduledForDeletion = null;
                }
            }

            if ($this->collRUsuarioProductos !== null) {
                foreach ($this->collRUsuarioProductos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ProductoPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ProductoPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ProductoPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(ProductoPeer::NOMBRE)) {
            $modifiedColumns[':p' . $index++]  = '`nombre`';
        }
        if ($this->isColumnModified(ProductoPeer::DESCRIPCION)) {
            $modifiedColumns[':p' . $index++]  = '`descripcion`';
        }
        if ($this->isColumnModified(ProductoPeer::CODIGO)) {
            $modifiedColumns[':p' . $index++]  = '`codigo`';
        }
        if ($this->isColumnModified(ProductoPeer::IMAGEN)) {
            $modifiedColumns[':p' . $index++]  = '`imagen`';
        }

        $sql = sprintf(
            'INSERT INTO `producto` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`nombre`':
                        $stmt->bindValue($identifier, $this->nombre, PDO::PARAM_STR);
                        break;
                    case '`descripcion`':
                        $stmt->bindValue($identifier, $this->descripcion, PDO::PARAM_STR);
                        break;
                    case '`codigo`':
                        $stmt->bindValue($identifier, $this->codigo, PDO::PARAM_STR);
                        break;
                    case '`imagen`':
                        $stmt->bindValue($identifier, $this->imagen, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = ProductoPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collRComercioProductos !== null) {
                    foreach ($this->collRComercioProductos as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRProductoCategoriaproductoss !== null) {
                    foreach ($this->collRProductoCategoriaproductoss as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRProductoEmpaques !== null) {
                    foreach ($this->collRProductoEmpaques as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRProductoEnvases !== null) {
                    foreach ($this->collRProductoEnvases as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRProductoMarcas !== null) {
                    foreach ($this->collRProductoMarcas as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRProductoPrecios !== null) {
                    foreach ($this->collRProductoPrecios as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRProveedorProductos !== null) {
                    foreach ($this->collRProveedorProductos as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRUsuarioProductos !== null) {
                    foreach ($this->collRUsuarioProductos as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ProductoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getNombre();
                break;
            case 2:
                return $this->getDescripcion();
                break;
            case 3:
                return $this->getCodigo();
                break;
            case 4:
                return $this->getImagen();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Producto'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Producto'][$this->getPrimaryKey()] = true;
        $keys = ProductoPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getNombre(),
            $keys[2] => $this->getDescripcion(),
            $keys[3] => $this->getCodigo(),
            $keys[4] => $this->getImagen(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collRComercioProductos) {
                $result['RComercioProductos'] = $this->collRComercioProductos->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRProductoCategoriaproductoss) {
                $result['RProductoCategoriaproductoss'] = $this->collRProductoCategoriaproductoss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRProductoEmpaques) {
                $result['RProductoEmpaques'] = $this->collRProductoEmpaques->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRProductoEnvases) {
                $result['RProductoEnvases'] = $this->collRProductoEnvases->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRProductoMarcas) {
                $result['RProductoMarcas'] = $this->collRProductoMarcas->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRProductoPrecios) {
                $result['RProductoPrecios'] = $this->collRProductoPrecios->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRProveedorProductos) {
                $result['RProveedorProductos'] = $this->collRProveedorProductos->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRUsuarioProductos) {
                $result['RUsuarioProductos'] = $this->collRUsuarioProductos->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ProductoPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setNombre($value);
                break;
            case 2:
                $this->setDescripcion($value);
                break;
            case 3:
                $this->setCodigo($value);
                break;
            case 4:
                $this->setImagen($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ProductoPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNombre($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setDescripcion($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setCodigo($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setImagen($arr[$keys[4]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ProductoPeer::DATABASE_NAME);

        if ($this->isColumnModified(ProductoPeer::ID)) $criteria->add(ProductoPeer::ID, $this->id);
        if ($this->isColumnModified(ProductoPeer::NOMBRE)) $criteria->add(ProductoPeer::NOMBRE, $this->nombre);
        if ($this->isColumnModified(ProductoPeer::DESCRIPCION)) $criteria->add(ProductoPeer::DESCRIPCION, $this->descripcion);
        if ($this->isColumnModified(ProductoPeer::CODIGO)) $criteria->add(ProductoPeer::CODIGO, $this->codigo);
        if ($this->isColumnModified(ProductoPeer::IMAGEN)) $criteria->add(ProductoPeer::IMAGEN, $this->imagen);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ProductoPeer::DATABASE_NAME);
        $criteria->add(ProductoPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Producto (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNombre($this->getNombre());
        $copyObj->setDescripcion($this->getDescripcion());
        $copyObj->setCodigo($this->getCodigo());
        $copyObj->setImagen($this->getImagen());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getRComercioProductos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRComercioProducto($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRProductoCategoriaproductoss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRProductoCategoriaproductos($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRProductoEmpaques() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRProductoEmpaque($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRProductoEnvases() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRProductoEnvase($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRProductoMarcas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRProductoMarca($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRProductoPrecios() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRProductoPrecio($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRProveedorProductos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRProveedorProducto($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRUsuarioProductos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRUsuarioProducto($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Producto Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ProductoPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ProductoPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('RComercioProducto' == $relationName) {
            $this->initRComercioProductos();
        }
        if ('RProductoCategoriaproductos' == $relationName) {
            $this->initRProductoCategoriaproductoss();
        }
        if ('RProductoEmpaque' == $relationName) {
            $this->initRProductoEmpaques();
        }
        if ('RProductoEnvase' == $relationName) {
            $this->initRProductoEnvases();
        }
        if ('RProductoMarca' == $relationName) {
            $this->initRProductoMarcas();
        }
        if ('RProductoPrecio' == $relationName) {
            $this->initRProductoPrecios();
        }
        if ('RProveedorProducto' == $relationName) {
            $this->initRProveedorProductos();
        }
        if ('RUsuarioProducto' == $relationName) {
            $this->initRUsuarioProductos();
        }
    }

    /**
     * Clears out the collRComercioProductos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Producto The current object (for fluent API support)
     * @see        addRComercioProductos()
     */
    public function clearRComercioProductos()
    {
        $this->collRComercioProductos = null; // important to set this to null since that means it is uninitialized
        $this->collRComercioProductosPartial = null;

        return $this;
    }

    /**
     * reset is the collRComercioProductos collection loaded partially
     *
     * @return void
     */
    public function resetPartialRComercioProductos($v = true)
    {
        $this->collRComercioProductosPartial = $v;
    }

    /**
     * Initializes the collRComercioProductos collection.
     *
     * By default this just sets the collRComercioProductos collection to an empty array (like clearcollRComercioProductos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRComercioProductos($overrideExisting = true)
    {
        if (null !== $this->collRComercioProductos && !$overrideExisting) {
            return;
        }
        $this->collRComercioProductos = new PropelObjectCollection();
        $this->collRComercioProductos->setModel('RComercioProducto');
    }

    /**
     * Gets an array of RComercioProducto objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Producto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RComercioProducto[] List of RComercioProducto objects
     * @throws PropelException
     */
    public function getRComercioProductos($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRComercioProductosPartial && !$this->isNew();
        if (null === $this->collRComercioProductos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRComercioProductos) {
                // return empty collection
                $this->initRComercioProductos();
            } else {
                $collRComercioProductos = RComercioProductoQuery::create(null, $criteria)
                    ->filterByProducto($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRComercioProductosPartial && count($collRComercioProductos)) {
                      $this->initRComercioProductos(false);

                      foreach ($collRComercioProductos as $obj) {
                        if (false == $this->collRComercioProductos->contains($obj)) {
                          $this->collRComercioProductos->append($obj);
                        }
                      }

                      $this->collRComercioProductosPartial = true;
                    }

                    $collRComercioProductos->getInternalIterator()->rewind();

                    return $collRComercioProductos;
                }

                if ($partial && $this->collRComercioProductos) {
                    foreach ($this->collRComercioProductos as $obj) {
                        if ($obj->isNew()) {
                            $collRComercioProductos[] = $obj;
                        }
                    }
                }

                $this->collRComercioProductos = $collRComercioProductos;
                $this->collRComercioProductosPartial = false;
            }
        }

        return $this->collRComercioProductos;
    }

    /**
     * Sets a collection of RComercioProducto objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rComercioProductos A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Producto The current object (for fluent API support)
     */
    public function setRComercioProductos(PropelCollection $rComercioProductos, PropelPDO $con = null)
    {
        $rComercioProductosToDelete = $this->getRComercioProductos(new Criteria(), $con)->diff($rComercioProductos);


        $this->rComercioProductosScheduledForDeletion = $rComercioProductosToDelete;

        foreach ($rComercioProductosToDelete as $rComercioProductoRemoved) {
            $rComercioProductoRemoved->setProducto(null);
        }

        $this->collRComercioProductos = null;
        foreach ($rComercioProductos as $rComercioProducto) {
            $this->addRComercioProducto($rComercioProducto);
        }

        $this->collRComercioProductos = $rComercioProductos;
        $this->collRComercioProductosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RComercioProducto objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RComercioProducto objects.
     * @throws PropelException
     */
    public function countRComercioProductos(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRComercioProductosPartial && !$this->isNew();
        if (null === $this->collRComercioProductos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRComercioProductos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRComercioProductos());
            }
            $query = RComercioProductoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProducto($this)
                ->count($con);
        }

        return count($this->collRComercioProductos);
    }

    /**
     * Method called to associate a RComercioProducto object to this object
     * through the RComercioProducto foreign key attribute.
     *
     * @param    RComercioProducto $l RComercioProducto
     * @return Producto The current object (for fluent API support)
     */
    public function addRComercioProducto(RComercioProducto $l)
    {
        if ($this->collRComercioProductos === null) {
            $this->initRComercioProductos();
            $this->collRComercioProductosPartial = true;
        }

        if (!in_array($l, $this->collRComercioProductos->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRComercioProducto($l);

            if ($this->rComercioProductosScheduledForDeletion and $this->rComercioProductosScheduledForDeletion->contains($l)) {
                $this->rComercioProductosScheduledForDeletion->remove($this->rComercioProductosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RComercioProducto $rComercioProducto The rComercioProducto object to add.
     */
    protected function doAddRComercioProducto($rComercioProducto)
    {
        $this->collRComercioProductos[]= $rComercioProducto;
        $rComercioProducto->setProducto($this);
    }

    /**
     * @param	RComercioProducto $rComercioProducto The rComercioProducto object to remove.
     * @return Producto The current object (for fluent API support)
     */
    public function removeRComercioProducto($rComercioProducto)
    {
        if ($this->getRComercioProductos()->contains($rComercioProducto)) {
            $this->collRComercioProductos->remove($this->collRComercioProductos->search($rComercioProducto));
            if (null === $this->rComercioProductosScheduledForDeletion) {
                $this->rComercioProductosScheduledForDeletion = clone $this->collRComercioProductos;
                $this->rComercioProductosScheduledForDeletion->clear();
            }
            $this->rComercioProductosScheduledForDeletion[]= clone $rComercioProducto;
            $rComercioProducto->setProducto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Producto is new, it will return
     * an empty collection; or if this Producto has previously
     * been saved, it will retrieve related RComercioProductos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Producto.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RComercioProducto[] List of RComercioProducto objects
     */
    public function getRComercioProductosJoinComercio($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RComercioProductoQuery::create(null, $criteria);
        $query->joinWith('Comercio', $join_behavior);

        return $this->getRComercioProductos($query, $con);
    }

    /**
     * Clears out the collRProductoCategoriaproductoss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Producto The current object (for fluent API support)
     * @see        addRProductoCategoriaproductoss()
     */
    public function clearRProductoCategoriaproductoss()
    {
        $this->collRProductoCategoriaproductoss = null; // important to set this to null since that means it is uninitialized
        $this->collRProductoCategoriaproductossPartial = null;

        return $this;
    }

    /**
     * reset is the collRProductoCategoriaproductoss collection loaded partially
     *
     * @return void
     */
    public function resetPartialRProductoCategoriaproductoss($v = true)
    {
        $this->collRProductoCategoriaproductossPartial = $v;
    }

    /**
     * Initializes the collRProductoCategoriaproductoss collection.
     *
     * By default this just sets the collRProductoCategoriaproductoss collection to an empty array (like clearcollRProductoCategoriaproductoss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRProductoCategoriaproductoss($overrideExisting = true)
    {
        if (null !== $this->collRProductoCategoriaproductoss && !$overrideExisting) {
            return;
        }
        $this->collRProductoCategoriaproductoss = new PropelObjectCollection();
        $this->collRProductoCategoriaproductoss->setModel('RProductoCategoriaproductos');
    }

    /**
     * Gets an array of RProductoCategoriaproductos objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Producto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RProductoCategoriaproductos[] List of RProductoCategoriaproductos objects
     * @throws PropelException
     */
    public function getRProductoCategoriaproductoss($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRProductoCategoriaproductossPartial && !$this->isNew();
        if (null === $this->collRProductoCategoriaproductoss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRProductoCategoriaproductoss) {
                // return empty collection
                $this->initRProductoCategoriaproductoss();
            } else {
                $collRProductoCategoriaproductoss = RProductoCategoriaproductosQuery::create(null, $criteria)
                    ->filterByProducto($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRProductoCategoriaproductossPartial && count($collRProductoCategoriaproductoss)) {
                      $this->initRProductoCategoriaproductoss(false);

                      foreach ($collRProductoCategoriaproductoss as $obj) {
                        if (false == $this->collRProductoCategoriaproductoss->contains($obj)) {
                          $this->collRProductoCategoriaproductoss->append($obj);
                        }
                      }

                      $this->collRProductoCategoriaproductossPartial = true;
                    }

                    $collRProductoCategoriaproductoss->getInternalIterator()->rewind();

                    return $collRProductoCategoriaproductoss;
                }

                if ($partial && $this->collRProductoCategoriaproductoss) {
                    foreach ($this->collRProductoCategoriaproductoss as $obj) {
                        if ($obj->isNew()) {
                            $collRProductoCategoriaproductoss[] = $obj;
                        }
                    }
                }

                $this->collRProductoCategoriaproductoss = $collRProductoCategoriaproductoss;
                $this->collRProductoCategoriaproductossPartial = false;
            }
        }

        return $this->collRProductoCategoriaproductoss;
    }

    /**
     * Sets a collection of RProductoCategoriaproductos objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rProductoCategoriaproductoss A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Producto The current object (for fluent API support)
     */
    public function setRProductoCategoriaproductoss(PropelCollection $rProductoCategoriaproductoss, PropelPDO $con = null)
    {
        $rProductoCategoriaproductossToDelete = $this->getRProductoCategoriaproductoss(new Criteria(), $con)->diff($rProductoCategoriaproductoss);


        $this->rProductoCategoriaproductossScheduledForDeletion = $rProductoCategoriaproductossToDelete;

        foreach ($rProductoCategoriaproductossToDelete as $rProductoCategoriaproductosRemoved) {
            $rProductoCategoriaproductosRemoved->setProducto(null);
        }

        $this->collRProductoCategoriaproductoss = null;
        foreach ($rProductoCategoriaproductoss as $rProductoCategoriaproductos) {
            $this->addRProductoCategoriaproductos($rProductoCategoriaproductos);
        }

        $this->collRProductoCategoriaproductoss = $rProductoCategoriaproductoss;
        $this->collRProductoCategoriaproductossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RProductoCategoriaproductos objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RProductoCategoriaproductos objects.
     * @throws PropelException
     */
    public function countRProductoCategoriaproductoss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRProductoCategoriaproductossPartial && !$this->isNew();
        if (null === $this->collRProductoCategoriaproductoss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRProductoCategoriaproductoss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRProductoCategoriaproductoss());
            }
            $query = RProductoCategoriaproductosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProducto($this)
                ->count($con);
        }

        return count($this->collRProductoCategoriaproductoss);
    }

    /**
     * Method called to associate a RProductoCategoriaproductos object to this object
     * through the RProductoCategoriaproductos foreign key attribute.
     *
     * @param    RProductoCategoriaproductos $l RProductoCategoriaproductos
     * @return Producto The current object (for fluent API support)
     */
    public function addRProductoCategoriaproductos(RProductoCategoriaproductos $l)
    {
        if ($this->collRProductoCategoriaproductoss === null) {
            $this->initRProductoCategoriaproductoss();
            $this->collRProductoCategoriaproductossPartial = true;
        }

        if (!in_array($l, $this->collRProductoCategoriaproductoss->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRProductoCategoriaproductos($l);

            if ($this->rProductoCategoriaproductossScheduledForDeletion and $this->rProductoCategoriaproductossScheduledForDeletion->contains($l)) {
                $this->rProductoCategoriaproductossScheduledForDeletion->remove($this->rProductoCategoriaproductossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RProductoCategoriaproductos $rProductoCategoriaproductos The rProductoCategoriaproductos object to add.
     */
    protected function doAddRProductoCategoriaproductos($rProductoCategoriaproductos)
    {
        $this->collRProductoCategoriaproductoss[]= $rProductoCategoriaproductos;
        $rProductoCategoriaproductos->setProducto($this);
    }

    /**
     * @param	RProductoCategoriaproductos $rProductoCategoriaproductos The rProductoCategoriaproductos object to remove.
     * @return Producto The current object (for fluent API support)
     */
    public function removeRProductoCategoriaproductos($rProductoCategoriaproductos)
    {
        if ($this->getRProductoCategoriaproductoss()->contains($rProductoCategoriaproductos)) {
            $this->collRProductoCategoriaproductoss->remove($this->collRProductoCategoriaproductoss->search($rProductoCategoriaproductos));
            if (null === $this->rProductoCategoriaproductossScheduledForDeletion) {
                $this->rProductoCategoriaproductossScheduledForDeletion = clone $this->collRProductoCategoriaproductoss;
                $this->rProductoCategoriaproductossScheduledForDeletion->clear();
            }
            $this->rProductoCategoriaproductossScheduledForDeletion[]= clone $rProductoCategoriaproductos;
            $rProductoCategoriaproductos->setProducto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Producto is new, it will return
     * an empty collection; or if this Producto has previously
     * been saved, it will retrieve related RProductoCategoriaproductoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Producto.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RProductoCategoriaproductos[] List of RProductoCategoriaproductos objects
     */
    public function getRProductoCategoriaproductossJoinCategoriaProductos($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RProductoCategoriaproductosQuery::create(null, $criteria);
        $query->joinWith('CategoriaProductos', $join_behavior);

        return $this->getRProductoCategoriaproductoss($query, $con);
    }

    /**
     * Clears out the collRProductoEmpaques collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Producto The current object (for fluent API support)
     * @see        addRProductoEmpaques()
     */
    public function clearRProductoEmpaques()
    {
        $this->collRProductoEmpaques = null; // important to set this to null since that means it is uninitialized
        $this->collRProductoEmpaquesPartial = null;

        return $this;
    }

    /**
     * reset is the collRProductoEmpaques collection loaded partially
     *
     * @return void
     */
    public function resetPartialRProductoEmpaques($v = true)
    {
        $this->collRProductoEmpaquesPartial = $v;
    }

    /**
     * Initializes the collRProductoEmpaques collection.
     *
     * By default this just sets the collRProductoEmpaques collection to an empty array (like clearcollRProductoEmpaques());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRProductoEmpaques($overrideExisting = true)
    {
        if (null !== $this->collRProductoEmpaques && !$overrideExisting) {
            return;
        }
        $this->collRProductoEmpaques = new PropelObjectCollection();
        $this->collRProductoEmpaques->setModel('RProductoEmpaque');
    }

    /**
     * Gets an array of RProductoEmpaque objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Producto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RProductoEmpaque[] List of RProductoEmpaque objects
     * @throws PropelException
     */
    public function getRProductoEmpaques($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRProductoEmpaquesPartial && !$this->isNew();
        if (null === $this->collRProductoEmpaques || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRProductoEmpaques) {
                // return empty collection
                $this->initRProductoEmpaques();
            } else {
                $collRProductoEmpaques = RProductoEmpaqueQuery::create(null, $criteria)
                    ->filterByProducto($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRProductoEmpaquesPartial && count($collRProductoEmpaques)) {
                      $this->initRProductoEmpaques(false);

                      foreach ($collRProductoEmpaques as $obj) {
                        if (false == $this->collRProductoEmpaques->contains($obj)) {
                          $this->collRProductoEmpaques->append($obj);
                        }
                      }

                      $this->collRProductoEmpaquesPartial = true;
                    }

                    $collRProductoEmpaques->getInternalIterator()->rewind();

                    return $collRProductoEmpaques;
                }

                if ($partial && $this->collRProductoEmpaques) {
                    foreach ($this->collRProductoEmpaques as $obj) {
                        if ($obj->isNew()) {
                            $collRProductoEmpaques[] = $obj;
                        }
                    }
                }

                $this->collRProductoEmpaques = $collRProductoEmpaques;
                $this->collRProductoEmpaquesPartial = false;
            }
        }

        return $this->collRProductoEmpaques;
    }

    /**
     * Sets a collection of RProductoEmpaque objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rProductoEmpaques A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Producto The current object (for fluent API support)
     */
    public function setRProductoEmpaques(PropelCollection $rProductoEmpaques, PropelPDO $con = null)
    {
        $rProductoEmpaquesToDelete = $this->getRProductoEmpaques(new Criteria(), $con)->diff($rProductoEmpaques);


        $this->rProductoEmpaquesScheduledForDeletion = $rProductoEmpaquesToDelete;

        foreach ($rProductoEmpaquesToDelete as $rProductoEmpaqueRemoved) {
            $rProductoEmpaqueRemoved->setProducto(null);
        }

        $this->collRProductoEmpaques = null;
        foreach ($rProductoEmpaques as $rProductoEmpaque) {
            $this->addRProductoEmpaque($rProductoEmpaque);
        }

        $this->collRProductoEmpaques = $rProductoEmpaques;
        $this->collRProductoEmpaquesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RProductoEmpaque objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RProductoEmpaque objects.
     * @throws PropelException
     */
    public function countRProductoEmpaques(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRProductoEmpaquesPartial && !$this->isNew();
        if (null === $this->collRProductoEmpaques || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRProductoEmpaques) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRProductoEmpaques());
            }
            $query = RProductoEmpaqueQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProducto($this)
                ->count($con);
        }

        return count($this->collRProductoEmpaques);
    }

    /**
     * Method called to associate a RProductoEmpaque object to this object
     * through the RProductoEmpaque foreign key attribute.
     *
     * @param    RProductoEmpaque $l RProductoEmpaque
     * @return Producto The current object (for fluent API support)
     */
    public function addRProductoEmpaque(RProductoEmpaque $l)
    {
        if ($this->collRProductoEmpaques === null) {
            $this->initRProductoEmpaques();
            $this->collRProductoEmpaquesPartial = true;
        }

        if (!in_array($l, $this->collRProductoEmpaques->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRProductoEmpaque($l);

            if ($this->rProductoEmpaquesScheduledForDeletion and $this->rProductoEmpaquesScheduledForDeletion->contains($l)) {
                $this->rProductoEmpaquesScheduledForDeletion->remove($this->rProductoEmpaquesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RProductoEmpaque $rProductoEmpaque The rProductoEmpaque object to add.
     */
    protected function doAddRProductoEmpaque($rProductoEmpaque)
    {
        $this->collRProductoEmpaques[]= $rProductoEmpaque;
        $rProductoEmpaque->setProducto($this);
    }

    /**
     * @param	RProductoEmpaque $rProductoEmpaque The rProductoEmpaque object to remove.
     * @return Producto The current object (for fluent API support)
     */
    public function removeRProductoEmpaque($rProductoEmpaque)
    {
        if ($this->getRProductoEmpaques()->contains($rProductoEmpaque)) {
            $this->collRProductoEmpaques->remove($this->collRProductoEmpaques->search($rProductoEmpaque));
            if (null === $this->rProductoEmpaquesScheduledForDeletion) {
                $this->rProductoEmpaquesScheduledForDeletion = clone $this->collRProductoEmpaques;
                $this->rProductoEmpaquesScheduledForDeletion->clear();
            }
            $this->rProductoEmpaquesScheduledForDeletion[]= clone $rProductoEmpaque;
            $rProductoEmpaque->setProducto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Producto is new, it will return
     * an empty collection; or if this Producto has previously
     * been saved, it will retrieve related RProductoEmpaques from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Producto.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RProductoEmpaque[] List of RProductoEmpaque objects
     */
    public function getRProductoEmpaquesJoinEmpaque($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RProductoEmpaqueQuery::create(null, $criteria);
        $query->joinWith('Empaque', $join_behavior);

        return $this->getRProductoEmpaques($query, $con);
    }

    /**
     * Clears out the collRProductoEnvases collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Producto The current object (for fluent API support)
     * @see        addRProductoEnvases()
     */
    public function clearRProductoEnvases()
    {
        $this->collRProductoEnvases = null; // important to set this to null since that means it is uninitialized
        $this->collRProductoEnvasesPartial = null;

        return $this;
    }

    /**
     * reset is the collRProductoEnvases collection loaded partially
     *
     * @return void
     */
    public function resetPartialRProductoEnvases($v = true)
    {
        $this->collRProductoEnvasesPartial = $v;
    }

    /**
     * Initializes the collRProductoEnvases collection.
     *
     * By default this just sets the collRProductoEnvases collection to an empty array (like clearcollRProductoEnvases());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRProductoEnvases($overrideExisting = true)
    {
        if (null !== $this->collRProductoEnvases && !$overrideExisting) {
            return;
        }
        $this->collRProductoEnvases = new PropelObjectCollection();
        $this->collRProductoEnvases->setModel('RProductoEnvase');
    }

    /**
     * Gets an array of RProductoEnvase objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Producto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RProductoEnvase[] List of RProductoEnvase objects
     * @throws PropelException
     */
    public function getRProductoEnvases($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRProductoEnvasesPartial && !$this->isNew();
        if (null === $this->collRProductoEnvases || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRProductoEnvases) {
                // return empty collection
                $this->initRProductoEnvases();
            } else {
                $collRProductoEnvases = RProductoEnvaseQuery::create(null, $criteria)
                    ->filterByProducto($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRProductoEnvasesPartial && count($collRProductoEnvases)) {
                      $this->initRProductoEnvases(false);

                      foreach ($collRProductoEnvases as $obj) {
                        if (false == $this->collRProductoEnvases->contains($obj)) {
                          $this->collRProductoEnvases->append($obj);
                        }
                      }

                      $this->collRProductoEnvasesPartial = true;
                    }

                    $collRProductoEnvases->getInternalIterator()->rewind();

                    return $collRProductoEnvases;
                }

                if ($partial && $this->collRProductoEnvases) {
                    foreach ($this->collRProductoEnvases as $obj) {
                        if ($obj->isNew()) {
                            $collRProductoEnvases[] = $obj;
                        }
                    }
                }

                $this->collRProductoEnvases = $collRProductoEnvases;
                $this->collRProductoEnvasesPartial = false;
            }
        }

        return $this->collRProductoEnvases;
    }

    /**
     * Sets a collection of RProductoEnvase objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rProductoEnvases A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Producto The current object (for fluent API support)
     */
    public function setRProductoEnvases(PropelCollection $rProductoEnvases, PropelPDO $con = null)
    {
        $rProductoEnvasesToDelete = $this->getRProductoEnvases(new Criteria(), $con)->diff($rProductoEnvases);


        $this->rProductoEnvasesScheduledForDeletion = $rProductoEnvasesToDelete;

        foreach ($rProductoEnvasesToDelete as $rProductoEnvaseRemoved) {
            $rProductoEnvaseRemoved->setProducto(null);
        }

        $this->collRProductoEnvases = null;
        foreach ($rProductoEnvases as $rProductoEnvase) {
            $this->addRProductoEnvase($rProductoEnvase);
        }

        $this->collRProductoEnvases = $rProductoEnvases;
        $this->collRProductoEnvasesPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RProductoEnvase objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RProductoEnvase objects.
     * @throws PropelException
     */
    public function countRProductoEnvases(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRProductoEnvasesPartial && !$this->isNew();
        if (null === $this->collRProductoEnvases || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRProductoEnvases) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRProductoEnvases());
            }
            $query = RProductoEnvaseQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProducto($this)
                ->count($con);
        }

        return count($this->collRProductoEnvases);
    }

    /**
     * Method called to associate a RProductoEnvase object to this object
     * through the RProductoEnvase foreign key attribute.
     *
     * @param    RProductoEnvase $l RProductoEnvase
     * @return Producto The current object (for fluent API support)
     */
    public function addRProductoEnvase(RProductoEnvase $l)
    {
        if ($this->collRProductoEnvases === null) {
            $this->initRProductoEnvases();
            $this->collRProductoEnvasesPartial = true;
        }

        if (!in_array($l, $this->collRProductoEnvases->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRProductoEnvase($l);

            if ($this->rProductoEnvasesScheduledForDeletion and $this->rProductoEnvasesScheduledForDeletion->contains($l)) {
                $this->rProductoEnvasesScheduledForDeletion->remove($this->rProductoEnvasesScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RProductoEnvase $rProductoEnvase The rProductoEnvase object to add.
     */
    protected function doAddRProductoEnvase($rProductoEnvase)
    {
        $this->collRProductoEnvases[]= $rProductoEnvase;
        $rProductoEnvase->setProducto($this);
    }

    /**
     * @param	RProductoEnvase $rProductoEnvase The rProductoEnvase object to remove.
     * @return Producto The current object (for fluent API support)
     */
    public function removeRProductoEnvase($rProductoEnvase)
    {
        if ($this->getRProductoEnvases()->contains($rProductoEnvase)) {
            $this->collRProductoEnvases->remove($this->collRProductoEnvases->search($rProductoEnvase));
            if (null === $this->rProductoEnvasesScheduledForDeletion) {
                $this->rProductoEnvasesScheduledForDeletion = clone $this->collRProductoEnvases;
                $this->rProductoEnvasesScheduledForDeletion->clear();
            }
            $this->rProductoEnvasesScheduledForDeletion[]= clone $rProductoEnvase;
            $rProductoEnvase->setProducto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Producto is new, it will return
     * an empty collection; or if this Producto has previously
     * been saved, it will retrieve related RProductoEnvases from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Producto.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RProductoEnvase[] List of RProductoEnvase objects
     */
    public function getRProductoEnvasesJoinEnvase($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RProductoEnvaseQuery::create(null, $criteria);
        $query->joinWith('Envase', $join_behavior);

        return $this->getRProductoEnvases($query, $con);
    }

    /**
     * Clears out the collRProductoMarcas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Producto The current object (for fluent API support)
     * @see        addRProductoMarcas()
     */
    public function clearRProductoMarcas()
    {
        $this->collRProductoMarcas = null; // important to set this to null since that means it is uninitialized
        $this->collRProductoMarcasPartial = null;

        return $this;
    }

    /**
     * reset is the collRProductoMarcas collection loaded partially
     *
     * @return void
     */
    public function resetPartialRProductoMarcas($v = true)
    {
        $this->collRProductoMarcasPartial = $v;
    }

    /**
     * Initializes the collRProductoMarcas collection.
     *
     * By default this just sets the collRProductoMarcas collection to an empty array (like clearcollRProductoMarcas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRProductoMarcas($overrideExisting = true)
    {
        if (null !== $this->collRProductoMarcas && !$overrideExisting) {
            return;
        }
        $this->collRProductoMarcas = new PropelObjectCollection();
        $this->collRProductoMarcas->setModel('RProductoMarca');
    }

    /**
     * Gets an array of RProductoMarca objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Producto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RProductoMarca[] List of RProductoMarca objects
     * @throws PropelException
     */
    public function getRProductoMarcas($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRProductoMarcasPartial && !$this->isNew();
        if (null === $this->collRProductoMarcas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRProductoMarcas) {
                // return empty collection
                $this->initRProductoMarcas();
            } else {
                $collRProductoMarcas = RProductoMarcaQuery::create(null, $criteria)
                    ->filterByProducto($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRProductoMarcasPartial && count($collRProductoMarcas)) {
                      $this->initRProductoMarcas(false);

                      foreach ($collRProductoMarcas as $obj) {
                        if (false == $this->collRProductoMarcas->contains($obj)) {
                          $this->collRProductoMarcas->append($obj);
                        }
                      }

                      $this->collRProductoMarcasPartial = true;
                    }

                    $collRProductoMarcas->getInternalIterator()->rewind();

                    return $collRProductoMarcas;
                }

                if ($partial && $this->collRProductoMarcas) {
                    foreach ($this->collRProductoMarcas as $obj) {
                        if ($obj->isNew()) {
                            $collRProductoMarcas[] = $obj;
                        }
                    }
                }

                $this->collRProductoMarcas = $collRProductoMarcas;
                $this->collRProductoMarcasPartial = false;
            }
        }

        return $this->collRProductoMarcas;
    }

    /**
     * Sets a collection of RProductoMarca objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rProductoMarcas A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Producto The current object (for fluent API support)
     */
    public function setRProductoMarcas(PropelCollection $rProductoMarcas, PropelPDO $con = null)
    {
        $rProductoMarcasToDelete = $this->getRProductoMarcas(new Criteria(), $con)->diff($rProductoMarcas);


        $this->rProductoMarcasScheduledForDeletion = $rProductoMarcasToDelete;

        foreach ($rProductoMarcasToDelete as $rProductoMarcaRemoved) {
            $rProductoMarcaRemoved->setProducto(null);
        }

        $this->collRProductoMarcas = null;
        foreach ($rProductoMarcas as $rProductoMarca) {
            $this->addRProductoMarca($rProductoMarca);
        }

        $this->collRProductoMarcas = $rProductoMarcas;
        $this->collRProductoMarcasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RProductoMarca objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RProductoMarca objects.
     * @throws PropelException
     */
    public function countRProductoMarcas(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRProductoMarcasPartial && !$this->isNew();
        if (null === $this->collRProductoMarcas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRProductoMarcas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRProductoMarcas());
            }
            $query = RProductoMarcaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProducto($this)
                ->count($con);
        }

        return count($this->collRProductoMarcas);
    }

    /**
     * Method called to associate a RProductoMarca object to this object
     * through the RProductoMarca foreign key attribute.
     *
     * @param    RProductoMarca $l RProductoMarca
     * @return Producto The current object (for fluent API support)
     */
    public function addRProductoMarca(RProductoMarca $l)
    {
        if ($this->collRProductoMarcas === null) {
            $this->initRProductoMarcas();
            $this->collRProductoMarcasPartial = true;
        }

        if (!in_array($l, $this->collRProductoMarcas->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRProductoMarca($l);

            if ($this->rProductoMarcasScheduledForDeletion and $this->rProductoMarcasScheduledForDeletion->contains($l)) {
                $this->rProductoMarcasScheduledForDeletion->remove($this->rProductoMarcasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RProductoMarca $rProductoMarca The rProductoMarca object to add.
     */
    protected function doAddRProductoMarca($rProductoMarca)
    {
        $this->collRProductoMarcas[]= $rProductoMarca;
        $rProductoMarca->setProducto($this);
    }

    /**
     * @param	RProductoMarca $rProductoMarca The rProductoMarca object to remove.
     * @return Producto The current object (for fluent API support)
     */
    public function removeRProductoMarca($rProductoMarca)
    {
        if ($this->getRProductoMarcas()->contains($rProductoMarca)) {
            $this->collRProductoMarcas->remove($this->collRProductoMarcas->search($rProductoMarca));
            if (null === $this->rProductoMarcasScheduledForDeletion) {
                $this->rProductoMarcasScheduledForDeletion = clone $this->collRProductoMarcas;
                $this->rProductoMarcasScheduledForDeletion->clear();
            }
            $this->rProductoMarcasScheduledForDeletion[]= clone $rProductoMarca;
            $rProductoMarca->setProducto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Producto is new, it will return
     * an empty collection; or if this Producto has previously
     * been saved, it will retrieve related RProductoMarcas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Producto.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RProductoMarca[] List of RProductoMarca objects
     */
    public function getRProductoMarcasJoinMarca($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RProductoMarcaQuery::create(null, $criteria);
        $query->joinWith('Marca', $join_behavior);

        return $this->getRProductoMarcas($query, $con);
    }

    /**
     * Clears out the collRProductoPrecios collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Producto The current object (for fluent API support)
     * @see        addRProductoPrecios()
     */
    public function clearRProductoPrecios()
    {
        $this->collRProductoPrecios = null; // important to set this to null since that means it is uninitialized
        $this->collRProductoPreciosPartial = null;

        return $this;
    }

    /**
     * reset is the collRProductoPrecios collection loaded partially
     *
     * @return void
     */
    public function resetPartialRProductoPrecios($v = true)
    {
        $this->collRProductoPreciosPartial = $v;
    }

    /**
     * Initializes the collRProductoPrecios collection.
     *
     * By default this just sets the collRProductoPrecios collection to an empty array (like clearcollRProductoPrecios());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRProductoPrecios($overrideExisting = true)
    {
        if (null !== $this->collRProductoPrecios && !$overrideExisting) {
            return;
        }
        $this->collRProductoPrecios = new PropelObjectCollection();
        $this->collRProductoPrecios->setModel('RProductoPrecio');
    }

    /**
     * Gets an array of RProductoPrecio objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Producto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RProductoPrecio[] List of RProductoPrecio objects
     * @throws PropelException
     */
    public function getRProductoPrecios($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRProductoPreciosPartial && !$this->isNew();
        if (null === $this->collRProductoPrecios || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRProductoPrecios) {
                // return empty collection
                $this->initRProductoPrecios();
            } else {
                $collRProductoPrecios = RProductoPrecioQuery::create(null, $criteria)
                    ->filterByProducto($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRProductoPreciosPartial && count($collRProductoPrecios)) {
                      $this->initRProductoPrecios(false);

                      foreach ($collRProductoPrecios as $obj) {
                        if (false == $this->collRProductoPrecios->contains($obj)) {
                          $this->collRProductoPrecios->append($obj);
                        }
                      }

                      $this->collRProductoPreciosPartial = true;
                    }

                    $collRProductoPrecios->getInternalIterator()->rewind();

                    return $collRProductoPrecios;
                }

                if ($partial && $this->collRProductoPrecios) {
                    foreach ($this->collRProductoPrecios as $obj) {
                        if ($obj->isNew()) {
                            $collRProductoPrecios[] = $obj;
                        }
                    }
                }

                $this->collRProductoPrecios = $collRProductoPrecios;
                $this->collRProductoPreciosPartial = false;
            }
        }

        return $this->collRProductoPrecios;
    }

    /**
     * Sets a collection of RProductoPrecio objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rProductoPrecios A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Producto The current object (for fluent API support)
     */
    public function setRProductoPrecios(PropelCollection $rProductoPrecios, PropelPDO $con = null)
    {
        $rProductoPreciosToDelete = $this->getRProductoPrecios(new Criteria(), $con)->diff($rProductoPrecios);


        $this->rProductoPreciosScheduledForDeletion = $rProductoPreciosToDelete;

        foreach ($rProductoPreciosToDelete as $rProductoPrecioRemoved) {
            $rProductoPrecioRemoved->setProducto(null);
        }

        $this->collRProductoPrecios = null;
        foreach ($rProductoPrecios as $rProductoPrecio) {
            $this->addRProductoPrecio($rProductoPrecio);
        }

        $this->collRProductoPrecios = $rProductoPrecios;
        $this->collRProductoPreciosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RProductoPrecio objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RProductoPrecio objects.
     * @throws PropelException
     */
    public function countRProductoPrecios(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRProductoPreciosPartial && !$this->isNew();
        if (null === $this->collRProductoPrecios || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRProductoPrecios) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRProductoPrecios());
            }
            $query = RProductoPrecioQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProducto($this)
                ->count($con);
        }

        return count($this->collRProductoPrecios);
    }

    /**
     * Method called to associate a RProductoPrecio object to this object
     * through the RProductoPrecio foreign key attribute.
     *
     * @param    RProductoPrecio $l RProductoPrecio
     * @return Producto The current object (for fluent API support)
     */
    public function addRProductoPrecio(RProductoPrecio $l)
    {
        if ($this->collRProductoPrecios === null) {
            $this->initRProductoPrecios();
            $this->collRProductoPreciosPartial = true;
        }

        if (!in_array($l, $this->collRProductoPrecios->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRProductoPrecio($l);

            if ($this->rProductoPreciosScheduledForDeletion and $this->rProductoPreciosScheduledForDeletion->contains($l)) {
                $this->rProductoPreciosScheduledForDeletion->remove($this->rProductoPreciosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RProductoPrecio $rProductoPrecio The rProductoPrecio object to add.
     */
    protected function doAddRProductoPrecio($rProductoPrecio)
    {
        $this->collRProductoPrecios[]= $rProductoPrecio;
        $rProductoPrecio->setProducto($this);
    }

    /**
     * @param	RProductoPrecio $rProductoPrecio The rProductoPrecio object to remove.
     * @return Producto The current object (for fluent API support)
     */
    public function removeRProductoPrecio($rProductoPrecio)
    {
        if ($this->getRProductoPrecios()->contains($rProductoPrecio)) {
            $this->collRProductoPrecios->remove($this->collRProductoPrecios->search($rProductoPrecio));
            if (null === $this->rProductoPreciosScheduledForDeletion) {
                $this->rProductoPreciosScheduledForDeletion = clone $this->collRProductoPrecios;
                $this->rProductoPreciosScheduledForDeletion->clear();
            }
            $this->rProductoPreciosScheduledForDeletion[]= clone $rProductoPrecio;
            $rProductoPrecio->setProducto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Producto is new, it will return
     * an empty collection; or if this Producto has previously
     * been saved, it will retrieve related RProductoPrecios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Producto.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RProductoPrecio[] List of RProductoPrecio objects
     */
    public function getRProductoPreciosJoinPrecio($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RProductoPrecioQuery::create(null, $criteria);
        $query->joinWith('Precio', $join_behavior);

        return $this->getRProductoPrecios($query, $con);
    }

    /**
     * Clears out the collRProveedorProductos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Producto The current object (for fluent API support)
     * @see        addRProveedorProductos()
     */
    public function clearRProveedorProductos()
    {
        $this->collRProveedorProductos = null; // important to set this to null since that means it is uninitialized
        $this->collRProveedorProductosPartial = null;

        return $this;
    }

    /**
     * reset is the collRProveedorProductos collection loaded partially
     *
     * @return void
     */
    public function resetPartialRProveedorProductos($v = true)
    {
        $this->collRProveedorProductosPartial = $v;
    }

    /**
     * Initializes the collRProveedorProductos collection.
     *
     * By default this just sets the collRProveedorProductos collection to an empty array (like clearcollRProveedorProductos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRProveedorProductos($overrideExisting = true)
    {
        if (null !== $this->collRProveedorProductos && !$overrideExisting) {
            return;
        }
        $this->collRProveedorProductos = new PropelObjectCollection();
        $this->collRProveedorProductos->setModel('RProveedorProducto');
    }

    /**
     * Gets an array of RProveedorProducto objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Producto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RProveedorProducto[] List of RProveedorProducto objects
     * @throws PropelException
     */
    public function getRProveedorProductos($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorProductosPartial && !$this->isNew();
        if (null === $this->collRProveedorProductos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRProveedorProductos) {
                // return empty collection
                $this->initRProveedorProductos();
            } else {
                $collRProveedorProductos = RProveedorProductoQuery::create(null, $criteria)
                    ->filterByProducto($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRProveedorProductosPartial && count($collRProveedorProductos)) {
                      $this->initRProveedorProductos(false);

                      foreach ($collRProveedorProductos as $obj) {
                        if (false == $this->collRProveedorProductos->contains($obj)) {
                          $this->collRProveedorProductos->append($obj);
                        }
                      }

                      $this->collRProveedorProductosPartial = true;
                    }

                    $collRProveedorProductos->getInternalIterator()->rewind();

                    return $collRProveedorProductos;
                }

                if ($partial && $this->collRProveedorProductos) {
                    foreach ($this->collRProveedorProductos as $obj) {
                        if ($obj->isNew()) {
                            $collRProveedorProductos[] = $obj;
                        }
                    }
                }

                $this->collRProveedorProductos = $collRProveedorProductos;
                $this->collRProveedorProductosPartial = false;
            }
        }

        return $this->collRProveedorProductos;
    }

    /**
     * Sets a collection of RProveedorProducto objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rProveedorProductos A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Producto The current object (for fluent API support)
     */
    public function setRProveedorProductos(PropelCollection $rProveedorProductos, PropelPDO $con = null)
    {
        $rProveedorProductosToDelete = $this->getRProveedorProductos(new Criteria(), $con)->diff($rProveedorProductos);


        $this->rProveedorProductosScheduledForDeletion = $rProveedorProductosToDelete;

        foreach ($rProveedorProductosToDelete as $rProveedorProductoRemoved) {
            $rProveedorProductoRemoved->setProducto(null);
        }

        $this->collRProveedorProductos = null;
        foreach ($rProveedorProductos as $rProveedorProducto) {
            $this->addRProveedorProducto($rProveedorProducto);
        }

        $this->collRProveedorProductos = $rProveedorProductos;
        $this->collRProveedorProductosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RProveedorProducto objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RProveedorProducto objects.
     * @throws PropelException
     */
    public function countRProveedorProductos(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorProductosPartial && !$this->isNew();
        if (null === $this->collRProveedorProductos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRProveedorProductos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRProveedorProductos());
            }
            $query = RProveedorProductoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProducto($this)
                ->count($con);
        }

        return count($this->collRProveedorProductos);
    }

    /**
     * Method called to associate a RProveedorProducto object to this object
     * through the RProveedorProducto foreign key attribute.
     *
     * @param    RProveedorProducto $l RProveedorProducto
     * @return Producto The current object (for fluent API support)
     */
    public function addRProveedorProducto(RProveedorProducto $l)
    {
        if ($this->collRProveedorProductos === null) {
            $this->initRProveedorProductos();
            $this->collRProveedorProductosPartial = true;
        }

        if (!in_array($l, $this->collRProveedorProductos->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRProveedorProducto($l);

            if ($this->rProveedorProductosScheduledForDeletion and $this->rProveedorProductosScheduledForDeletion->contains($l)) {
                $this->rProveedorProductosScheduledForDeletion->remove($this->rProveedorProductosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RProveedorProducto $rProveedorProducto The rProveedorProducto object to add.
     */
    protected function doAddRProveedorProducto($rProveedorProducto)
    {
        $this->collRProveedorProductos[]= $rProveedorProducto;
        $rProveedorProducto->setProducto($this);
    }

    /**
     * @param	RProveedorProducto $rProveedorProducto The rProveedorProducto object to remove.
     * @return Producto The current object (for fluent API support)
     */
    public function removeRProveedorProducto($rProveedorProducto)
    {
        if ($this->getRProveedorProductos()->contains($rProveedorProducto)) {
            $this->collRProveedorProductos->remove($this->collRProveedorProductos->search($rProveedorProducto));
            if (null === $this->rProveedorProductosScheduledForDeletion) {
                $this->rProveedorProductosScheduledForDeletion = clone $this->collRProveedorProductos;
                $this->rProveedorProductosScheduledForDeletion->clear();
            }
            $this->rProveedorProductosScheduledForDeletion[]= clone $rProveedorProducto;
            $rProveedorProducto->setProducto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Producto is new, it will return
     * an empty collection; or if this Producto has previously
     * been saved, it will retrieve related RProveedorProductos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Producto.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RProveedorProducto[] List of RProveedorProducto objects
     */
    public function getRProveedorProductosJoinProveedor($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RProveedorProductoQuery::create(null, $criteria);
        $query->joinWith('Proveedor', $join_behavior);

        return $this->getRProveedorProductos($query, $con);
    }

    /**
     * Clears out the collRUsuarioProductos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Producto The current object (for fluent API support)
     * @see        addRUsuarioProductos()
     */
    public function clearRUsuarioProductos()
    {
        $this->collRUsuarioProductos = null; // important to set this to null since that means it is uninitialized
        $this->collRUsuarioProductosPartial = null;

        return $this;
    }

    /**
     * reset is the collRUsuarioProductos collection loaded partially
     *
     * @return void
     */
    public function resetPartialRUsuarioProductos($v = true)
    {
        $this->collRUsuarioProductosPartial = $v;
    }

    /**
     * Initializes the collRUsuarioProductos collection.
     *
     * By default this just sets the collRUsuarioProductos collection to an empty array (like clearcollRUsuarioProductos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRUsuarioProductos($overrideExisting = true)
    {
        if (null !== $this->collRUsuarioProductos && !$overrideExisting) {
            return;
        }
        $this->collRUsuarioProductos = new PropelObjectCollection();
        $this->collRUsuarioProductos->setModel('RUsuarioProducto');
    }

    /**
     * Gets an array of RUsuarioProducto objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Producto is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RUsuarioProducto[] List of RUsuarioProducto objects
     * @throws PropelException
     */
    public function getRUsuarioProductos($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioProductosPartial && !$this->isNew();
        if (null === $this->collRUsuarioProductos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioProductos) {
                // return empty collection
                $this->initRUsuarioProductos();
            } else {
                $collRUsuarioProductos = RUsuarioProductoQuery::create(null, $criteria)
                    ->filterByProducto($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRUsuarioProductosPartial && count($collRUsuarioProductos)) {
                      $this->initRUsuarioProductos(false);

                      foreach ($collRUsuarioProductos as $obj) {
                        if (false == $this->collRUsuarioProductos->contains($obj)) {
                          $this->collRUsuarioProductos->append($obj);
                        }
                      }

                      $this->collRUsuarioProductosPartial = true;
                    }

                    $collRUsuarioProductos->getInternalIterator()->rewind();

                    return $collRUsuarioProductos;
                }

                if ($partial && $this->collRUsuarioProductos) {
                    foreach ($this->collRUsuarioProductos as $obj) {
                        if ($obj->isNew()) {
                            $collRUsuarioProductos[] = $obj;
                        }
                    }
                }

                $this->collRUsuarioProductos = $collRUsuarioProductos;
                $this->collRUsuarioProductosPartial = false;
            }
        }

        return $this->collRUsuarioProductos;
    }

    /**
     * Sets a collection of RUsuarioProducto objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rUsuarioProductos A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Producto The current object (for fluent API support)
     */
    public function setRUsuarioProductos(PropelCollection $rUsuarioProductos, PropelPDO $con = null)
    {
        $rUsuarioProductosToDelete = $this->getRUsuarioProductos(new Criteria(), $con)->diff($rUsuarioProductos);


        $this->rUsuarioProductosScheduledForDeletion = $rUsuarioProductosToDelete;

        foreach ($rUsuarioProductosToDelete as $rUsuarioProductoRemoved) {
            $rUsuarioProductoRemoved->setProducto(null);
        }

        $this->collRUsuarioProductos = null;
        foreach ($rUsuarioProductos as $rUsuarioProducto) {
            $this->addRUsuarioProducto($rUsuarioProducto);
        }

        $this->collRUsuarioProductos = $rUsuarioProductos;
        $this->collRUsuarioProductosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RUsuarioProducto objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RUsuarioProducto objects.
     * @throws PropelException
     */
    public function countRUsuarioProductos(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioProductosPartial && !$this->isNew();
        if (null === $this->collRUsuarioProductos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioProductos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRUsuarioProductos());
            }
            $query = RUsuarioProductoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProducto($this)
                ->count($con);
        }

        return count($this->collRUsuarioProductos);
    }

    /**
     * Method called to associate a RUsuarioProducto object to this object
     * through the RUsuarioProducto foreign key attribute.
     *
     * @param    RUsuarioProducto $l RUsuarioProducto
     * @return Producto The current object (for fluent API support)
     */
    public function addRUsuarioProducto(RUsuarioProducto $l)
    {
        if ($this->collRUsuarioProductos === null) {
            $this->initRUsuarioProductos();
            $this->collRUsuarioProductosPartial = true;
        }

        if (!in_array($l, $this->collRUsuarioProductos->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRUsuarioProducto($l);

            if ($this->rUsuarioProductosScheduledForDeletion and $this->rUsuarioProductosScheduledForDeletion->contains($l)) {
                $this->rUsuarioProductosScheduledForDeletion->remove($this->rUsuarioProductosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RUsuarioProducto $rUsuarioProducto The rUsuarioProducto object to add.
     */
    protected function doAddRUsuarioProducto($rUsuarioProducto)
    {
        $this->collRUsuarioProductos[]= $rUsuarioProducto;
        $rUsuarioProducto->setProducto($this);
    }

    /**
     * @param	RUsuarioProducto $rUsuarioProducto The rUsuarioProducto object to remove.
     * @return Producto The current object (for fluent API support)
     */
    public function removeRUsuarioProducto($rUsuarioProducto)
    {
        if ($this->getRUsuarioProductos()->contains($rUsuarioProducto)) {
            $this->collRUsuarioProductos->remove($this->collRUsuarioProductos->search($rUsuarioProducto));
            if (null === $this->rUsuarioProductosScheduledForDeletion) {
                $this->rUsuarioProductosScheduledForDeletion = clone $this->collRUsuarioProductos;
                $this->rUsuarioProductosScheduledForDeletion->clear();
            }
            $this->rUsuarioProductosScheduledForDeletion[]= clone $rUsuarioProducto;
            $rUsuarioProducto->setProducto(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Producto is new, it will return
     * an empty collection; or if this Producto has previously
     * been saved, it will retrieve related RUsuarioProductos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Producto.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RUsuarioProducto[] List of RUsuarioProducto objects
     */
    public function getRUsuarioProductosJoinUsuario($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RUsuarioProductoQuery::create(null, $criteria);
        $query->joinWith('Usuario', $join_behavior);

        return $this->getRUsuarioProductos($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->nombre = null;
        $this->descripcion = null;
        $this->codigo = null;
        $this->imagen = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collRComercioProductos) {
                foreach ($this->collRComercioProductos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRProductoCategoriaproductoss) {
                foreach ($this->collRProductoCategoriaproductoss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRProductoEmpaques) {
                foreach ($this->collRProductoEmpaques as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRProductoEnvases) {
                foreach ($this->collRProductoEnvases as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRProductoMarcas) {
                foreach ($this->collRProductoMarcas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRProductoPrecios) {
                foreach ($this->collRProductoPrecios as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRProveedorProductos) {
                foreach ($this->collRProveedorProductos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRUsuarioProductos) {
                foreach ($this->collRUsuarioProductos as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collRComercioProductos instanceof PropelCollection) {
            $this->collRComercioProductos->clearIterator();
        }
        $this->collRComercioProductos = null;
        if ($this->collRProductoCategoriaproductoss instanceof PropelCollection) {
            $this->collRProductoCategoriaproductoss->clearIterator();
        }
        $this->collRProductoCategoriaproductoss = null;
        if ($this->collRProductoEmpaques instanceof PropelCollection) {
            $this->collRProductoEmpaques->clearIterator();
        }
        $this->collRProductoEmpaques = null;
        if ($this->collRProductoEnvases instanceof PropelCollection) {
            $this->collRProductoEnvases->clearIterator();
        }
        $this->collRProductoEnvases = null;
        if ($this->collRProductoMarcas instanceof PropelCollection) {
            $this->collRProductoMarcas->clearIterator();
        }
        $this->collRProductoMarcas = null;
        if ($this->collRProductoPrecios instanceof PropelCollection) {
            $this->collRProductoPrecios->clearIterator();
        }
        $this->collRProductoPrecios = null;
        if ($this->collRProveedorProductos instanceof PropelCollection) {
            $this->collRProveedorProductos->clearIterator();
        }
        $this->collRProveedorProductos = null;
        if ($this->collRUsuarioProductos instanceof PropelCollection) {
            $this->collRUsuarioProductos->clearIterator();
        }
        $this->collRUsuarioProductos = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ProductoPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
