<?php


/**
 * Base class that represents a query for the 'producto' table.
 *
 *
 *
 * @method ProductoQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ProductoQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 * @method ProductoQuery orderByDescripcion($order = Criteria::ASC) Order by the descripcion column
 * @method ProductoQuery orderByCodigo($order = Criteria::ASC) Order by the codigo column
 * @method ProductoQuery orderByImagen($order = Criteria::ASC) Order by the imagen column
 *
 * @method ProductoQuery groupById() Group by the id column
 * @method ProductoQuery groupByNombre() Group by the nombre column
 * @method ProductoQuery groupByDescripcion() Group by the descripcion column
 * @method ProductoQuery groupByCodigo() Group by the codigo column
 * @method ProductoQuery groupByImagen() Group by the imagen column
 *
 * @method ProductoQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ProductoQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ProductoQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ProductoQuery leftJoinRComercioProducto($relationAlias = null) Adds a LEFT JOIN clause to the query using the RComercioProducto relation
 * @method ProductoQuery rightJoinRComercioProducto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RComercioProducto relation
 * @method ProductoQuery innerJoinRComercioProducto($relationAlias = null) Adds a INNER JOIN clause to the query using the RComercioProducto relation
 *
 * @method ProductoQuery leftJoinRProductoCategoriaproductos($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProductoCategoriaproductos relation
 * @method ProductoQuery rightJoinRProductoCategoriaproductos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProductoCategoriaproductos relation
 * @method ProductoQuery innerJoinRProductoCategoriaproductos($relationAlias = null) Adds a INNER JOIN clause to the query using the RProductoCategoriaproductos relation
 *
 * @method ProductoQuery leftJoinRProductoEmpaque($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProductoEmpaque relation
 * @method ProductoQuery rightJoinRProductoEmpaque($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProductoEmpaque relation
 * @method ProductoQuery innerJoinRProductoEmpaque($relationAlias = null) Adds a INNER JOIN clause to the query using the RProductoEmpaque relation
 *
 * @method ProductoQuery leftJoinRProductoEnvase($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProductoEnvase relation
 * @method ProductoQuery rightJoinRProductoEnvase($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProductoEnvase relation
 * @method ProductoQuery innerJoinRProductoEnvase($relationAlias = null) Adds a INNER JOIN clause to the query using the RProductoEnvase relation
 *
 * @method ProductoQuery leftJoinRProductoMarca($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProductoMarca relation
 * @method ProductoQuery rightJoinRProductoMarca($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProductoMarca relation
 * @method ProductoQuery innerJoinRProductoMarca($relationAlias = null) Adds a INNER JOIN clause to the query using the RProductoMarca relation
 *
 * @method ProductoQuery leftJoinRProductoPrecio($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProductoPrecio relation
 * @method ProductoQuery rightJoinRProductoPrecio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProductoPrecio relation
 * @method ProductoQuery innerJoinRProductoPrecio($relationAlias = null) Adds a INNER JOIN clause to the query using the RProductoPrecio relation
 *
 * @method ProductoQuery leftJoinRProveedorProducto($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProveedorProducto relation
 * @method ProductoQuery rightJoinRProveedorProducto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProveedorProducto relation
 * @method ProductoQuery innerJoinRProveedorProducto($relationAlias = null) Adds a INNER JOIN clause to the query using the RProveedorProducto relation
 *
 * @method ProductoQuery leftJoinRUsuarioProducto($relationAlias = null) Adds a LEFT JOIN clause to the query using the RUsuarioProducto relation
 * @method ProductoQuery rightJoinRUsuarioProducto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RUsuarioProducto relation
 * @method ProductoQuery innerJoinRUsuarioProducto($relationAlias = null) Adds a INNER JOIN clause to the query using the RUsuarioProducto relation
 *
 * @method Producto findOne(PropelPDO $con = null) Return the first Producto matching the query
 * @method Producto findOneOrCreate(PropelPDO $con = null) Return the first Producto matching the query, or a new Producto object populated from the query conditions when no match is found
 *
 * @method Producto findOneByNombre(string $nombre) Return the first Producto filtered by the nombre column
 * @method Producto findOneByDescripcion(string $descripcion) Return the first Producto filtered by the descripcion column
 * @method Producto findOneByCodigo(string $codigo) Return the first Producto filtered by the codigo column
 * @method Producto findOneByImagen(string $imagen) Return the first Producto filtered by the imagen column
 *
 * @method array findById(int $id) Return Producto objects filtered by the id column
 * @method array findByNombre(string $nombre) Return Producto objects filtered by the nombre column
 * @method array findByDescripcion(string $descripcion) Return Producto objects filtered by the descripcion column
 * @method array findByCodigo(string $codigo) Return Producto objects filtered by the codigo column
 * @method array findByImagen(string $imagen) Return Producto objects filtered by the imagen column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseProductoQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseProductoQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'Producto';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ProductoQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ProductoQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ProductoQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ProductoQuery) {
            return $criteria;
        }
        $query = new ProductoQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Producto|Producto[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ProductoPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ProductoPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Producto A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Producto A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `nombre`, `descripcion`, `codigo`, `imagen` FROM `producto` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Producto();
            $obj->hydrate($row);
            ProductoPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Producto|Producto[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Producto[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProductoPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProductoPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ProductoPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ProductoPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProductoPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%'); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nombre)) {
                $nombre = str_replace('*', '%', $nombre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductoPeer::NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query on the descripcion column
     *
     * Example usage:
     * <code>
     * $query->filterByDescripcion('fooValue');   // WHERE descripcion = 'fooValue'
     * $query->filterByDescripcion('%fooValue%'); // WHERE descripcion LIKE '%fooValue%'
     * </code>
     *
     * @param     string $descripcion The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function filterByDescripcion($descripcion = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($descripcion)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $descripcion)) {
                $descripcion = str_replace('*', '%', $descripcion);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductoPeer::DESCRIPCION, $descripcion, $comparison);
    }

    /**
     * Filter the query on the codigo column
     *
     * Example usage:
     * <code>
     * $query->filterByCodigo('fooValue');   // WHERE codigo = 'fooValue'
     * $query->filterByCodigo('%fooValue%'); // WHERE codigo LIKE '%fooValue%'
     * </code>
     *
     * @param     string $codigo The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function filterByCodigo($codigo = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($codigo)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $codigo)) {
                $codigo = str_replace('*', '%', $codigo);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductoPeer::CODIGO, $codigo, $comparison);
    }

    /**
     * Filter the query on the imagen column
     *
     * Example usage:
     * <code>
     * $query->filterByImagen('fooValue');   // WHERE imagen = 'fooValue'
     * $query->filterByImagen('%fooValue%'); // WHERE imagen LIKE '%fooValue%'
     * </code>
     *
     * @param     string $imagen The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function filterByImagen($imagen = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($imagen)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $imagen)) {
                $imagen = str_replace('*', '%', $imagen);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProductoPeer::IMAGEN, $imagen, $comparison);
    }

    /**
     * Filter the query by a related RComercioProducto object
     *
     * @param   RComercioProducto|PropelObjectCollection $rComercioProducto  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProductoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRComercioProducto($rComercioProducto, $comparison = null)
    {
        if ($rComercioProducto instanceof RComercioProducto) {
            return $this
                ->addUsingAlias(ProductoPeer::ID, $rComercioProducto->getProductoId(), $comparison);
        } elseif ($rComercioProducto instanceof PropelObjectCollection) {
            return $this
                ->useRComercioProductoQuery()
                ->filterByPrimaryKeys($rComercioProducto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRComercioProducto() only accepts arguments of type RComercioProducto or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RComercioProducto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function joinRComercioProducto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RComercioProducto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RComercioProducto');
        }

        return $this;
    }

    /**
     * Use the RComercioProducto relation RComercioProducto object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RComercioProductoQuery A secondary query class using the current class as primary query
     */
    public function useRComercioProductoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRComercioProducto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RComercioProducto', 'RComercioProductoQuery');
    }

    /**
     * Filter the query by a related RProductoCategoriaproductos object
     *
     * @param   RProductoCategoriaproductos|PropelObjectCollection $rProductoCategoriaproductos  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProductoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProductoCategoriaproductos($rProductoCategoriaproductos, $comparison = null)
    {
        if ($rProductoCategoriaproductos instanceof RProductoCategoriaproductos) {
            return $this
                ->addUsingAlias(ProductoPeer::ID, $rProductoCategoriaproductos->getProductoId(), $comparison);
        } elseif ($rProductoCategoriaproductos instanceof PropelObjectCollection) {
            return $this
                ->useRProductoCategoriaproductosQuery()
                ->filterByPrimaryKeys($rProductoCategoriaproductos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProductoCategoriaproductos() only accepts arguments of type RProductoCategoriaproductos or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProductoCategoriaproductos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function joinRProductoCategoriaproductos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProductoCategoriaproductos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProductoCategoriaproductos');
        }

        return $this;
    }

    /**
     * Use the RProductoCategoriaproductos relation RProductoCategoriaproductos object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProductoCategoriaproductosQuery A secondary query class using the current class as primary query
     */
    public function useRProductoCategoriaproductosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProductoCategoriaproductos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProductoCategoriaproductos', 'RProductoCategoriaproductosQuery');
    }

    /**
     * Filter the query by a related RProductoEmpaque object
     *
     * @param   RProductoEmpaque|PropelObjectCollection $rProductoEmpaque  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProductoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProductoEmpaque($rProductoEmpaque, $comparison = null)
    {
        if ($rProductoEmpaque instanceof RProductoEmpaque) {
            return $this
                ->addUsingAlias(ProductoPeer::ID, $rProductoEmpaque->getProductoId(), $comparison);
        } elseif ($rProductoEmpaque instanceof PropelObjectCollection) {
            return $this
                ->useRProductoEmpaqueQuery()
                ->filterByPrimaryKeys($rProductoEmpaque->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProductoEmpaque() only accepts arguments of type RProductoEmpaque or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProductoEmpaque relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function joinRProductoEmpaque($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProductoEmpaque');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProductoEmpaque');
        }

        return $this;
    }

    /**
     * Use the RProductoEmpaque relation RProductoEmpaque object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProductoEmpaqueQuery A secondary query class using the current class as primary query
     */
    public function useRProductoEmpaqueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProductoEmpaque($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProductoEmpaque', 'RProductoEmpaqueQuery');
    }

    /**
     * Filter the query by a related RProductoEnvase object
     *
     * @param   RProductoEnvase|PropelObjectCollection $rProductoEnvase  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProductoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProductoEnvase($rProductoEnvase, $comparison = null)
    {
        if ($rProductoEnvase instanceof RProductoEnvase) {
            return $this
                ->addUsingAlias(ProductoPeer::ID, $rProductoEnvase->getProductoId(), $comparison);
        } elseif ($rProductoEnvase instanceof PropelObjectCollection) {
            return $this
                ->useRProductoEnvaseQuery()
                ->filterByPrimaryKeys($rProductoEnvase->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProductoEnvase() only accepts arguments of type RProductoEnvase or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProductoEnvase relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function joinRProductoEnvase($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProductoEnvase');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProductoEnvase');
        }

        return $this;
    }

    /**
     * Use the RProductoEnvase relation RProductoEnvase object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProductoEnvaseQuery A secondary query class using the current class as primary query
     */
    public function useRProductoEnvaseQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProductoEnvase($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProductoEnvase', 'RProductoEnvaseQuery');
    }

    /**
     * Filter the query by a related RProductoMarca object
     *
     * @param   RProductoMarca|PropelObjectCollection $rProductoMarca  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProductoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProductoMarca($rProductoMarca, $comparison = null)
    {
        if ($rProductoMarca instanceof RProductoMarca) {
            return $this
                ->addUsingAlias(ProductoPeer::ID, $rProductoMarca->getProductoId(), $comparison);
        } elseif ($rProductoMarca instanceof PropelObjectCollection) {
            return $this
                ->useRProductoMarcaQuery()
                ->filterByPrimaryKeys($rProductoMarca->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProductoMarca() only accepts arguments of type RProductoMarca or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProductoMarca relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function joinRProductoMarca($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProductoMarca');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProductoMarca');
        }

        return $this;
    }

    /**
     * Use the RProductoMarca relation RProductoMarca object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProductoMarcaQuery A secondary query class using the current class as primary query
     */
    public function useRProductoMarcaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProductoMarca($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProductoMarca', 'RProductoMarcaQuery');
    }

    /**
     * Filter the query by a related RProductoPrecio object
     *
     * @param   RProductoPrecio|PropelObjectCollection $rProductoPrecio  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProductoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProductoPrecio($rProductoPrecio, $comparison = null)
    {
        if ($rProductoPrecio instanceof RProductoPrecio) {
            return $this
                ->addUsingAlias(ProductoPeer::ID, $rProductoPrecio->getProductoId(), $comparison);
        } elseif ($rProductoPrecio instanceof PropelObjectCollection) {
            return $this
                ->useRProductoPrecioQuery()
                ->filterByPrimaryKeys($rProductoPrecio->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProductoPrecio() only accepts arguments of type RProductoPrecio or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProductoPrecio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function joinRProductoPrecio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProductoPrecio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProductoPrecio');
        }

        return $this;
    }

    /**
     * Use the RProductoPrecio relation RProductoPrecio object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProductoPrecioQuery A secondary query class using the current class as primary query
     */
    public function useRProductoPrecioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProductoPrecio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProductoPrecio', 'RProductoPrecioQuery');
    }

    /**
     * Filter the query by a related RProveedorProducto object
     *
     * @param   RProveedorProducto|PropelObjectCollection $rProveedorProducto  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProductoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProveedorProducto($rProveedorProducto, $comparison = null)
    {
        if ($rProveedorProducto instanceof RProveedorProducto) {
            return $this
                ->addUsingAlias(ProductoPeer::ID, $rProveedorProducto->getProductoId(), $comparison);
        } elseif ($rProveedorProducto instanceof PropelObjectCollection) {
            return $this
                ->useRProveedorProductoQuery()
                ->filterByPrimaryKeys($rProveedorProducto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProveedorProducto() only accepts arguments of type RProveedorProducto or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProveedorProducto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function joinRProveedorProducto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProveedorProducto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProveedorProducto');
        }

        return $this;
    }

    /**
     * Use the RProveedorProducto relation RProveedorProducto object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProveedorProductoQuery A secondary query class using the current class as primary query
     */
    public function useRProveedorProductoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProveedorProducto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProveedorProducto', 'RProveedorProductoQuery');
    }

    /**
     * Filter the query by a related RUsuarioProducto object
     *
     * @param   RUsuarioProducto|PropelObjectCollection $rUsuarioProducto  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProductoQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRUsuarioProducto($rUsuarioProducto, $comparison = null)
    {
        if ($rUsuarioProducto instanceof RUsuarioProducto) {
            return $this
                ->addUsingAlias(ProductoPeer::ID, $rUsuarioProducto->getProductoId(), $comparison);
        } elseif ($rUsuarioProducto instanceof PropelObjectCollection) {
            return $this
                ->useRUsuarioProductoQuery()
                ->filterByPrimaryKeys($rUsuarioProducto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRUsuarioProducto() only accepts arguments of type RUsuarioProducto or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RUsuarioProducto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function joinRUsuarioProducto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RUsuarioProducto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RUsuarioProducto');
        }

        return $this;
    }

    /**
     * Use the RUsuarioProducto relation RUsuarioProducto object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RUsuarioProductoQuery A secondary query class using the current class as primary query
     */
    public function useRUsuarioProductoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRUsuarioProducto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RUsuarioProducto', 'RUsuarioProductoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Producto $producto Object to remove from the list of results
     *
     * @return ProductoQuery The current query, for fluid interface
     */
    public function prune($producto = null)
    {
        if ($producto) {
            $this->addUsingAlias(ProductoPeer::ID, $producto->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
