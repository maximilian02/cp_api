<?php


/**
 * Base class that represents a row from the 'proveedor' table.
 *
 *
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseProveedor extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'ProveedorPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        ProveedorPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the email field.
     * @var        string
     */
    protected $email;

    /**
     * The value for the nombre_contacto field.
     * @var        string
     */
    protected $nombre_contacto;

    /**
     * The value for the preferencia_pago field.
     * @var        string
     */
    protected $preferencia_pago;

    /**
     * The value for the pedido_minimo field.
     * @var        int
     */
    protected $pedido_minimo;

    /**
     * @var        PropelObjectCollection|RComercioProveedor[] Collection to store aggregation of RComercioProveedor objects.
     */
    protected $collRComercioProveedors;
    protected $collRComercioProveedorsPartial;

    /**
     * @var        PropelObjectCollection|RProveedorCategoriaproductos[] Collection to store aggregation of RProveedorCategoriaproductos objects.
     */
    protected $collRProveedorCategoriaproductoss;
    protected $collRProveedorCategoriaproductossPartial;

    /**
     * @var        PropelObjectCollection|RProveedorListaprecios[] Collection to store aggregation of RProveedorListaprecios objects.
     */
    protected $collRProveedorListaprecioss;
    protected $collRProveedorListapreciossPartial;

    /**
     * @var        PropelObjectCollection|RProveedorProducto[] Collection to store aggregation of RProveedorProducto objects.
     */
    protected $collRProveedorProductos;
    protected $collRProveedorProductosPartial;

    /**
     * @var        PropelObjectCollection|RProveedorReputacion[] Collection to store aggregation of RProveedorReputacion objects.
     */
    protected $collRProveedorReputacions;
    protected $collRProveedorReputacionsPartial;

    /**
     * @var        PropelObjectCollection|RProveedorZonaentrega[] Collection to store aggregation of RProveedorZonaentrega objects.
     */
    protected $collRProveedorZonaentregas;
    protected $collRProveedorZonaentregasPartial;

    /**
     * @var        PropelObjectCollection|RUsuarioProveedor[] Collection to store aggregation of RUsuarioProveedor objects.
     */
    protected $collRUsuarioProveedors;
    protected $collRUsuarioProveedorsPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rComercioProveedorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rProveedorCategoriaproductossScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rProveedorListapreciossScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rProveedorProductosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rProveedorReputacionsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rProveedorZonaentregasScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rUsuarioProveedorsScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [email] column value.
     *
     * @return string
     */
    public function getEmail()
    {

        return $this->email;
    }

    /**
     * Get the [nombre_contacto] column value.
     *
     * @return string
     */
    public function getNombreContacto()
    {

        return $this->nombre_contacto;
    }

    /**
     * Get the [preferencia_pago] column value.
     *
     * @return string
     */
    public function getPreferenciaPago()
    {

        return $this->preferencia_pago;
    }

    /**
     * Get the [pedido_minimo] column value.
     *
     * @return int
     */
    public function getPedidoMinimo()
    {

        return $this->pedido_minimo;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Proveedor The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = ProveedorPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [email] column.
     *
     * @param  string $v new value
     * @return Proveedor The current object (for fluent API support)
     */
    public function setEmail($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->email !== $v) {
            $this->email = $v;
            $this->modifiedColumns[] = ProveedorPeer::EMAIL;
        }


        return $this;
    } // setEmail()

    /**
     * Set the value of [nombre_contacto] column.
     *
     * @param  string $v new value
     * @return Proveedor The current object (for fluent API support)
     */
    public function setNombreContacto($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->nombre_contacto !== $v) {
            $this->nombre_contacto = $v;
            $this->modifiedColumns[] = ProveedorPeer::NOMBRE_CONTACTO;
        }


        return $this;
    } // setNombreContacto()

    /**
     * Set the value of [preferencia_pago] column.
     *
     * @param  string $v new value
     * @return Proveedor The current object (for fluent API support)
     */
    public function setPreferenciaPago($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->preferencia_pago !== $v) {
            $this->preferencia_pago = $v;
            $this->modifiedColumns[] = ProveedorPeer::PREFERENCIA_PAGO;
        }


        return $this;
    } // setPreferenciaPago()

    /**
     * Set the value of [pedido_minimo] column.
     *
     * @param  int $v new value
     * @return Proveedor The current object (for fluent API support)
     */
    public function setPedidoMinimo($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->pedido_minimo !== $v) {
            $this->pedido_minimo = $v;
            $this->modifiedColumns[] = ProveedorPeer::PEDIDO_MINIMO;
        }


        return $this;
    } // setPedidoMinimo()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->email = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->nombre_contacto = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->preferencia_pago = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->pedido_minimo = ($row[$startcol + 4] !== null) ? (int) $row[$startcol + 4] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 5; // 5 = ProveedorPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Proveedor object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ProveedorPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = ProveedorPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collRComercioProveedors = null;

            $this->collRProveedorCategoriaproductoss = null;

            $this->collRProveedorListaprecioss = null;

            $this->collRProveedorProductos = null;

            $this->collRProveedorReputacions = null;

            $this->collRProveedorZonaentregas = null;

            $this->collRUsuarioProveedors = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ProveedorPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = ProveedorQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(ProveedorPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                ProveedorPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->rComercioProveedorsScheduledForDeletion !== null) {
                if (!$this->rComercioProveedorsScheduledForDeletion->isEmpty()) {
                    RComercioProveedorQuery::create()
                        ->filterByPrimaryKeys($this->rComercioProveedorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rComercioProveedorsScheduledForDeletion = null;
                }
            }

            if ($this->collRComercioProveedors !== null) {
                foreach ($this->collRComercioProveedors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rProveedorCategoriaproductossScheduledForDeletion !== null) {
                if (!$this->rProveedorCategoriaproductossScheduledForDeletion->isEmpty()) {
                    RProveedorCategoriaproductosQuery::create()
                        ->filterByPrimaryKeys($this->rProveedorCategoriaproductossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rProveedorCategoriaproductossScheduledForDeletion = null;
                }
            }

            if ($this->collRProveedorCategoriaproductoss !== null) {
                foreach ($this->collRProveedorCategoriaproductoss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rProveedorListapreciossScheduledForDeletion !== null) {
                if (!$this->rProveedorListapreciossScheduledForDeletion->isEmpty()) {
                    RProveedorListapreciosQuery::create()
                        ->filterByPrimaryKeys($this->rProveedorListapreciossScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rProveedorListapreciossScheduledForDeletion = null;
                }
            }

            if ($this->collRProveedorListaprecioss !== null) {
                foreach ($this->collRProveedorListaprecioss as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rProveedorProductosScheduledForDeletion !== null) {
                if (!$this->rProveedorProductosScheduledForDeletion->isEmpty()) {
                    RProveedorProductoQuery::create()
                        ->filterByPrimaryKeys($this->rProveedorProductosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rProveedorProductosScheduledForDeletion = null;
                }
            }

            if ($this->collRProveedorProductos !== null) {
                foreach ($this->collRProveedorProductos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rProveedorReputacionsScheduledForDeletion !== null) {
                if (!$this->rProveedorReputacionsScheduledForDeletion->isEmpty()) {
                    RProveedorReputacionQuery::create()
                        ->filterByPrimaryKeys($this->rProveedorReputacionsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rProveedorReputacionsScheduledForDeletion = null;
                }
            }

            if ($this->collRProveedorReputacions !== null) {
                foreach ($this->collRProveedorReputacions as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rProveedorZonaentregasScheduledForDeletion !== null) {
                if (!$this->rProveedorZonaentregasScheduledForDeletion->isEmpty()) {
                    RProveedorZonaentregaQuery::create()
                        ->filterByPrimaryKeys($this->rProveedorZonaentregasScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rProveedorZonaentregasScheduledForDeletion = null;
                }
            }

            if ($this->collRProveedorZonaentregas !== null) {
                foreach ($this->collRProveedorZonaentregas as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rUsuarioProveedorsScheduledForDeletion !== null) {
                if (!$this->rUsuarioProveedorsScheduledForDeletion->isEmpty()) {
                    RUsuarioProveedorQuery::create()
                        ->filterByPrimaryKeys($this->rUsuarioProveedorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rUsuarioProveedorsScheduledForDeletion = null;
                }
            }

            if ($this->collRUsuarioProveedors !== null) {
                foreach ($this->collRUsuarioProveedors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = ProveedorPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . ProveedorPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(ProveedorPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(ProveedorPeer::EMAIL)) {
            $modifiedColumns[':p' . $index++]  = '`email`';
        }
        if ($this->isColumnModified(ProveedorPeer::NOMBRE_CONTACTO)) {
            $modifiedColumns[':p' . $index++]  = '`nombre_contacto`';
        }
        if ($this->isColumnModified(ProveedorPeer::PREFERENCIA_PAGO)) {
            $modifiedColumns[':p' . $index++]  = '`preferencia_pago`';
        }
        if ($this->isColumnModified(ProveedorPeer::PEDIDO_MINIMO)) {
            $modifiedColumns[':p' . $index++]  = '`pedido_minimo`';
        }

        $sql = sprintf(
            'INSERT INTO `proveedor` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`email`':
                        $stmt->bindValue($identifier, $this->email, PDO::PARAM_STR);
                        break;
                    case '`nombre_contacto`':
                        $stmt->bindValue($identifier, $this->nombre_contacto, PDO::PARAM_STR);
                        break;
                    case '`preferencia_pago`':
                        $stmt->bindValue($identifier, $this->preferencia_pago, PDO::PARAM_STR);
                        break;
                    case '`pedido_minimo`':
                        $stmt->bindValue($identifier, $this->pedido_minimo, PDO::PARAM_INT);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = ProveedorPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collRComercioProveedors !== null) {
                    foreach ($this->collRComercioProveedors as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRProveedorCategoriaproductoss !== null) {
                    foreach ($this->collRProveedorCategoriaproductoss as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRProveedorListaprecioss !== null) {
                    foreach ($this->collRProveedorListaprecioss as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRProveedorProductos !== null) {
                    foreach ($this->collRProveedorProductos as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRProveedorReputacions !== null) {
                    foreach ($this->collRProveedorReputacions as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRProveedorZonaentregas !== null) {
                    foreach ($this->collRProveedorZonaentregas as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRUsuarioProveedors !== null) {
                    foreach ($this->collRUsuarioProveedors as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ProveedorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getEmail();
                break;
            case 2:
                return $this->getNombreContacto();
                break;
            case 3:
                return $this->getPreferenciaPago();
                break;
            case 4:
                return $this->getPedidoMinimo();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Proveedor'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Proveedor'][$this->getPrimaryKey()] = true;
        $keys = ProveedorPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getEmail(),
            $keys[2] => $this->getNombreContacto(),
            $keys[3] => $this->getPreferenciaPago(),
            $keys[4] => $this->getPedidoMinimo(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collRComercioProveedors) {
                $result['RComercioProveedors'] = $this->collRComercioProveedors->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRProveedorCategoriaproductoss) {
                $result['RProveedorCategoriaproductoss'] = $this->collRProveedorCategoriaproductoss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRProveedorListaprecioss) {
                $result['RProveedorListaprecioss'] = $this->collRProveedorListaprecioss->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRProveedorProductos) {
                $result['RProveedorProductos'] = $this->collRProveedorProductos->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRProveedorReputacions) {
                $result['RProveedorReputacions'] = $this->collRProveedorReputacions->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRProveedorZonaentregas) {
                $result['RProveedorZonaentregas'] = $this->collRProveedorZonaentregas->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRUsuarioProveedors) {
                $result['RUsuarioProveedors'] = $this->collRUsuarioProveedors->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = ProveedorPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setEmail($value);
                break;
            case 2:
                $this->setNombreContacto($value);
                break;
            case 3:
                $this->setPreferenciaPago($value);
                break;
            case 4:
                $this->setPedidoMinimo($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = ProveedorPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setEmail($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setNombreContacto($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setPreferenciaPago($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setPedidoMinimo($arr[$keys[4]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(ProveedorPeer::DATABASE_NAME);

        if ($this->isColumnModified(ProveedorPeer::ID)) $criteria->add(ProveedorPeer::ID, $this->id);
        if ($this->isColumnModified(ProveedorPeer::EMAIL)) $criteria->add(ProveedorPeer::EMAIL, $this->email);
        if ($this->isColumnModified(ProveedorPeer::NOMBRE_CONTACTO)) $criteria->add(ProveedorPeer::NOMBRE_CONTACTO, $this->nombre_contacto);
        if ($this->isColumnModified(ProveedorPeer::PREFERENCIA_PAGO)) $criteria->add(ProveedorPeer::PREFERENCIA_PAGO, $this->preferencia_pago);
        if ($this->isColumnModified(ProveedorPeer::PEDIDO_MINIMO)) $criteria->add(ProveedorPeer::PEDIDO_MINIMO, $this->pedido_minimo);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(ProveedorPeer::DATABASE_NAME);
        $criteria->add(ProveedorPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Proveedor (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setEmail($this->getEmail());
        $copyObj->setNombreContacto($this->getNombreContacto());
        $copyObj->setPreferenciaPago($this->getPreferenciaPago());
        $copyObj->setPedidoMinimo($this->getPedidoMinimo());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getRComercioProveedors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRComercioProveedor($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRProveedorCategoriaproductoss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRProveedorCategoriaproductos($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRProveedorListaprecioss() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRProveedorListaprecios($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRProveedorProductos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRProveedorProducto($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRProveedorReputacions() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRProveedorReputacion($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRProveedorZonaentregas() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRProveedorZonaentrega($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRUsuarioProveedors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRUsuarioProveedor($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Proveedor Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return ProveedorPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new ProveedorPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('RComercioProveedor' == $relationName) {
            $this->initRComercioProveedors();
        }
        if ('RProveedorCategoriaproductos' == $relationName) {
            $this->initRProveedorCategoriaproductoss();
        }
        if ('RProveedorListaprecios' == $relationName) {
            $this->initRProveedorListaprecioss();
        }
        if ('RProveedorProducto' == $relationName) {
            $this->initRProveedorProductos();
        }
        if ('RProveedorReputacion' == $relationName) {
            $this->initRProveedorReputacions();
        }
        if ('RProveedorZonaentrega' == $relationName) {
            $this->initRProveedorZonaentregas();
        }
        if ('RUsuarioProveedor' == $relationName) {
            $this->initRUsuarioProveedors();
        }
    }

    /**
     * Clears out the collRComercioProveedors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Proveedor The current object (for fluent API support)
     * @see        addRComercioProveedors()
     */
    public function clearRComercioProveedors()
    {
        $this->collRComercioProveedors = null; // important to set this to null since that means it is uninitialized
        $this->collRComercioProveedorsPartial = null;

        return $this;
    }

    /**
     * reset is the collRComercioProveedors collection loaded partially
     *
     * @return void
     */
    public function resetPartialRComercioProveedors($v = true)
    {
        $this->collRComercioProveedorsPartial = $v;
    }

    /**
     * Initializes the collRComercioProveedors collection.
     *
     * By default this just sets the collRComercioProveedors collection to an empty array (like clearcollRComercioProveedors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRComercioProveedors($overrideExisting = true)
    {
        if (null !== $this->collRComercioProveedors && !$overrideExisting) {
            return;
        }
        $this->collRComercioProveedors = new PropelObjectCollection();
        $this->collRComercioProveedors->setModel('RComercioProveedor');
    }

    /**
     * Gets an array of RComercioProveedor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Proveedor is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RComercioProveedor[] List of RComercioProveedor objects
     * @throws PropelException
     */
    public function getRComercioProveedors($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRComercioProveedorsPartial && !$this->isNew();
        if (null === $this->collRComercioProveedors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRComercioProveedors) {
                // return empty collection
                $this->initRComercioProveedors();
            } else {
                $collRComercioProveedors = RComercioProveedorQuery::create(null, $criteria)
                    ->filterByProveedor($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRComercioProveedorsPartial && count($collRComercioProveedors)) {
                      $this->initRComercioProveedors(false);

                      foreach ($collRComercioProveedors as $obj) {
                        if (false == $this->collRComercioProveedors->contains($obj)) {
                          $this->collRComercioProveedors->append($obj);
                        }
                      }

                      $this->collRComercioProveedorsPartial = true;
                    }

                    $collRComercioProveedors->getInternalIterator()->rewind();

                    return $collRComercioProveedors;
                }

                if ($partial && $this->collRComercioProveedors) {
                    foreach ($this->collRComercioProveedors as $obj) {
                        if ($obj->isNew()) {
                            $collRComercioProveedors[] = $obj;
                        }
                    }
                }

                $this->collRComercioProveedors = $collRComercioProveedors;
                $this->collRComercioProveedorsPartial = false;
            }
        }

        return $this->collRComercioProveedors;
    }

    /**
     * Sets a collection of RComercioProveedor objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rComercioProveedors A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Proveedor The current object (for fluent API support)
     */
    public function setRComercioProveedors(PropelCollection $rComercioProveedors, PropelPDO $con = null)
    {
        $rComercioProveedorsToDelete = $this->getRComercioProveedors(new Criteria(), $con)->diff($rComercioProveedors);


        $this->rComercioProveedorsScheduledForDeletion = $rComercioProveedorsToDelete;

        foreach ($rComercioProveedorsToDelete as $rComercioProveedorRemoved) {
            $rComercioProveedorRemoved->setProveedor(null);
        }

        $this->collRComercioProveedors = null;
        foreach ($rComercioProveedors as $rComercioProveedor) {
            $this->addRComercioProveedor($rComercioProveedor);
        }

        $this->collRComercioProveedors = $rComercioProveedors;
        $this->collRComercioProveedorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RComercioProveedor objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RComercioProveedor objects.
     * @throws PropelException
     */
    public function countRComercioProveedors(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRComercioProveedorsPartial && !$this->isNew();
        if (null === $this->collRComercioProveedors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRComercioProveedors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRComercioProveedors());
            }
            $query = RComercioProveedorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProveedor($this)
                ->count($con);
        }

        return count($this->collRComercioProveedors);
    }

    /**
     * Method called to associate a RComercioProveedor object to this object
     * through the RComercioProveedor foreign key attribute.
     *
     * @param    RComercioProveedor $l RComercioProveedor
     * @return Proveedor The current object (for fluent API support)
     */
    public function addRComercioProveedor(RComercioProveedor $l)
    {
        if ($this->collRComercioProveedors === null) {
            $this->initRComercioProveedors();
            $this->collRComercioProveedorsPartial = true;
        }

        if (!in_array($l, $this->collRComercioProveedors->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRComercioProveedor($l);

            if ($this->rComercioProveedorsScheduledForDeletion and $this->rComercioProveedorsScheduledForDeletion->contains($l)) {
                $this->rComercioProveedorsScheduledForDeletion->remove($this->rComercioProveedorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RComercioProveedor $rComercioProveedor The rComercioProveedor object to add.
     */
    protected function doAddRComercioProveedor($rComercioProveedor)
    {
        $this->collRComercioProveedors[]= $rComercioProveedor;
        $rComercioProveedor->setProveedor($this);
    }

    /**
     * @param	RComercioProveedor $rComercioProveedor The rComercioProveedor object to remove.
     * @return Proveedor The current object (for fluent API support)
     */
    public function removeRComercioProveedor($rComercioProveedor)
    {
        if ($this->getRComercioProveedors()->contains($rComercioProveedor)) {
            $this->collRComercioProveedors->remove($this->collRComercioProveedors->search($rComercioProveedor));
            if (null === $this->rComercioProveedorsScheduledForDeletion) {
                $this->rComercioProveedorsScheduledForDeletion = clone $this->collRComercioProveedors;
                $this->rComercioProveedorsScheduledForDeletion->clear();
            }
            $this->rComercioProveedorsScheduledForDeletion[]= clone $rComercioProveedor;
            $rComercioProveedor->setProveedor(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Proveedor is new, it will return
     * an empty collection; or if this Proveedor has previously
     * been saved, it will retrieve related RComercioProveedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Proveedor.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RComercioProveedor[] List of RComercioProveedor objects
     */
    public function getRComercioProveedorsJoinComercio($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RComercioProveedorQuery::create(null, $criteria);
        $query->joinWith('Comercio', $join_behavior);

        return $this->getRComercioProveedors($query, $con);
    }

    /**
     * Clears out the collRProveedorCategoriaproductoss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Proveedor The current object (for fluent API support)
     * @see        addRProveedorCategoriaproductoss()
     */
    public function clearRProveedorCategoriaproductoss()
    {
        $this->collRProveedorCategoriaproductoss = null; // important to set this to null since that means it is uninitialized
        $this->collRProveedorCategoriaproductossPartial = null;

        return $this;
    }

    /**
     * reset is the collRProveedorCategoriaproductoss collection loaded partially
     *
     * @return void
     */
    public function resetPartialRProveedorCategoriaproductoss($v = true)
    {
        $this->collRProveedorCategoriaproductossPartial = $v;
    }

    /**
     * Initializes the collRProveedorCategoriaproductoss collection.
     *
     * By default this just sets the collRProveedorCategoriaproductoss collection to an empty array (like clearcollRProveedorCategoriaproductoss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRProveedorCategoriaproductoss($overrideExisting = true)
    {
        if (null !== $this->collRProveedorCategoriaproductoss && !$overrideExisting) {
            return;
        }
        $this->collRProveedorCategoriaproductoss = new PropelObjectCollection();
        $this->collRProveedorCategoriaproductoss->setModel('RProveedorCategoriaproductos');
    }

    /**
     * Gets an array of RProveedorCategoriaproductos objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Proveedor is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RProveedorCategoriaproductos[] List of RProveedorCategoriaproductos objects
     * @throws PropelException
     */
    public function getRProveedorCategoriaproductoss($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorCategoriaproductossPartial && !$this->isNew();
        if (null === $this->collRProveedorCategoriaproductoss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRProveedorCategoriaproductoss) {
                // return empty collection
                $this->initRProveedorCategoriaproductoss();
            } else {
                $collRProveedorCategoriaproductoss = RProveedorCategoriaproductosQuery::create(null, $criteria)
                    ->filterByProveedor($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRProveedorCategoriaproductossPartial && count($collRProveedorCategoriaproductoss)) {
                      $this->initRProveedorCategoriaproductoss(false);

                      foreach ($collRProveedorCategoriaproductoss as $obj) {
                        if (false == $this->collRProveedorCategoriaproductoss->contains($obj)) {
                          $this->collRProveedorCategoriaproductoss->append($obj);
                        }
                      }

                      $this->collRProveedorCategoriaproductossPartial = true;
                    }

                    $collRProveedorCategoriaproductoss->getInternalIterator()->rewind();

                    return $collRProveedorCategoriaproductoss;
                }

                if ($partial && $this->collRProveedorCategoriaproductoss) {
                    foreach ($this->collRProveedorCategoriaproductoss as $obj) {
                        if ($obj->isNew()) {
                            $collRProveedorCategoriaproductoss[] = $obj;
                        }
                    }
                }

                $this->collRProveedorCategoriaproductoss = $collRProveedorCategoriaproductoss;
                $this->collRProveedorCategoriaproductossPartial = false;
            }
        }

        return $this->collRProveedorCategoriaproductoss;
    }

    /**
     * Sets a collection of RProveedorCategoriaproductos objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rProveedorCategoriaproductoss A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Proveedor The current object (for fluent API support)
     */
    public function setRProveedorCategoriaproductoss(PropelCollection $rProveedorCategoriaproductoss, PropelPDO $con = null)
    {
        $rProveedorCategoriaproductossToDelete = $this->getRProveedorCategoriaproductoss(new Criteria(), $con)->diff($rProveedorCategoriaproductoss);


        $this->rProveedorCategoriaproductossScheduledForDeletion = $rProveedorCategoriaproductossToDelete;

        foreach ($rProveedorCategoriaproductossToDelete as $rProveedorCategoriaproductosRemoved) {
            $rProveedorCategoriaproductosRemoved->setProveedor(null);
        }

        $this->collRProveedorCategoriaproductoss = null;
        foreach ($rProveedorCategoriaproductoss as $rProveedorCategoriaproductos) {
            $this->addRProveedorCategoriaproductos($rProveedorCategoriaproductos);
        }

        $this->collRProveedorCategoriaproductoss = $rProveedorCategoriaproductoss;
        $this->collRProveedorCategoriaproductossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RProveedorCategoriaproductos objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RProveedorCategoriaproductos objects.
     * @throws PropelException
     */
    public function countRProveedorCategoriaproductoss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorCategoriaproductossPartial && !$this->isNew();
        if (null === $this->collRProveedorCategoriaproductoss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRProveedorCategoriaproductoss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRProveedorCategoriaproductoss());
            }
            $query = RProveedorCategoriaproductosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProveedor($this)
                ->count($con);
        }

        return count($this->collRProveedorCategoriaproductoss);
    }

    /**
     * Method called to associate a RProveedorCategoriaproductos object to this object
     * through the RProveedorCategoriaproductos foreign key attribute.
     *
     * @param    RProveedorCategoriaproductos $l RProveedorCategoriaproductos
     * @return Proveedor The current object (for fluent API support)
     */
    public function addRProveedorCategoriaproductos(RProveedorCategoriaproductos $l)
    {
        if ($this->collRProveedorCategoriaproductoss === null) {
            $this->initRProveedorCategoriaproductoss();
            $this->collRProveedorCategoriaproductossPartial = true;
        }

        if (!in_array($l, $this->collRProveedorCategoriaproductoss->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRProveedorCategoriaproductos($l);

            if ($this->rProveedorCategoriaproductossScheduledForDeletion and $this->rProveedorCategoriaproductossScheduledForDeletion->contains($l)) {
                $this->rProveedorCategoriaproductossScheduledForDeletion->remove($this->rProveedorCategoriaproductossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RProveedorCategoriaproductos $rProveedorCategoriaproductos The rProveedorCategoriaproductos object to add.
     */
    protected function doAddRProveedorCategoriaproductos($rProveedorCategoriaproductos)
    {
        $this->collRProveedorCategoriaproductoss[]= $rProveedorCategoriaproductos;
        $rProveedorCategoriaproductos->setProveedor($this);
    }

    /**
     * @param	RProveedorCategoriaproductos $rProveedorCategoriaproductos The rProveedorCategoriaproductos object to remove.
     * @return Proveedor The current object (for fluent API support)
     */
    public function removeRProveedorCategoriaproductos($rProveedorCategoriaproductos)
    {
        if ($this->getRProveedorCategoriaproductoss()->contains($rProveedorCategoriaproductos)) {
            $this->collRProveedorCategoriaproductoss->remove($this->collRProveedorCategoriaproductoss->search($rProveedorCategoriaproductos));
            if (null === $this->rProveedorCategoriaproductossScheduledForDeletion) {
                $this->rProveedorCategoriaproductossScheduledForDeletion = clone $this->collRProveedorCategoriaproductoss;
                $this->rProveedorCategoriaproductossScheduledForDeletion->clear();
            }
            $this->rProveedorCategoriaproductossScheduledForDeletion[]= clone $rProveedorCategoriaproductos;
            $rProveedorCategoriaproductos->setProveedor(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Proveedor is new, it will return
     * an empty collection; or if this Proveedor has previously
     * been saved, it will retrieve related RProveedorCategoriaproductoss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Proveedor.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RProveedorCategoriaproductos[] List of RProveedorCategoriaproductos objects
     */
    public function getRProveedorCategoriaproductossJoinCategoriaProductos($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RProveedorCategoriaproductosQuery::create(null, $criteria);
        $query->joinWith('CategoriaProductos', $join_behavior);

        return $this->getRProveedorCategoriaproductoss($query, $con);
    }

    /**
     * Clears out the collRProveedorListaprecioss collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Proveedor The current object (for fluent API support)
     * @see        addRProveedorListaprecioss()
     */
    public function clearRProveedorListaprecioss()
    {
        $this->collRProveedorListaprecioss = null; // important to set this to null since that means it is uninitialized
        $this->collRProveedorListapreciossPartial = null;

        return $this;
    }

    /**
     * reset is the collRProveedorListaprecioss collection loaded partially
     *
     * @return void
     */
    public function resetPartialRProveedorListaprecioss($v = true)
    {
        $this->collRProveedorListapreciossPartial = $v;
    }

    /**
     * Initializes the collRProveedorListaprecioss collection.
     *
     * By default this just sets the collRProveedorListaprecioss collection to an empty array (like clearcollRProveedorListaprecioss());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRProveedorListaprecioss($overrideExisting = true)
    {
        if (null !== $this->collRProveedorListaprecioss && !$overrideExisting) {
            return;
        }
        $this->collRProveedorListaprecioss = new PropelObjectCollection();
        $this->collRProveedorListaprecioss->setModel('RProveedorListaprecios');
    }

    /**
     * Gets an array of RProveedorListaprecios objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Proveedor is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RProveedorListaprecios[] List of RProveedorListaprecios objects
     * @throws PropelException
     */
    public function getRProveedorListaprecioss($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorListapreciossPartial && !$this->isNew();
        if (null === $this->collRProveedorListaprecioss || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRProveedorListaprecioss) {
                // return empty collection
                $this->initRProveedorListaprecioss();
            } else {
                $collRProveedorListaprecioss = RProveedorListapreciosQuery::create(null, $criteria)
                    ->filterByProveedor($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRProveedorListapreciossPartial && count($collRProveedorListaprecioss)) {
                      $this->initRProveedorListaprecioss(false);

                      foreach ($collRProveedorListaprecioss as $obj) {
                        if (false == $this->collRProveedorListaprecioss->contains($obj)) {
                          $this->collRProveedorListaprecioss->append($obj);
                        }
                      }

                      $this->collRProveedorListapreciossPartial = true;
                    }

                    $collRProveedorListaprecioss->getInternalIterator()->rewind();

                    return $collRProveedorListaprecioss;
                }

                if ($partial && $this->collRProveedorListaprecioss) {
                    foreach ($this->collRProveedorListaprecioss as $obj) {
                        if ($obj->isNew()) {
                            $collRProveedorListaprecioss[] = $obj;
                        }
                    }
                }

                $this->collRProveedorListaprecioss = $collRProveedorListaprecioss;
                $this->collRProveedorListapreciossPartial = false;
            }
        }

        return $this->collRProveedorListaprecioss;
    }

    /**
     * Sets a collection of RProveedorListaprecios objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rProveedorListaprecioss A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Proveedor The current object (for fluent API support)
     */
    public function setRProveedorListaprecioss(PropelCollection $rProveedorListaprecioss, PropelPDO $con = null)
    {
        $rProveedorListapreciossToDelete = $this->getRProveedorListaprecioss(new Criteria(), $con)->diff($rProveedorListaprecioss);


        $this->rProveedorListapreciossScheduledForDeletion = $rProveedorListapreciossToDelete;

        foreach ($rProveedorListapreciossToDelete as $rProveedorListapreciosRemoved) {
            $rProveedorListapreciosRemoved->setProveedor(null);
        }

        $this->collRProveedorListaprecioss = null;
        foreach ($rProveedorListaprecioss as $rProveedorListaprecios) {
            $this->addRProveedorListaprecios($rProveedorListaprecios);
        }

        $this->collRProveedorListaprecioss = $rProveedorListaprecioss;
        $this->collRProveedorListapreciossPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RProveedorListaprecios objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RProveedorListaprecios objects.
     * @throws PropelException
     */
    public function countRProveedorListaprecioss(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorListapreciossPartial && !$this->isNew();
        if (null === $this->collRProveedorListaprecioss || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRProveedorListaprecioss) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRProveedorListaprecioss());
            }
            $query = RProveedorListapreciosQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProveedor($this)
                ->count($con);
        }

        return count($this->collRProveedorListaprecioss);
    }

    /**
     * Method called to associate a RProveedorListaprecios object to this object
     * through the RProveedorListaprecios foreign key attribute.
     *
     * @param    RProveedorListaprecios $l RProveedorListaprecios
     * @return Proveedor The current object (for fluent API support)
     */
    public function addRProveedorListaprecios(RProveedorListaprecios $l)
    {
        if ($this->collRProveedorListaprecioss === null) {
            $this->initRProveedorListaprecioss();
            $this->collRProveedorListapreciossPartial = true;
        }

        if (!in_array($l, $this->collRProveedorListaprecioss->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRProveedorListaprecios($l);

            if ($this->rProveedorListapreciossScheduledForDeletion and $this->rProveedorListapreciossScheduledForDeletion->contains($l)) {
                $this->rProveedorListapreciossScheduledForDeletion->remove($this->rProveedorListapreciossScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RProveedorListaprecios $rProveedorListaprecios The rProveedorListaprecios object to add.
     */
    protected function doAddRProveedorListaprecios($rProveedorListaprecios)
    {
        $this->collRProveedorListaprecioss[]= $rProveedorListaprecios;
        $rProveedorListaprecios->setProveedor($this);
    }

    /**
     * @param	RProveedorListaprecios $rProveedorListaprecios The rProveedorListaprecios object to remove.
     * @return Proveedor The current object (for fluent API support)
     */
    public function removeRProveedorListaprecios($rProveedorListaprecios)
    {
        if ($this->getRProveedorListaprecioss()->contains($rProveedorListaprecios)) {
            $this->collRProveedorListaprecioss->remove($this->collRProveedorListaprecioss->search($rProveedorListaprecios));
            if (null === $this->rProveedorListapreciossScheduledForDeletion) {
                $this->rProveedorListapreciossScheduledForDeletion = clone $this->collRProveedorListaprecioss;
                $this->rProveedorListapreciossScheduledForDeletion->clear();
            }
            $this->rProveedorListapreciossScheduledForDeletion[]= clone $rProveedorListaprecios;
            $rProveedorListaprecios->setProveedor(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Proveedor is new, it will return
     * an empty collection; or if this Proveedor has previously
     * been saved, it will retrieve related RProveedorListaprecioss from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Proveedor.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RProveedorListaprecios[] List of RProveedorListaprecios objects
     */
    public function getRProveedorListapreciossJoinListaPrecios($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RProveedorListapreciosQuery::create(null, $criteria);
        $query->joinWith('ListaPrecios', $join_behavior);

        return $this->getRProveedorListaprecioss($query, $con);
    }

    /**
     * Clears out the collRProveedorProductos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Proveedor The current object (for fluent API support)
     * @see        addRProveedorProductos()
     */
    public function clearRProveedorProductos()
    {
        $this->collRProveedorProductos = null; // important to set this to null since that means it is uninitialized
        $this->collRProveedorProductosPartial = null;

        return $this;
    }

    /**
     * reset is the collRProveedorProductos collection loaded partially
     *
     * @return void
     */
    public function resetPartialRProveedorProductos($v = true)
    {
        $this->collRProveedorProductosPartial = $v;
    }

    /**
     * Initializes the collRProveedorProductos collection.
     *
     * By default this just sets the collRProveedorProductos collection to an empty array (like clearcollRProveedorProductos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRProveedorProductos($overrideExisting = true)
    {
        if (null !== $this->collRProveedorProductos && !$overrideExisting) {
            return;
        }
        $this->collRProveedorProductos = new PropelObjectCollection();
        $this->collRProveedorProductos->setModel('RProveedorProducto');
    }

    /**
     * Gets an array of RProveedorProducto objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Proveedor is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RProveedorProducto[] List of RProveedorProducto objects
     * @throws PropelException
     */
    public function getRProveedorProductos($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorProductosPartial && !$this->isNew();
        if (null === $this->collRProveedorProductos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRProveedorProductos) {
                // return empty collection
                $this->initRProveedorProductos();
            } else {
                $collRProveedorProductos = RProveedorProductoQuery::create(null, $criteria)
                    ->filterByProveedor($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRProveedorProductosPartial && count($collRProveedorProductos)) {
                      $this->initRProveedorProductos(false);

                      foreach ($collRProveedorProductos as $obj) {
                        if (false == $this->collRProveedorProductos->contains($obj)) {
                          $this->collRProveedorProductos->append($obj);
                        }
                      }

                      $this->collRProveedorProductosPartial = true;
                    }

                    $collRProveedorProductos->getInternalIterator()->rewind();

                    return $collRProveedorProductos;
                }

                if ($partial && $this->collRProveedorProductos) {
                    foreach ($this->collRProveedorProductos as $obj) {
                        if ($obj->isNew()) {
                            $collRProveedorProductos[] = $obj;
                        }
                    }
                }

                $this->collRProveedorProductos = $collRProveedorProductos;
                $this->collRProveedorProductosPartial = false;
            }
        }

        return $this->collRProveedorProductos;
    }

    /**
     * Sets a collection of RProveedorProducto objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rProveedorProductos A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Proveedor The current object (for fluent API support)
     */
    public function setRProveedorProductos(PropelCollection $rProveedorProductos, PropelPDO $con = null)
    {
        $rProveedorProductosToDelete = $this->getRProveedorProductos(new Criteria(), $con)->diff($rProveedorProductos);


        $this->rProveedorProductosScheduledForDeletion = $rProveedorProductosToDelete;

        foreach ($rProveedorProductosToDelete as $rProveedorProductoRemoved) {
            $rProveedorProductoRemoved->setProveedor(null);
        }

        $this->collRProveedorProductos = null;
        foreach ($rProveedorProductos as $rProveedorProducto) {
            $this->addRProveedorProducto($rProveedorProducto);
        }

        $this->collRProveedorProductos = $rProveedorProductos;
        $this->collRProveedorProductosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RProveedorProducto objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RProveedorProducto objects.
     * @throws PropelException
     */
    public function countRProveedorProductos(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorProductosPartial && !$this->isNew();
        if (null === $this->collRProveedorProductos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRProveedorProductos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRProveedorProductos());
            }
            $query = RProveedorProductoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProveedor($this)
                ->count($con);
        }

        return count($this->collRProveedorProductos);
    }

    /**
     * Method called to associate a RProveedorProducto object to this object
     * through the RProveedorProducto foreign key attribute.
     *
     * @param    RProveedorProducto $l RProveedorProducto
     * @return Proveedor The current object (for fluent API support)
     */
    public function addRProveedorProducto(RProveedorProducto $l)
    {
        if ($this->collRProveedorProductos === null) {
            $this->initRProveedorProductos();
            $this->collRProveedorProductosPartial = true;
        }

        if (!in_array($l, $this->collRProveedorProductos->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRProveedorProducto($l);

            if ($this->rProveedorProductosScheduledForDeletion and $this->rProveedorProductosScheduledForDeletion->contains($l)) {
                $this->rProveedorProductosScheduledForDeletion->remove($this->rProveedorProductosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RProveedorProducto $rProveedorProducto The rProveedorProducto object to add.
     */
    protected function doAddRProveedorProducto($rProveedorProducto)
    {
        $this->collRProveedorProductos[]= $rProveedorProducto;
        $rProveedorProducto->setProveedor($this);
    }

    /**
     * @param	RProveedorProducto $rProveedorProducto The rProveedorProducto object to remove.
     * @return Proveedor The current object (for fluent API support)
     */
    public function removeRProveedorProducto($rProveedorProducto)
    {
        if ($this->getRProveedorProductos()->contains($rProveedorProducto)) {
            $this->collRProveedorProductos->remove($this->collRProveedorProductos->search($rProveedorProducto));
            if (null === $this->rProveedorProductosScheduledForDeletion) {
                $this->rProveedorProductosScheduledForDeletion = clone $this->collRProveedorProductos;
                $this->rProveedorProductosScheduledForDeletion->clear();
            }
            $this->rProveedorProductosScheduledForDeletion[]= clone $rProveedorProducto;
            $rProveedorProducto->setProveedor(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Proveedor is new, it will return
     * an empty collection; or if this Proveedor has previously
     * been saved, it will retrieve related RProveedorProductos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Proveedor.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RProveedorProducto[] List of RProveedorProducto objects
     */
    public function getRProveedorProductosJoinProducto($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RProveedorProductoQuery::create(null, $criteria);
        $query->joinWith('Producto', $join_behavior);

        return $this->getRProveedorProductos($query, $con);
    }

    /**
     * Clears out the collRProveedorReputacions collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Proveedor The current object (for fluent API support)
     * @see        addRProveedorReputacions()
     */
    public function clearRProveedorReputacions()
    {
        $this->collRProveedorReputacions = null; // important to set this to null since that means it is uninitialized
        $this->collRProveedorReputacionsPartial = null;

        return $this;
    }

    /**
     * reset is the collRProveedorReputacions collection loaded partially
     *
     * @return void
     */
    public function resetPartialRProveedorReputacions($v = true)
    {
        $this->collRProveedorReputacionsPartial = $v;
    }

    /**
     * Initializes the collRProveedorReputacions collection.
     *
     * By default this just sets the collRProveedorReputacions collection to an empty array (like clearcollRProveedorReputacions());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRProveedorReputacions($overrideExisting = true)
    {
        if (null !== $this->collRProveedorReputacions && !$overrideExisting) {
            return;
        }
        $this->collRProveedorReputacions = new PropelObjectCollection();
        $this->collRProveedorReputacions->setModel('RProveedorReputacion');
    }

    /**
     * Gets an array of RProveedorReputacion objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Proveedor is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RProveedorReputacion[] List of RProveedorReputacion objects
     * @throws PropelException
     */
    public function getRProveedorReputacions($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorReputacionsPartial && !$this->isNew();
        if (null === $this->collRProveedorReputacions || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRProveedorReputacions) {
                // return empty collection
                $this->initRProveedorReputacions();
            } else {
                $collRProveedorReputacions = RProveedorReputacionQuery::create(null, $criteria)
                    ->filterByProveedor($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRProveedorReputacionsPartial && count($collRProveedorReputacions)) {
                      $this->initRProveedorReputacions(false);

                      foreach ($collRProveedorReputacions as $obj) {
                        if (false == $this->collRProveedorReputacions->contains($obj)) {
                          $this->collRProveedorReputacions->append($obj);
                        }
                      }

                      $this->collRProveedorReputacionsPartial = true;
                    }

                    $collRProveedorReputacions->getInternalIterator()->rewind();

                    return $collRProveedorReputacions;
                }

                if ($partial && $this->collRProveedorReputacions) {
                    foreach ($this->collRProveedorReputacions as $obj) {
                        if ($obj->isNew()) {
                            $collRProveedorReputacions[] = $obj;
                        }
                    }
                }

                $this->collRProveedorReputacions = $collRProveedorReputacions;
                $this->collRProveedorReputacionsPartial = false;
            }
        }

        return $this->collRProveedorReputacions;
    }

    /**
     * Sets a collection of RProveedorReputacion objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rProveedorReputacions A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Proveedor The current object (for fluent API support)
     */
    public function setRProveedorReputacions(PropelCollection $rProveedorReputacions, PropelPDO $con = null)
    {
        $rProveedorReputacionsToDelete = $this->getRProveedorReputacions(new Criteria(), $con)->diff($rProveedorReputacions);


        $this->rProveedorReputacionsScheduledForDeletion = $rProveedorReputacionsToDelete;

        foreach ($rProveedorReputacionsToDelete as $rProveedorReputacionRemoved) {
            $rProveedorReputacionRemoved->setProveedor(null);
        }

        $this->collRProveedorReputacions = null;
        foreach ($rProveedorReputacions as $rProveedorReputacion) {
            $this->addRProveedorReputacion($rProveedorReputacion);
        }

        $this->collRProveedorReputacions = $rProveedorReputacions;
        $this->collRProveedorReputacionsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RProveedorReputacion objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RProveedorReputacion objects.
     * @throws PropelException
     */
    public function countRProveedorReputacions(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorReputacionsPartial && !$this->isNew();
        if (null === $this->collRProveedorReputacions || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRProveedorReputacions) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRProveedorReputacions());
            }
            $query = RProveedorReputacionQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProveedor($this)
                ->count($con);
        }

        return count($this->collRProveedorReputacions);
    }

    /**
     * Method called to associate a RProveedorReputacion object to this object
     * through the RProveedorReputacion foreign key attribute.
     *
     * @param    RProveedorReputacion $l RProveedorReputacion
     * @return Proveedor The current object (for fluent API support)
     */
    public function addRProveedorReputacion(RProveedorReputacion $l)
    {
        if ($this->collRProveedorReputacions === null) {
            $this->initRProveedorReputacions();
            $this->collRProveedorReputacionsPartial = true;
        }

        if (!in_array($l, $this->collRProveedorReputacions->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRProveedorReputacion($l);

            if ($this->rProveedorReputacionsScheduledForDeletion and $this->rProveedorReputacionsScheduledForDeletion->contains($l)) {
                $this->rProveedorReputacionsScheduledForDeletion->remove($this->rProveedorReputacionsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RProveedorReputacion $rProveedorReputacion The rProveedorReputacion object to add.
     */
    protected function doAddRProveedorReputacion($rProveedorReputacion)
    {
        $this->collRProveedorReputacions[]= $rProveedorReputacion;
        $rProveedorReputacion->setProveedor($this);
    }

    /**
     * @param	RProveedorReputacion $rProveedorReputacion The rProveedorReputacion object to remove.
     * @return Proveedor The current object (for fluent API support)
     */
    public function removeRProveedorReputacion($rProveedorReputacion)
    {
        if ($this->getRProveedorReputacions()->contains($rProveedorReputacion)) {
            $this->collRProveedorReputacions->remove($this->collRProveedorReputacions->search($rProveedorReputacion));
            if (null === $this->rProveedorReputacionsScheduledForDeletion) {
                $this->rProveedorReputacionsScheduledForDeletion = clone $this->collRProveedorReputacions;
                $this->rProveedorReputacionsScheduledForDeletion->clear();
            }
            $this->rProveedorReputacionsScheduledForDeletion[]= clone $rProveedorReputacion;
            $rProveedorReputacion->setProveedor(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Proveedor is new, it will return
     * an empty collection; or if this Proveedor has previously
     * been saved, it will retrieve related RProveedorReputacions from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Proveedor.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RProveedorReputacion[] List of RProveedorReputacion objects
     */
    public function getRProveedorReputacionsJoinReputacion($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RProveedorReputacionQuery::create(null, $criteria);
        $query->joinWith('Reputacion', $join_behavior);

        return $this->getRProveedorReputacions($query, $con);
    }

    /**
     * Clears out the collRProveedorZonaentregas collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Proveedor The current object (for fluent API support)
     * @see        addRProveedorZonaentregas()
     */
    public function clearRProveedorZonaentregas()
    {
        $this->collRProveedorZonaentregas = null; // important to set this to null since that means it is uninitialized
        $this->collRProveedorZonaentregasPartial = null;

        return $this;
    }

    /**
     * reset is the collRProveedorZonaentregas collection loaded partially
     *
     * @return void
     */
    public function resetPartialRProveedorZonaentregas($v = true)
    {
        $this->collRProveedorZonaentregasPartial = $v;
    }

    /**
     * Initializes the collRProveedorZonaentregas collection.
     *
     * By default this just sets the collRProveedorZonaentregas collection to an empty array (like clearcollRProveedorZonaentregas());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRProveedorZonaentregas($overrideExisting = true)
    {
        if (null !== $this->collRProveedorZonaentregas && !$overrideExisting) {
            return;
        }
        $this->collRProveedorZonaentregas = new PropelObjectCollection();
        $this->collRProveedorZonaentregas->setModel('RProveedorZonaentrega');
    }

    /**
     * Gets an array of RProveedorZonaentrega objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Proveedor is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RProveedorZonaentrega[] List of RProveedorZonaentrega objects
     * @throws PropelException
     */
    public function getRProveedorZonaentregas($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorZonaentregasPartial && !$this->isNew();
        if (null === $this->collRProveedorZonaentregas || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRProveedorZonaentregas) {
                // return empty collection
                $this->initRProveedorZonaentregas();
            } else {
                $collRProveedorZonaentregas = RProveedorZonaentregaQuery::create(null, $criteria)
                    ->filterByProveedor($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRProveedorZonaentregasPartial && count($collRProveedorZonaentregas)) {
                      $this->initRProveedorZonaentregas(false);

                      foreach ($collRProveedorZonaentregas as $obj) {
                        if (false == $this->collRProveedorZonaentregas->contains($obj)) {
                          $this->collRProveedorZonaentregas->append($obj);
                        }
                      }

                      $this->collRProveedorZonaentregasPartial = true;
                    }

                    $collRProveedorZonaentregas->getInternalIterator()->rewind();

                    return $collRProveedorZonaentregas;
                }

                if ($partial && $this->collRProveedorZonaentregas) {
                    foreach ($this->collRProveedorZonaentregas as $obj) {
                        if ($obj->isNew()) {
                            $collRProveedorZonaentregas[] = $obj;
                        }
                    }
                }

                $this->collRProveedorZonaentregas = $collRProveedorZonaentregas;
                $this->collRProveedorZonaentregasPartial = false;
            }
        }

        return $this->collRProveedorZonaentregas;
    }

    /**
     * Sets a collection of RProveedorZonaentrega objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rProveedorZonaentregas A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Proveedor The current object (for fluent API support)
     */
    public function setRProveedorZonaentregas(PropelCollection $rProveedorZonaentregas, PropelPDO $con = null)
    {
        $rProveedorZonaentregasToDelete = $this->getRProveedorZonaentregas(new Criteria(), $con)->diff($rProveedorZonaentregas);


        $this->rProveedorZonaentregasScheduledForDeletion = $rProveedorZonaentregasToDelete;

        foreach ($rProveedorZonaentregasToDelete as $rProveedorZonaentregaRemoved) {
            $rProveedorZonaentregaRemoved->setProveedor(null);
        }

        $this->collRProveedorZonaentregas = null;
        foreach ($rProveedorZonaentregas as $rProveedorZonaentrega) {
            $this->addRProveedorZonaentrega($rProveedorZonaentrega);
        }

        $this->collRProveedorZonaentregas = $rProveedorZonaentregas;
        $this->collRProveedorZonaentregasPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RProveedorZonaentrega objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RProveedorZonaentrega objects.
     * @throws PropelException
     */
    public function countRProveedorZonaentregas(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRProveedorZonaentregasPartial && !$this->isNew();
        if (null === $this->collRProveedorZonaentregas || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRProveedorZonaentregas) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRProveedorZonaentregas());
            }
            $query = RProveedorZonaentregaQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProveedor($this)
                ->count($con);
        }

        return count($this->collRProveedorZonaentregas);
    }

    /**
     * Method called to associate a RProveedorZonaentrega object to this object
     * through the RProveedorZonaentrega foreign key attribute.
     *
     * @param    RProveedorZonaentrega $l RProveedorZonaentrega
     * @return Proveedor The current object (for fluent API support)
     */
    public function addRProveedorZonaentrega(RProveedorZonaentrega $l)
    {
        if ($this->collRProveedorZonaentregas === null) {
            $this->initRProveedorZonaentregas();
            $this->collRProveedorZonaentregasPartial = true;
        }

        if (!in_array($l, $this->collRProveedorZonaentregas->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRProveedorZonaentrega($l);

            if ($this->rProveedorZonaentregasScheduledForDeletion and $this->rProveedorZonaentregasScheduledForDeletion->contains($l)) {
                $this->rProveedorZonaentregasScheduledForDeletion->remove($this->rProveedorZonaentregasScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RProveedorZonaentrega $rProveedorZonaentrega The rProveedorZonaentrega object to add.
     */
    protected function doAddRProveedorZonaentrega($rProveedorZonaentrega)
    {
        $this->collRProveedorZonaentregas[]= $rProveedorZonaentrega;
        $rProveedorZonaentrega->setProveedor($this);
    }

    /**
     * @param	RProveedorZonaentrega $rProveedorZonaentrega The rProveedorZonaentrega object to remove.
     * @return Proveedor The current object (for fluent API support)
     */
    public function removeRProveedorZonaentrega($rProveedorZonaentrega)
    {
        if ($this->getRProveedorZonaentregas()->contains($rProveedorZonaentrega)) {
            $this->collRProveedorZonaentregas->remove($this->collRProveedorZonaentregas->search($rProveedorZonaentrega));
            if (null === $this->rProveedorZonaentregasScheduledForDeletion) {
                $this->rProveedorZonaentregasScheduledForDeletion = clone $this->collRProveedorZonaentregas;
                $this->rProveedorZonaentregasScheduledForDeletion->clear();
            }
            $this->rProveedorZonaentregasScheduledForDeletion[]= clone $rProveedorZonaentrega;
            $rProveedorZonaentrega->setProveedor(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Proveedor is new, it will return
     * an empty collection; or if this Proveedor has previously
     * been saved, it will retrieve related RProveedorZonaentregas from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Proveedor.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RProveedorZonaentrega[] List of RProveedorZonaentrega objects
     */
    public function getRProveedorZonaentregasJoinZonaEntrega($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RProveedorZonaentregaQuery::create(null, $criteria);
        $query->joinWith('ZonaEntrega', $join_behavior);

        return $this->getRProveedorZonaentregas($query, $con);
    }

    /**
     * Clears out the collRUsuarioProveedors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Proveedor The current object (for fluent API support)
     * @see        addRUsuarioProveedors()
     */
    public function clearRUsuarioProveedors()
    {
        $this->collRUsuarioProveedors = null; // important to set this to null since that means it is uninitialized
        $this->collRUsuarioProveedorsPartial = null;

        return $this;
    }

    /**
     * reset is the collRUsuarioProveedors collection loaded partially
     *
     * @return void
     */
    public function resetPartialRUsuarioProveedors($v = true)
    {
        $this->collRUsuarioProveedorsPartial = $v;
    }

    /**
     * Initializes the collRUsuarioProveedors collection.
     *
     * By default this just sets the collRUsuarioProveedors collection to an empty array (like clearcollRUsuarioProveedors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRUsuarioProveedors($overrideExisting = true)
    {
        if (null !== $this->collRUsuarioProveedors && !$overrideExisting) {
            return;
        }
        $this->collRUsuarioProveedors = new PropelObjectCollection();
        $this->collRUsuarioProveedors->setModel('RUsuarioProveedor');
    }

    /**
     * Gets an array of RUsuarioProveedor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Proveedor is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RUsuarioProveedor[] List of RUsuarioProveedor objects
     * @throws PropelException
     */
    public function getRUsuarioProveedors($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioProveedorsPartial && !$this->isNew();
        if (null === $this->collRUsuarioProveedors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioProveedors) {
                // return empty collection
                $this->initRUsuarioProveedors();
            } else {
                $collRUsuarioProveedors = RUsuarioProveedorQuery::create(null, $criteria)
                    ->filterByProveedor($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRUsuarioProveedorsPartial && count($collRUsuarioProveedors)) {
                      $this->initRUsuarioProveedors(false);

                      foreach ($collRUsuarioProveedors as $obj) {
                        if (false == $this->collRUsuarioProveedors->contains($obj)) {
                          $this->collRUsuarioProveedors->append($obj);
                        }
                      }

                      $this->collRUsuarioProveedorsPartial = true;
                    }

                    $collRUsuarioProveedors->getInternalIterator()->rewind();

                    return $collRUsuarioProveedors;
                }

                if ($partial && $this->collRUsuarioProveedors) {
                    foreach ($this->collRUsuarioProveedors as $obj) {
                        if ($obj->isNew()) {
                            $collRUsuarioProveedors[] = $obj;
                        }
                    }
                }

                $this->collRUsuarioProveedors = $collRUsuarioProveedors;
                $this->collRUsuarioProveedorsPartial = false;
            }
        }

        return $this->collRUsuarioProveedors;
    }

    /**
     * Sets a collection of RUsuarioProveedor objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rUsuarioProveedors A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Proveedor The current object (for fluent API support)
     */
    public function setRUsuarioProveedors(PropelCollection $rUsuarioProveedors, PropelPDO $con = null)
    {
        $rUsuarioProveedorsToDelete = $this->getRUsuarioProveedors(new Criteria(), $con)->diff($rUsuarioProveedors);


        $this->rUsuarioProveedorsScheduledForDeletion = $rUsuarioProveedorsToDelete;

        foreach ($rUsuarioProveedorsToDelete as $rUsuarioProveedorRemoved) {
            $rUsuarioProveedorRemoved->setProveedor(null);
        }

        $this->collRUsuarioProveedors = null;
        foreach ($rUsuarioProveedors as $rUsuarioProveedor) {
            $this->addRUsuarioProveedor($rUsuarioProveedor);
        }

        $this->collRUsuarioProveedors = $rUsuarioProveedors;
        $this->collRUsuarioProveedorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RUsuarioProveedor objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RUsuarioProveedor objects.
     * @throws PropelException
     */
    public function countRUsuarioProveedors(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioProveedorsPartial && !$this->isNew();
        if (null === $this->collRUsuarioProveedors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioProveedors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRUsuarioProveedors());
            }
            $query = RUsuarioProveedorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByProveedor($this)
                ->count($con);
        }

        return count($this->collRUsuarioProveedors);
    }

    /**
     * Method called to associate a RUsuarioProveedor object to this object
     * through the RUsuarioProveedor foreign key attribute.
     *
     * @param    RUsuarioProveedor $l RUsuarioProveedor
     * @return Proveedor The current object (for fluent API support)
     */
    public function addRUsuarioProveedor(RUsuarioProveedor $l)
    {
        if ($this->collRUsuarioProveedors === null) {
            $this->initRUsuarioProveedors();
            $this->collRUsuarioProveedorsPartial = true;
        }

        if (!in_array($l, $this->collRUsuarioProveedors->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRUsuarioProveedor($l);

            if ($this->rUsuarioProveedorsScheduledForDeletion and $this->rUsuarioProveedorsScheduledForDeletion->contains($l)) {
                $this->rUsuarioProveedorsScheduledForDeletion->remove($this->rUsuarioProveedorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RUsuarioProveedor $rUsuarioProveedor The rUsuarioProveedor object to add.
     */
    protected function doAddRUsuarioProveedor($rUsuarioProveedor)
    {
        $this->collRUsuarioProveedors[]= $rUsuarioProveedor;
        $rUsuarioProveedor->setProveedor($this);
    }

    /**
     * @param	RUsuarioProveedor $rUsuarioProveedor The rUsuarioProveedor object to remove.
     * @return Proveedor The current object (for fluent API support)
     */
    public function removeRUsuarioProveedor($rUsuarioProveedor)
    {
        if ($this->getRUsuarioProveedors()->contains($rUsuarioProveedor)) {
            $this->collRUsuarioProveedors->remove($this->collRUsuarioProveedors->search($rUsuarioProveedor));
            if (null === $this->rUsuarioProveedorsScheduledForDeletion) {
                $this->rUsuarioProveedorsScheduledForDeletion = clone $this->collRUsuarioProveedors;
                $this->rUsuarioProveedorsScheduledForDeletion->clear();
            }
            $this->rUsuarioProveedorsScheduledForDeletion[]= clone $rUsuarioProveedor;
            $rUsuarioProveedor->setProveedor(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Proveedor is new, it will return
     * an empty collection; or if this Proveedor has previously
     * been saved, it will retrieve related RUsuarioProveedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Proveedor.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RUsuarioProveedor[] List of RUsuarioProveedor objects
     */
    public function getRUsuarioProveedorsJoinUsuario($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RUsuarioProveedorQuery::create(null, $criteria);
        $query->joinWith('Usuario', $join_behavior);

        return $this->getRUsuarioProveedors($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->email = null;
        $this->nombre_contacto = null;
        $this->preferencia_pago = null;
        $this->pedido_minimo = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collRComercioProveedors) {
                foreach ($this->collRComercioProveedors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRProveedorCategoriaproductoss) {
                foreach ($this->collRProveedorCategoriaproductoss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRProveedorListaprecioss) {
                foreach ($this->collRProveedorListaprecioss as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRProveedorProductos) {
                foreach ($this->collRProveedorProductos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRProveedorReputacions) {
                foreach ($this->collRProveedorReputacions as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRProveedorZonaentregas) {
                foreach ($this->collRProveedorZonaentregas as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRUsuarioProveedors) {
                foreach ($this->collRUsuarioProveedors as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collRComercioProveedors instanceof PropelCollection) {
            $this->collRComercioProveedors->clearIterator();
        }
        $this->collRComercioProveedors = null;
        if ($this->collRProveedorCategoriaproductoss instanceof PropelCollection) {
            $this->collRProveedorCategoriaproductoss->clearIterator();
        }
        $this->collRProveedorCategoriaproductoss = null;
        if ($this->collRProveedorListaprecioss instanceof PropelCollection) {
            $this->collRProveedorListaprecioss->clearIterator();
        }
        $this->collRProveedorListaprecioss = null;
        if ($this->collRProveedorProductos instanceof PropelCollection) {
            $this->collRProveedorProductos->clearIterator();
        }
        $this->collRProveedorProductos = null;
        if ($this->collRProveedorReputacions instanceof PropelCollection) {
            $this->collRProveedorReputacions->clearIterator();
        }
        $this->collRProveedorReputacions = null;
        if ($this->collRProveedorZonaentregas instanceof PropelCollection) {
            $this->collRProveedorZonaentregas->clearIterator();
        }
        $this->collRProveedorZonaentregas = null;
        if ($this->collRUsuarioProveedors instanceof PropelCollection) {
            $this->collRUsuarioProveedors->clearIterator();
        }
        $this->collRUsuarioProveedors = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(ProveedorPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
