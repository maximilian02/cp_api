<?php


/**
 * Base class that represents a query for the 'proveedor' table.
 *
 *
 *
 * @method ProveedorQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ProveedorQuery orderByEmail($order = Criteria::ASC) Order by the email column
 * @method ProveedorQuery orderByNombreContacto($order = Criteria::ASC) Order by the nombre_contacto column
 * @method ProveedorQuery orderByPreferenciaPago($order = Criteria::ASC) Order by the preferencia_pago column
 * @method ProveedorQuery orderByPedidoMinimo($order = Criteria::ASC) Order by the pedido_minimo column
 *
 * @method ProveedorQuery groupById() Group by the id column
 * @method ProveedorQuery groupByEmail() Group by the email column
 * @method ProveedorQuery groupByNombreContacto() Group by the nombre_contacto column
 * @method ProveedorQuery groupByPreferenciaPago() Group by the preferencia_pago column
 * @method ProveedorQuery groupByPedidoMinimo() Group by the pedido_minimo column
 *
 * @method ProveedorQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ProveedorQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ProveedorQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ProveedorQuery leftJoinRComercioProveedor($relationAlias = null) Adds a LEFT JOIN clause to the query using the RComercioProveedor relation
 * @method ProveedorQuery rightJoinRComercioProveedor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RComercioProveedor relation
 * @method ProveedorQuery innerJoinRComercioProveedor($relationAlias = null) Adds a INNER JOIN clause to the query using the RComercioProveedor relation
 *
 * @method ProveedorQuery leftJoinRProveedorCategoriaproductos($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProveedorCategoriaproductos relation
 * @method ProveedorQuery rightJoinRProveedorCategoriaproductos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProveedorCategoriaproductos relation
 * @method ProveedorQuery innerJoinRProveedorCategoriaproductos($relationAlias = null) Adds a INNER JOIN clause to the query using the RProveedorCategoriaproductos relation
 *
 * @method ProveedorQuery leftJoinRProveedorListaprecios($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProveedorListaprecios relation
 * @method ProveedorQuery rightJoinRProveedorListaprecios($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProveedorListaprecios relation
 * @method ProveedorQuery innerJoinRProveedorListaprecios($relationAlias = null) Adds a INNER JOIN clause to the query using the RProveedorListaprecios relation
 *
 * @method ProveedorQuery leftJoinRProveedorProducto($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProveedorProducto relation
 * @method ProveedorQuery rightJoinRProveedorProducto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProveedorProducto relation
 * @method ProveedorQuery innerJoinRProveedorProducto($relationAlias = null) Adds a INNER JOIN clause to the query using the RProveedorProducto relation
 *
 * @method ProveedorQuery leftJoinRProveedorReputacion($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProveedorReputacion relation
 * @method ProveedorQuery rightJoinRProveedorReputacion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProveedorReputacion relation
 * @method ProveedorQuery innerJoinRProveedorReputacion($relationAlias = null) Adds a INNER JOIN clause to the query using the RProveedorReputacion relation
 *
 * @method ProveedorQuery leftJoinRProveedorZonaentrega($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProveedorZonaentrega relation
 * @method ProveedorQuery rightJoinRProveedorZonaentrega($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProveedorZonaentrega relation
 * @method ProveedorQuery innerJoinRProveedorZonaentrega($relationAlias = null) Adds a INNER JOIN clause to the query using the RProveedorZonaentrega relation
 *
 * @method ProveedorQuery leftJoinRUsuarioProveedor($relationAlias = null) Adds a LEFT JOIN clause to the query using the RUsuarioProveedor relation
 * @method ProveedorQuery rightJoinRUsuarioProveedor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RUsuarioProveedor relation
 * @method ProveedorQuery innerJoinRUsuarioProveedor($relationAlias = null) Adds a INNER JOIN clause to the query using the RUsuarioProveedor relation
 *
 * @method Proveedor findOne(PropelPDO $con = null) Return the first Proveedor matching the query
 * @method Proveedor findOneOrCreate(PropelPDO $con = null) Return the first Proveedor matching the query, or a new Proveedor object populated from the query conditions when no match is found
 *
 * @method Proveedor findOneByEmail(string $email) Return the first Proveedor filtered by the email column
 * @method Proveedor findOneByNombreContacto(string $nombre_contacto) Return the first Proveedor filtered by the nombre_contacto column
 * @method Proveedor findOneByPreferenciaPago(string $preferencia_pago) Return the first Proveedor filtered by the preferencia_pago column
 * @method Proveedor findOneByPedidoMinimo(int $pedido_minimo) Return the first Proveedor filtered by the pedido_minimo column
 *
 * @method array findById(int $id) Return Proveedor objects filtered by the id column
 * @method array findByEmail(string $email) Return Proveedor objects filtered by the email column
 * @method array findByNombreContacto(string $nombre_contacto) Return Proveedor objects filtered by the nombre_contacto column
 * @method array findByPreferenciaPago(string $preferencia_pago) Return Proveedor objects filtered by the preferencia_pago column
 * @method array findByPedidoMinimo(int $pedido_minimo) Return Proveedor objects filtered by the pedido_minimo column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseProveedorQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseProveedorQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'Proveedor';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ProveedorQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ProveedorQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ProveedorQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ProveedorQuery) {
            return $criteria;
        }
        $query = new ProveedorQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Proveedor|Proveedor[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ProveedorPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ProveedorPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Proveedor A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Proveedor A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `email`, `nombre_contacto`, `preferencia_pago`, `pedido_minimo` FROM `proveedor` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Proveedor();
            $obj->hydrate($row);
            ProveedorPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Proveedor|Proveedor[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Proveedor[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ProveedorPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ProveedorPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ProveedorPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ProveedorPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProveedorPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the email column
     *
     * Example usage:
     * <code>
     * $query->filterByEmail('fooValue');   // WHERE email = 'fooValue'
     * $query->filterByEmail('%fooValue%'); // WHERE email LIKE '%fooValue%'
     * </code>
     *
     * @param     string $email The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function filterByEmail($email = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($email)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $email)) {
                $email = str_replace('*', '%', $email);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProveedorPeer::EMAIL, $email, $comparison);
    }

    /**
     * Filter the query on the nombre_contacto column
     *
     * Example usage:
     * <code>
     * $query->filterByNombreContacto('fooValue');   // WHERE nombre_contacto = 'fooValue'
     * $query->filterByNombreContacto('%fooValue%'); // WHERE nombre_contacto LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombreContacto The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function filterByNombreContacto($nombreContacto = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombreContacto)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nombreContacto)) {
                $nombreContacto = str_replace('*', '%', $nombreContacto);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProveedorPeer::NOMBRE_CONTACTO, $nombreContacto, $comparison);
    }

    /**
     * Filter the query on the preferencia_pago column
     *
     * Example usage:
     * <code>
     * $query->filterByPreferenciaPago('fooValue');   // WHERE preferencia_pago = 'fooValue'
     * $query->filterByPreferenciaPago('%fooValue%'); // WHERE preferencia_pago LIKE '%fooValue%'
     * </code>
     *
     * @param     string $preferenciaPago The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function filterByPreferenciaPago($preferenciaPago = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($preferenciaPago)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $preferenciaPago)) {
                $preferenciaPago = str_replace('*', '%', $preferenciaPago);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ProveedorPeer::PREFERENCIA_PAGO, $preferenciaPago, $comparison);
    }

    /**
     * Filter the query on the pedido_minimo column
     *
     * Example usage:
     * <code>
     * $query->filterByPedidoMinimo(1234); // WHERE pedido_minimo = 1234
     * $query->filterByPedidoMinimo(array(12, 34)); // WHERE pedido_minimo IN (12, 34)
     * $query->filterByPedidoMinimo(array('min' => 12)); // WHERE pedido_minimo >= 12
     * $query->filterByPedidoMinimo(array('max' => 12)); // WHERE pedido_minimo <= 12
     * </code>
     *
     * @param     mixed $pedidoMinimo The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function filterByPedidoMinimo($pedidoMinimo = null, $comparison = null)
    {
        if (is_array($pedidoMinimo)) {
            $useMinMax = false;
            if (isset($pedidoMinimo['min'])) {
                $this->addUsingAlias(ProveedorPeer::PEDIDO_MINIMO, $pedidoMinimo['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($pedidoMinimo['max'])) {
                $this->addUsingAlias(ProveedorPeer::PEDIDO_MINIMO, $pedidoMinimo['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ProveedorPeer::PEDIDO_MINIMO, $pedidoMinimo, $comparison);
    }

    /**
     * Filter the query by a related RComercioProveedor object
     *
     * @param   RComercioProveedor|PropelObjectCollection $rComercioProveedor  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProveedorQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRComercioProveedor($rComercioProveedor, $comparison = null)
    {
        if ($rComercioProveedor instanceof RComercioProveedor) {
            return $this
                ->addUsingAlias(ProveedorPeer::ID, $rComercioProveedor->getProveedorId(), $comparison);
        } elseif ($rComercioProveedor instanceof PropelObjectCollection) {
            return $this
                ->useRComercioProveedorQuery()
                ->filterByPrimaryKeys($rComercioProveedor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRComercioProveedor() only accepts arguments of type RComercioProveedor or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RComercioProveedor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function joinRComercioProveedor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RComercioProveedor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RComercioProveedor');
        }

        return $this;
    }

    /**
     * Use the RComercioProveedor relation RComercioProveedor object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RComercioProveedorQuery A secondary query class using the current class as primary query
     */
    public function useRComercioProveedorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRComercioProveedor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RComercioProveedor', 'RComercioProveedorQuery');
    }

    /**
     * Filter the query by a related RProveedorCategoriaproductos object
     *
     * @param   RProveedorCategoriaproductos|PropelObjectCollection $rProveedorCategoriaproductos  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProveedorQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProveedorCategoriaproductos($rProveedorCategoriaproductos, $comparison = null)
    {
        if ($rProveedorCategoriaproductos instanceof RProveedorCategoriaproductos) {
            return $this
                ->addUsingAlias(ProveedorPeer::ID, $rProveedorCategoriaproductos->getProveedorId(), $comparison);
        } elseif ($rProveedorCategoriaproductos instanceof PropelObjectCollection) {
            return $this
                ->useRProveedorCategoriaproductosQuery()
                ->filterByPrimaryKeys($rProveedorCategoriaproductos->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProveedorCategoriaproductos() only accepts arguments of type RProveedorCategoriaproductos or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProveedorCategoriaproductos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function joinRProveedorCategoriaproductos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProveedorCategoriaproductos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProveedorCategoriaproductos');
        }

        return $this;
    }

    /**
     * Use the RProveedorCategoriaproductos relation RProveedorCategoriaproductos object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProveedorCategoriaproductosQuery A secondary query class using the current class as primary query
     */
    public function useRProveedorCategoriaproductosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProveedorCategoriaproductos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProveedorCategoriaproductos', 'RProveedorCategoriaproductosQuery');
    }

    /**
     * Filter the query by a related RProveedorListaprecios object
     *
     * @param   RProveedorListaprecios|PropelObjectCollection $rProveedorListaprecios  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProveedorQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProveedorListaprecios($rProveedorListaprecios, $comparison = null)
    {
        if ($rProveedorListaprecios instanceof RProveedorListaprecios) {
            return $this
                ->addUsingAlias(ProveedorPeer::ID, $rProveedorListaprecios->getProveedorId(), $comparison);
        } elseif ($rProveedorListaprecios instanceof PropelObjectCollection) {
            return $this
                ->useRProveedorListapreciosQuery()
                ->filterByPrimaryKeys($rProveedorListaprecios->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProveedorListaprecios() only accepts arguments of type RProveedorListaprecios or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProveedorListaprecios relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function joinRProveedorListaprecios($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProveedorListaprecios');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProveedorListaprecios');
        }

        return $this;
    }

    /**
     * Use the RProveedorListaprecios relation RProveedorListaprecios object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProveedorListapreciosQuery A secondary query class using the current class as primary query
     */
    public function useRProveedorListapreciosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProveedorListaprecios($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProveedorListaprecios', 'RProveedorListapreciosQuery');
    }

    /**
     * Filter the query by a related RProveedorProducto object
     *
     * @param   RProveedorProducto|PropelObjectCollection $rProveedorProducto  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProveedorQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProveedorProducto($rProveedorProducto, $comparison = null)
    {
        if ($rProveedorProducto instanceof RProveedorProducto) {
            return $this
                ->addUsingAlias(ProveedorPeer::ID, $rProveedorProducto->getProveedorId(), $comparison);
        } elseif ($rProveedorProducto instanceof PropelObjectCollection) {
            return $this
                ->useRProveedorProductoQuery()
                ->filterByPrimaryKeys($rProveedorProducto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProveedorProducto() only accepts arguments of type RProveedorProducto or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProveedorProducto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function joinRProveedorProducto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProveedorProducto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProveedorProducto');
        }

        return $this;
    }

    /**
     * Use the RProveedorProducto relation RProveedorProducto object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProveedorProductoQuery A secondary query class using the current class as primary query
     */
    public function useRProveedorProductoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProveedorProducto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProveedorProducto', 'RProveedorProductoQuery');
    }

    /**
     * Filter the query by a related RProveedorReputacion object
     *
     * @param   RProveedorReputacion|PropelObjectCollection $rProveedorReputacion  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProveedorQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProveedorReputacion($rProveedorReputacion, $comparison = null)
    {
        if ($rProveedorReputacion instanceof RProveedorReputacion) {
            return $this
                ->addUsingAlias(ProveedorPeer::ID, $rProveedorReputacion->getProveedorId(), $comparison);
        } elseif ($rProveedorReputacion instanceof PropelObjectCollection) {
            return $this
                ->useRProveedorReputacionQuery()
                ->filterByPrimaryKeys($rProveedorReputacion->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProveedorReputacion() only accepts arguments of type RProveedorReputacion or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProveedorReputacion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function joinRProveedorReputacion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProveedorReputacion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProveedorReputacion');
        }

        return $this;
    }

    /**
     * Use the RProveedorReputacion relation RProveedorReputacion object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProveedorReputacionQuery A secondary query class using the current class as primary query
     */
    public function useRProveedorReputacionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProveedorReputacion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProveedorReputacion', 'RProveedorReputacionQuery');
    }

    /**
     * Filter the query by a related RProveedorZonaentrega object
     *
     * @param   RProveedorZonaentrega|PropelObjectCollection $rProveedorZonaentrega  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProveedorQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProveedorZonaentrega($rProveedorZonaentrega, $comparison = null)
    {
        if ($rProveedorZonaentrega instanceof RProveedorZonaentrega) {
            return $this
                ->addUsingAlias(ProveedorPeer::ID, $rProveedorZonaentrega->getProveedorId(), $comparison);
        } elseif ($rProveedorZonaentrega instanceof PropelObjectCollection) {
            return $this
                ->useRProveedorZonaentregaQuery()
                ->filterByPrimaryKeys($rProveedorZonaentrega->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProveedorZonaentrega() only accepts arguments of type RProveedorZonaentrega or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProveedorZonaentrega relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function joinRProveedorZonaentrega($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProveedorZonaentrega');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProveedorZonaentrega');
        }

        return $this;
    }

    /**
     * Use the RProveedorZonaentrega relation RProveedorZonaentrega object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProveedorZonaentregaQuery A secondary query class using the current class as primary query
     */
    public function useRProveedorZonaentregaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProveedorZonaentrega($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProveedorZonaentrega', 'RProveedorZonaentregaQuery');
    }

    /**
     * Filter the query by a related RUsuarioProveedor object
     *
     * @param   RUsuarioProveedor|PropelObjectCollection $rUsuarioProveedor  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ProveedorQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRUsuarioProveedor($rUsuarioProveedor, $comparison = null)
    {
        if ($rUsuarioProveedor instanceof RUsuarioProveedor) {
            return $this
                ->addUsingAlias(ProveedorPeer::ID, $rUsuarioProveedor->getProveedorId(), $comparison);
        } elseif ($rUsuarioProveedor instanceof PropelObjectCollection) {
            return $this
                ->useRUsuarioProveedorQuery()
                ->filterByPrimaryKeys($rUsuarioProveedor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRUsuarioProveedor() only accepts arguments of type RUsuarioProveedor or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RUsuarioProveedor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function joinRUsuarioProveedor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RUsuarioProveedor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RUsuarioProveedor');
        }

        return $this;
    }

    /**
     * Use the RUsuarioProveedor relation RUsuarioProveedor object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RUsuarioProveedorQuery A secondary query class using the current class as primary query
     */
    public function useRUsuarioProveedorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRUsuarioProveedor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RUsuarioProveedor', 'RUsuarioProveedorQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Proveedor $proveedor Object to remove from the list of results
     *
     * @return ProveedorQuery The current query, for fluid interface
     */
    public function prune($proveedor = null)
    {
        if ($proveedor) {
            $this->addUsingAlias(ProveedorPeer::ID, $proveedor->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
