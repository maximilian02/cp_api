<?php


/**
 * Base class that represents a query for the 'r_listaprecios_precio' table.
 *
 *
 *
 * @method RListapreciosPrecioQuery orderById($order = Criteria::ASC) Order by the id column
 * @method RListapreciosPrecioQuery orderByListaPreciosId($order = Criteria::ASC) Order by the lista_precios_id column
 * @method RListapreciosPrecioQuery orderByPrecioId($order = Criteria::ASC) Order by the precio_id column
 *
 * @method RListapreciosPrecioQuery groupById() Group by the id column
 * @method RListapreciosPrecioQuery groupByListaPreciosId() Group by the lista_precios_id column
 * @method RListapreciosPrecioQuery groupByPrecioId() Group by the precio_id column
 *
 * @method RListapreciosPrecioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RListapreciosPrecioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RListapreciosPrecioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RListapreciosPrecioQuery leftJoinListaPrecios($relationAlias = null) Adds a LEFT JOIN clause to the query using the ListaPrecios relation
 * @method RListapreciosPrecioQuery rightJoinListaPrecios($relationAlias = null) Adds a RIGHT JOIN clause to the query using the ListaPrecios relation
 * @method RListapreciosPrecioQuery innerJoinListaPrecios($relationAlias = null) Adds a INNER JOIN clause to the query using the ListaPrecios relation
 *
 * @method RListapreciosPrecioQuery leftJoinPrecio($relationAlias = null) Adds a LEFT JOIN clause to the query using the Precio relation
 * @method RListapreciosPrecioQuery rightJoinPrecio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Precio relation
 * @method RListapreciosPrecioQuery innerJoinPrecio($relationAlias = null) Adds a INNER JOIN clause to the query using the Precio relation
 *
 * @method RListapreciosPrecio findOne(PropelPDO $con = null) Return the first RListapreciosPrecio matching the query
 * @method RListapreciosPrecio findOneOrCreate(PropelPDO $con = null) Return the first RListapreciosPrecio matching the query, or a new RListapreciosPrecio object populated from the query conditions when no match is found
 *
 * @method RListapreciosPrecio findOneByListaPreciosId(int $lista_precios_id) Return the first RListapreciosPrecio filtered by the lista_precios_id column
 * @method RListapreciosPrecio findOneByPrecioId(int $precio_id) Return the first RListapreciosPrecio filtered by the precio_id column
 *
 * @method array findById(int $id) Return RListapreciosPrecio objects filtered by the id column
 * @method array findByListaPreciosId(int $lista_precios_id) Return RListapreciosPrecio objects filtered by the lista_precios_id column
 * @method array findByPrecioId(int $precio_id) Return RListapreciosPrecio objects filtered by the precio_id column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseRListapreciosPrecioQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRListapreciosPrecioQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'RListapreciosPrecio';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RListapreciosPrecioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RListapreciosPrecioQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RListapreciosPrecioQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RListapreciosPrecioQuery) {
            return $criteria;
        }
        $query = new RListapreciosPrecioQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RListapreciosPrecio|RListapreciosPrecio[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RListapreciosPrecioPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RListapreciosPrecioPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RListapreciosPrecio A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RListapreciosPrecio A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `lista_precios_id`, `precio_id` FROM `r_listaprecios_precio` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RListapreciosPrecio();
            $obj->hydrate($row);
            RListapreciosPrecioPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RListapreciosPrecio|RListapreciosPrecio[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RListapreciosPrecio[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RListapreciosPrecioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RListapreciosPrecioPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RListapreciosPrecioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RListapreciosPrecioPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RListapreciosPrecioQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RListapreciosPrecioPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RListapreciosPrecioPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RListapreciosPrecioPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the lista_precios_id column
     *
     * Example usage:
     * <code>
     * $query->filterByListaPreciosId(1234); // WHERE lista_precios_id = 1234
     * $query->filterByListaPreciosId(array(12, 34)); // WHERE lista_precios_id IN (12, 34)
     * $query->filterByListaPreciosId(array('min' => 12)); // WHERE lista_precios_id >= 12
     * $query->filterByListaPreciosId(array('max' => 12)); // WHERE lista_precios_id <= 12
     * </code>
     *
     * @see       filterByListaPrecios()
     *
     * @param     mixed $listaPreciosId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RListapreciosPrecioQuery The current query, for fluid interface
     */
    public function filterByListaPreciosId($listaPreciosId = null, $comparison = null)
    {
        if (is_array($listaPreciosId)) {
            $useMinMax = false;
            if (isset($listaPreciosId['min'])) {
                $this->addUsingAlias(RListapreciosPrecioPeer::LISTA_PRECIOS_ID, $listaPreciosId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($listaPreciosId['max'])) {
                $this->addUsingAlias(RListapreciosPrecioPeer::LISTA_PRECIOS_ID, $listaPreciosId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RListapreciosPrecioPeer::LISTA_PRECIOS_ID, $listaPreciosId, $comparison);
    }

    /**
     * Filter the query on the precio_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPrecioId(1234); // WHERE precio_id = 1234
     * $query->filterByPrecioId(array(12, 34)); // WHERE precio_id IN (12, 34)
     * $query->filterByPrecioId(array('min' => 12)); // WHERE precio_id >= 12
     * $query->filterByPrecioId(array('max' => 12)); // WHERE precio_id <= 12
     * </code>
     *
     * @see       filterByPrecio()
     *
     * @param     mixed $precioId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RListapreciosPrecioQuery The current query, for fluid interface
     */
    public function filterByPrecioId($precioId = null, $comparison = null)
    {
        if (is_array($precioId)) {
            $useMinMax = false;
            if (isset($precioId['min'])) {
                $this->addUsingAlias(RListapreciosPrecioPeer::PRECIO_ID, $precioId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($precioId['max'])) {
                $this->addUsingAlias(RListapreciosPrecioPeer::PRECIO_ID, $precioId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RListapreciosPrecioPeer::PRECIO_ID, $precioId, $comparison);
    }

    /**
     * Filter the query by a related ListaPrecios object
     *
     * @param   ListaPrecios|PropelObjectCollection $listaPrecios The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RListapreciosPrecioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByListaPrecios($listaPrecios, $comparison = null)
    {
        if ($listaPrecios instanceof ListaPrecios) {
            return $this
                ->addUsingAlias(RListapreciosPrecioPeer::LISTA_PRECIOS_ID, $listaPrecios->getId(), $comparison);
        } elseif ($listaPrecios instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RListapreciosPrecioPeer::LISTA_PRECIOS_ID, $listaPrecios->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByListaPrecios() only accepts arguments of type ListaPrecios or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the ListaPrecios relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RListapreciosPrecioQuery The current query, for fluid interface
     */
    public function joinListaPrecios($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('ListaPrecios');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'ListaPrecios');
        }

        return $this;
    }

    /**
     * Use the ListaPrecios relation ListaPrecios object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ListaPreciosQuery A secondary query class using the current class as primary query
     */
    public function useListaPreciosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinListaPrecios($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'ListaPrecios', 'ListaPreciosQuery');
    }

    /**
     * Filter the query by a related Precio object
     *
     * @param   Precio|PropelObjectCollection $precio The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RListapreciosPrecioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrecio($precio, $comparison = null)
    {
        if ($precio instanceof Precio) {
            return $this
                ->addUsingAlias(RListapreciosPrecioPeer::PRECIO_ID, $precio->getId(), $comparison);
        } elseif ($precio instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RListapreciosPrecioPeer::PRECIO_ID, $precio->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPrecio() only accepts arguments of type Precio or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Precio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RListapreciosPrecioQuery The current query, for fluid interface
     */
    public function joinPrecio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Precio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Precio');
        }

        return $this;
    }

    /**
     * Use the Precio relation Precio object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   PrecioQuery A secondary query class using the current class as primary query
     */
    public function usePrecioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrecio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Precio', 'PrecioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   RListapreciosPrecio $rListapreciosPrecio Object to remove from the list of results
     *
     * @return RListapreciosPrecioQuery The current query, for fluid interface
     */
    public function prune($rListapreciosPrecio = null)
    {
        if ($rListapreciosPrecio) {
            $this->addUsingAlias(RListapreciosPrecioPeer::ID, $rListapreciosPrecio->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
