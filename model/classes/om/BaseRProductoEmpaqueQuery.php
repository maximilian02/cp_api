<?php


/**
 * Base class that represents a query for the 'r_producto_empaque' table.
 *
 *
 *
 * @method RProductoEmpaqueQuery orderById($order = Criteria::ASC) Order by the id column
 * @method RProductoEmpaqueQuery orderByProductoId($order = Criteria::ASC) Order by the producto_id column
 * @method RProductoEmpaqueQuery orderByEmpaqueId($order = Criteria::ASC) Order by the empaque_id column
 *
 * @method RProductoEmpaqueQuery groupById() Group by the id column
 * @method RProductoEmpaqueQuery groupByProductoId() Group by the producto_id column
 * @method RProductoEmpaqueQuery groupByEmpaqueId() Group by the empaque_id column
 *
 * @method RProductoEmpaqueQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RProductoEmpaqueQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RProductoEmpaqueQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RProductoEmpaqueQuery leftJoinEmpaque($relationAlias = null) Adds a LEFT JOIN clause to the query using the Empaque relation
 * @method RProductoEmpaqueQuery rightJoinEmpaque($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Empaque relation
 * @method RProductoEmpaqueQuery innerJoinEmpaque($relationAlias = null) Adds a INNER JOIN clause to the query using the Empaque relation
 *
 * @method RProductoEmpaqueQuery leftJoinProducto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Producto relation
 * @method RProductoEmpaqueQuery rightJoinProducto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Producto relation
 * @method RProductoEmpaqueQuery innerJoinProducto($relationAlias = null) Adds a INNER JOIN clause to the query using the Producto relation
 *
 * @method RProductoEmpaque findOne(PropelPDO $con = null) Return the first RProductoEmpaque matching the query
 * @method RProductoEmpaque findOneOrCreate(PropelPDO $con = null) Return the first RProductoEmpaque matching the query, or a new RProductoEmpaque object populated from the query conditions when no match is found
 *
 * @method RProductoEmpaque findOneByProductoId(int $producto_id) Return the first RProductoEmpaque filtered by the producto_id column
 * @method RProductoEmpaque findOneByEmpaqueId(int $empaque_id) Return the first RProductoEmpaque filtered by the empaque_id column
 *
 * @method array findById(int $id) Return RProductoEmpaque objects filtered by the id column
 * @method array findByProductoId(int $producto_id) Return RProductoEmpaque objects filtered by the producto_id column
 * @method array findByEmpaqueId(int $empaque_id) Return RProductoEmpaque objects filtered by the empaque_id column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseRProductoEmpaqueQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRProductoEmpaqueQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'RProductoEmpaque';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RProductoEmpaqueQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RProductoEmpaqueQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RProductoEmpaqueQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RProductoEmpaqueQuery) {
            return $criteria;
        }
        $query = new RProductoEmpaqueQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RProductoEmpaque|RProductoEmpaque[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RProductoEmpaquePeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RProductoEmpaquePeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RProductoEmpaque A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RProductoEmpaque A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `producto_id`, `empaque_id` FROM `r_producto_empaque` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RProductoEmpaque();
            $obj->hydrate($row);
            RProductoEmpaquePeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RProductoEmpaque|RProductoEmpaque[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RProductoEmpaque[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RProductoEmpaqueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RProductoEmpaquePeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RProductoEmpaqueQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RProductoEmpaquePeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RProductoEmpaqueQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RProductoEmpaquePeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RProductoEmpaquePeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RProductoEmpaquePeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the producto_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProductoId(1234); // WHERE producto_id = 1234
     * $query->filterByProductoId(array(12, 34)); // WHERE producto_id IN (12, 34)
     * $query->filterByProductoId(array('min' => 12)); // WHERE producto_id >= 12
     * $query->filterByProductoId(array('max' => 12)); // WHERE producto_id <= 12
     * </code>
     *
     * @see       filterByProducto()
     *
     * @param     mixed $productoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RProductoEmpaqueQuery The current query, for fluid interface
     */
    public function filterByProductoId($productoId = null, $comparison = null)
    {
        if (is_array($productoId)) {
            $useMinMax = false;
            if (isset($productoId['min'])) {
                $this->addUsingAlias(RProductoEmpaquePeer::PRODUCTO_ID, $productoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productoId['max'])) {
                $this->addUsingAlias(RProductoEmpaquePeer::PRODUCTO_ID, $productoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RProductoEmpaquePeer::PRODUCTO_ID, $productoId, $comparison);
    }

    /**
     * Filter the query on the empaque_id column
     *
     * Example usage:
     * <code>
     * $query->filterByEmpaqueId(1234); // WHERE empaque_id = 1234
     * $query->filterByEmpaqueId(array(12, 34)); // WHERE empaque_id IN (12, 34)
     * $query->filterByEmpaqueId(array('min' => 12)); // WHERE empaque_id >= 12
     * $query->filterByEmpaqueId(array('max' => 12)); // WHERE empaque_id <= 12
     * </code>
     *
     * @see       filterByEmpaque()
     *
     * @param     mixed $empaqueId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RProductoEmpaqueQuery The current query, for fluid interface
     */
    public function filterByEmpaqueId($empaqueId = null, $comparison = null)
    {
        if (is_array($empaqueId)) {
            $useMinMax = false;
            if (isset($empaqueId['min'])) {
                $this->addUsingAlias(RProductoEmpaquePeer::EMPAQUE_ID, $empaqueId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($empaqueId['max'])) {
                $this->addUsingAlias(RProductoEmpaquePeer::EMPAQUE_ID, $empaqueId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RProductoEmpaquePeer::EMPAQUE_ID, $empaqueId, $comparison);
    }

    /**
     * Filter the query by a related Empaque object
     *
     * @param   Empaque|PropelObjectCollection $empaque The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RProductoEmpaqueQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByEmpaque($empaque, $comparison = null)
    {
        if ($empaque instanceof Empaque) {
            return $this
                ->addUsingAlias(RProductoEmpaquePeer::EMPAQUE_ID, $empaque->getId(), $comparison);
        } elseif ($empaque instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RProductoEmpaquePeer::EMPAQUE_ID, $empaque->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByEmpaque() only accepts arguments of type Empaque or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Empaque relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RProductoEmpaqueQuery The current query, for fluid interface
     */
    public function joinEmpaque($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Empaque');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Empaque');
        }

        return $this;
    }

    /**
     * Use the Empaque relation Empaque object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   EmpaqueQuery A secondary query class using the current class as primary query
     */
    public function useEmpaqueQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinEmpaque($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Empaque', 'EmpaqueQuery');
    }

    /**
     * Filter the query by a related Producto object
     *
     * @param   Producto|PropelObjectCollection $producto The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RProductoEmpaqueQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByProducto($producto, $comparison = null)
    {
        if ($producto instanceof Producto) {
            return $this
                ->addUsingAlias(RProductoEmpaquePeer::PRODUCTO_ID, $producto->getId(), $comparison);
        } elseif ($producto instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RProductoEmpaquePeer::PRODUCTO_ID, $producto->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProducto() only accepts arguments of type Producto or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Producto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RProductoEmpaqueQuery The current query, for fluid interface
     */
    public function joinProducto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Producto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Producto');
        }

        return $this;
    }

    /**
     * Use the Producto relation Producto object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ProductoQuery A secondary query class using the current class as primary query
     */
    public function useProductoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProducto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Producto', 'ProductoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   RProductoEmpaque $rProductoEmpaque Object to remove from the list of results
     *
     * @return RProductoEmpaqueQuery The current query, for fluid interface
     */
    public function prune($rProductoEmpaque = null)
    {
        if ($rProductoEmpaque) {
            $this->addUsingAlias(RProductoEmpaquePeer::ID, $rProductoEmpaque->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
