<?php


/**
 * Base class that represents a query for the 'r_producto_precio' table.
 *
 *
 *
 * @method RProductoPrecioQuery orderById($order = Criteria::ASC) Order by the id column
 * @method RProductoPrecioQuery orderByProductoId($order = Criteria::ASC) Order by the producto_id column
 * @method RProductoPrecioQuery orderByPrecioId($order = Criteria::ASC) Order by the precio_id column
 *
 * @method RProductoPrecioQuery groupById() Group by the id column
 * @method RProductoPrecioQuery groupByProductoId() Group by the producto_id column
 * @method RProductoPrecioQuery groupByPrecioId() Group by the precio_id column
 *
 * @method RProductoPrecioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RProductoPrecioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RProductoPrecioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RProductoPrecioQuery leftJoinPrecio($relationAlias = null) Adds a LEFT JOIN clause to the query using the Precio relation
 * @method RProductoPrecioQuery rightJoinPrecio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Precio relation
 * @method RProductoPrecioQuery innerJoinPrecio($relationAlias = null) Adds a INNER JOIN clause to the query using the Precio relation
 *
 * @method RProductoPrecioQuery leftJoinProducto($relationAlias = null) Adds a LEFT JOIN clause to the query using the Producto relation
 * @method RProductoPrecioQuery rightJoinProducto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Producto relation
 * @method RProductoPrecioQuery innerJoinProducto($relationAlias = null) Adds a INNER JOIN clause to the query using the Producto relation
 *
 * @method RProductoPrecio findOne(PropelPDO $con = null) Return the first RProductoPrecio matching the query
 * @method RProductoPrecio findOneOrCreate(PropelPDO $con = null) Return the first RProductoPrecio matching the query, or a new RProductoPrecio object populated from the query conditions when no match is found
 *
 * @method RProductoPrecio findOneByProductoId(int $producto_id) Return the first RProductoPrecio filtered by the producto_id column
 * @method RProductoPrecio findOneByPrecioId(int $precio_id) Return the first RProductoPrecio filtered by the precio_id column
 *
 * @method array findById(int $id) Return RProductoPrecio objects filtered by the id column
 * @method array findByProductoId(int $producto_id) Return RProductoPrecio objects filtered by the producto_id column
 * @method array findByPrecioId(int $precio_id) Return RProductoPrecio objects filtered by the precio_id column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseRProductoPrecioQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRProductoPrecioQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'RProductoPrecio';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RProductoPrecioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RProductoPrecioQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RProductoPrecioQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RProductoPrecioQuery) {
            return $criteria;
        }
        $query = new RProductoPrecioQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RProductoPrecio|RProductoPrecio[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RProductoPrecioPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RProductoPrecioPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RProductoPrecio A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RProductoPrecio A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `producto_id`, `precio_id` FROM `r_producto_precio` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RProductoPrecio();
            $obj->hydrate($row);
            RProductoPrecioPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RProductoPrecio|RProductoPrecio[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RProductoPrecio[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RProductoPrecioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RProductoPrecioPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RProductoPrecioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RProductoPrecioPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RProductoPrecioQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RProductoPrecioPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RProductoPrecioPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RProductoPrecioPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the producto_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProductoId(1234); // WHERE producto_id = 1234
     * $query->filterByProductoId(array(12, 34)); // WHERE producto_id IN (12, 34)
     * $query->filterByProductoId(array('min' => 12)); // WHERE producto_id >= 12
     * $query->filterByProductoId(array('max' => 12)); // WHERE producto_id <= 12
     * </code>
     *
     * @see       filterByProducto()
     *
     * @param     mixed $productoId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RProductoPrecioQuery The current query, for fluid interface
     */
    public function filterByProductoId($productoId = null, $comparison = null)
    {
        if (is_array($productoId)) {
            $useMinMax = false;
            if (isset($productoId['min'])) {
                $this->addUsingAlias(RProductoPrecioPeer::PRODUCTO_ID, $productoId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($productoId['max'])) {
                $this->addUsingAlias(RProductoPrecioPeer::PRODUCTO_ID, $productoId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RProductoPrecioPeer::PRODUCTO_ID, $productoId, $comparison);
    }

    /**
     * Filter the query on the precio_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPrecioId(1234); // WHERE precio_id = 1234
     * $query->filterByPrecioId(array(12, 34)); // WHERE precio_id IN (12, 34)
     * $query->filterByPrecioId(array('min' => 12)); // WHERE precio_id >= 12
     * $query->filterByPrecioId(array('max' => 12)); // WHERE precio_id <= 12
     * </code>
     *
     * @see       filterByPrecio()
     *
     * @param     mixed $precioId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RProductoPrecioQuery The current query, for fluid interface
     */
    public function filterByPrecioId($precioId = null, $comparison = null)
    {
        if (is_array($precioId)) {
            $useMinMax = false;
            if (isset($precioId['min'])) {
                $this->addUsingAlias(RProductoPrecioPeer::PRECIO_ID, $precioId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($precioId['max'])) {
                $this->addUsingAlias(RProductoPrecioPeer::PRECIO_ID, $precioId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RProductoPrecioPeer::PRECIO_ID, $precioId, $comparison);
    }

    /**
     * Filter the query by a related Precio object
     *
     * @param   Precio|PropelObjectCollection $precio The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RProductoPrecioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByPrecio($precio, $comparison = null)
    {
        if ($precio instanceof Precio) {
            return $this
                ->addUsingAlias(RProductoPrecioPeer::PRECIO_ID, $precio->getId(), $comparison);
        } elseif ($precio instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RProductoPrecioPeer::PRECIO_ID, $precio->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByPrecio() only accepts arguments of type Precio or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Precio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RProductoPrecioQuery The current query, for fluid interface
     */
    public function joinPrecio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Precio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Precio');
        }

        return $this;
    }

    /**
     * Use the Precio relation Precio object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   PrecioQuery A secondary query class using the current class as primary query
     */
    public function usePrecioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPrecio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Precio', 'PrecioQuery');
    }

    /**
     * Filter the query by a related Producto object
     *
     * @param   Producto|PropelObjectCollection $producto The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RProductoPrecioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByProducto($producto, $comparison = null)
    {
        if ($producto instanceof Producto) {
            return $this
                ->addUsingAlias(RProductoPrecioPeer::PRODUCTO_ID, $producto->getId(), $comparison);
        } elseif ($producto instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RProductoPrecioPeer::PRODUCTO_ID, $producto->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProducto() only accepts arguments of type Producto or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Producto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RProductoPrecioQuery The current query, for fluid interface
     */
    public function joinProducto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Producto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Producto');
        }

        return $this;
    }

    /**
     * Use the Producto relation Producto object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ProductoQuery A secondary query class using the current class as primary query
     */
    public function useProductoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProducto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Producto', 'ProductoQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   RProductoPrecio $rProductoPrecio Object to remove from the list of results
     *
     * @return RProductoPrecioQuery The current query, for fluid interface
     */
    public function prune($rProductoPrecio = null)
    {
        if ($rProductoPrecio) {
            $this->addUsingAlias(RProductoPrecioPeer::ID, $rProductoPrecio->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
