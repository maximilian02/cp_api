<?php


/**
 * Base class that represents a query for the 'r_proveedor_categoriaproductos' table.
 *
 *
 *
 * @method RProveedorCategoriaproductosQuery orderById($order = Criteria::ASC) Order by the id column
 * @method RProveedorCategoriaproductosQuery orderByProveedorId($order = Criteria::ASC) Order by the proveedor_id column
 * @method RProveedorCategoriaproductosQuery orderByCaterogiaProductosId($order = Criteria::ASC) Order by the caterogia_productos_id column
 *
 * @method RProveedorCategoriaproductosQuery groupById() Group by the id column
 * @method RProveedorCategoriaproductosQuery groupByProveedorId() Group by the proveedor_id column
 * @method RProveedorCategoriaproductosQuery groupByCaterogiaProductosId() Group by the caterogia_productos_id column
 *
 * @method RProveedorCategoriaproductosQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RProveedorCategoriaproductosQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RProveedorCategoriaproductosQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RProveedorCategoriaproductosQuery leftJoinCategoriaProductos($relationAlias = null) Adds a LEFT JOIN clause to the query using the CategoriaProductos relation
 * @method RProveedorCategoriaproductosQuery rightJoinCategoriaProductos($relationAlias = null) Adds a RIGHT JOIN clause to the query using the CategoriaProductos relation
 * @method RProveedorCategoriaproductosQuery innerJoinCategoriaProductos($relationAlias = null) Adds a INNER JOIN clause to the query using the CategoriaProductos relation
 *
 * @method RProveedorCategoriaproductosQuery leftJoinProveedor($relationAlias = null) Adds a LEFT JOIN clause to the query using the Proveedor relation
 * @method RProveedorCategoriaproductosQuery rightJoinProveedor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Proveedor relation
 * @method RProveedorCategoriaproductosQuery innerJoinProveedor($relationAlias = null) Adds a INNER JOIN clause to the query using the Proveedor relation
 *
 * @method RProveedorCategoriaproductos findOne(PropelPDO $con = null) Return the first RProveedorCategoriaproductos matching the query
 * @method RProveedorCategoriaproductos findOneOrCreate(PropelPDO $con = null) Return the first RProveedorCategoriaproductos matching the query, or a new RProveedorCategoriaproductos object populated from the query conditions when no match is found
 *
 * @method RProveedorCategoriaproductos findOneByProveedorId(int $proveedor_id) Return the first RProveedorCategoriaproductos filtered by the proveedor_id column
 * @method RProveedorCategoriaproductos findOneByCaterogiaProductosId(int $caterogia_productos_id) Return the first RProveedorCategoriaproductos filtered by the caterogia_productos_id column
 *
 * @method array findById(int $id) Return RProveedorCategoriaproductos objects filtered by the id column
 * @method array findByProveedorId(int $proveedor_id) Return RProveedorCategoriaproductos objects filtered by the proveedor_id column
 * @method array findByCaterogiaProductosId(int $caterogia_productos_id) Return RProveedorCategoriaproductos objects filtered by the caterogia_productos_id column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseRProveedorCategoriaproductosQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRProveedorCategoriaproductosQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'RProveedorCategoriaproductos';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RProveedorCategoriaproductosQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RProveedorCategoriaproductosQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RProveedorCategoriaproductosQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RProveedorCategoriaproductosQuery) {
            return $criteria;
        }
        $query = new RProveedorCategoriaproductosQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RProveedorCategoriaproductos|RProveedorCategoriaproductos[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RProveedorCategoriaproductosPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RProveedorCategoriaproductosPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RProveedorCategoriaproductos A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RProveedorCategoriaproductos A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `proveedor_id`, `caterogia_productos_id` FROM `r_proveedor_categoriaproductos` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RProveedorCategoriaproductos();
            $obj->hydrate($row);
            RProveedorCategoriaproductosPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RProveedorCategoriaproductos|RProveedorCategoriaproductos[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RProveedorCategoriaproductos[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RProveedorCategoriaproductosQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RProveedorCategoriaproductosPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RProveedorCategoriaproductosQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RProveedorCategoriaproductosPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RProveedorCategoriaproductosQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RProveedorCategoriaproductosPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RProveedorCategoriaproductosPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RProveedorCategoriaproductosPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the proveedor_id column
     *
     * Example usage:
     * <code>
     * $query->filterByProveedorId(1234); // WHERE proveedor_id = 1234
     * $query->filterByProveedorId(array(12, 34)); // WHERE proveedor_id IN (12, 34)
     * $query->filterByProveedorId(array('min' => 12)); // WHERE proveedor_id >= 12
     * $query->filterByProveedorId(array('max' => 12)); // WHERE proveedor_id <= 12
     * </code>
     *
     * @see       filterByProveedor()
     *
     * @param     mixed $proveedorId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RProveedorCategoriaproductosQuery The current query, for fluid interface
     */
    public function filterByProveedorId($proveedorId = null, $comparison = null)
    {
        if (is_array($proveedorId)) {
            $useMinMax = false;
            if (isset($proveedorId['min'])) {
                $this->addUsingAlias(RProveedorCategoriaproductosPeer::PROVEEDOR_ID, $proveedorId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($proveedorId['max'])) {
                $this->addUsingAlias(RProveedorCategoriaproductosPeer::PROVEEDOR_ID, $proveedorId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RProveedorCategoriaproductosPeer::PROVEEDOR_ID, $proveedorId, $comparison);
    }

    /**
     * Filter the query on the caterogia_productos_id column
     *
     * Example usage:
     * <code>
     * $query->filterByCaterogiaProductosId(1234); // WHERE caterogia_productos_id = 1234
     * $query->filterByCaterogiaProductosId(array(12, 34)); // WHERE caterogia_productos_id IN (12, 34)
     * $query->filterByCaterogiaProductosId(array('min' => 12)); // WHERE caterogia_productos_id >= 12
     * $query->filterByCaterogiaProductosId(array('max' => 12)); // WHERE caterogia_productos_id <= 12
     * </code>
     *
     * @see       filterByCategoriaProductos()
     *
     * @param     mixed $caterogiaProductosId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RProveedorCategoriaproductosQuery The current query, for fluid interface
     */
    public function filterByCaterogiaProductosId($caterogiaProductosId = null, $comparison = null)
    {
        if (is_array($caterogiaProductosId)) {
            $useMinMax = false;
            if (isset($caterogiaProductosId['min'])) {
                $this->addUsingAlias(RProveedorCategoriaproductosPeer::CATEROGIA_PRODUCTOS_ID, $caterogiaProductosId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($caterogiaProductosId['max'])) {
                $this->addUsingAlias(RProveedorCategoriaproductosPeer::CATEROGIA_PRODUCTOS_ID, $caterogiaProductosId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RProveedorCategoriaproductosPeer::CATEROGIA_PRODUCTOS_ID, $caterogiaProductosId, $comparison);
    }

    /**
     * Filter the query by a related CategoriaProductos object
     *
     * @param   CategoriaProductos|PropelObjectCollection $categoriaProductos The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RProveedorCategoriaproductosQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByCategoriaProductos($categoriaProductos, $comparison = null)
    {
        if ($categoriaProductos instanceof CategoriaProductos) {
            return $this
                ->addUsingAlias(RProveedorCategoriaproductosPeer::CATEROGIA_PRODUCTOS_ID, $categoriaProductos->getId(), $comparison);
        } elseif ($categoriaProductos instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RProveedorCategoriaproductosPeer::CATEROGIA_PRODUCTOS_ID, $categoriaProductos->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByCategoriaProductos() only accepts arguments of type CategoriaProductos or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the CategoriaProductos relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RProveedorCategoriaproductosQuery The current query, for fluid interface
     */
    public function joinCategoriaProductos($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('CategoriaProductos');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'CategoriaProductos');
        }

        return $this;
    }

    /**
     * Use the CategoriaProductos relation CategoriaProductos object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   CategoriaProductosQuery A secondary query class using the current class as primary query
     */
    public function useCategoriaProductosQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinCategoriaProductos($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'CategoriaProductos', 'CategoriaProductosQuery');
    }

    /**
     * Filter the query by a related Proveedor object
     *
     * @param   Proveedor|PropelObjectCollection $proveedor The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RProveedorCategoriaproductosQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByProveedor($proveedor, $comparison = null)
    {
        if ($proveedor instanceof Proveedor) {
            return $this
                ->addUsingAlias(RProveedorCategoriaproductosPeer::PROVEEDOR_ID, $proveedor->getId(), $comparison);
        } elseif ($proveedor instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RProveedorCategoriaproductosPeer::PROVEEDOR_ID, $proveedor->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByProveedor() only accepts arguments of type Proveedor or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Proveedor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RProveedorCategoriaproductosQuery The current query, for fluid interface
     */
    public function joinProveedor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Proveedor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Proveedor');
        }

        return $this;
    }

    /**
     * Use the Proveedor relation Proveedor object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ProveedorQuery A secondary query class using the current class as primary query
     */
    public function useProveedorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinProveedor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Proveedor', 'ProveedorQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   RProveedorCategoriaproductos $rProveedorCategoriaproductos Object to remove from the list of results
     *
     * @return RProveedorCategoriaproductosQuery The current query, for fluid interface
     */
    public function prune($rProveedorCategoriaproductos = null)
    {
        if ($rProveedorCategoriaproductos) {
            $this->addUsingAlias(RProveedorCategoriaproductosPeer::ID, $rProveedorCategoriaproductos->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
