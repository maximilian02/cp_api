<?php


/**
 * Base class that represents a query for the 'r_usuario_comercio' table.
 *
 *
 *
 * @method RUsuarioComercioQuery orderById($order = Criteria::ASC) Order by the id column
 * @method RUsuarioComercioQuery orderByUsuarioId($order = Criteria::ASC) Order by the usuario_id column
 * @method RUsuarioComercioQuery orderByComercioId($order = Criteria::ASC) Order by the comercio_id column
 *
 * @method RUsuarioComercioQuery groupById() Group by the id column
 * @method RUsuarioComercioQuery groupByUsuarioId() Group by the usuario_id column
 * @method RUsuarioComercioQuery groupByComercioId() Group by the comercio_id column
 *
 * @method RUsuarioComercioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RUsuarioComercioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RUsuarioComercioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RUsuarioComercioQuery leftJoinComercio($relationAlias = null) Adds a LEFT JOIN clause to the query using the Comercio relation
 * @method RUsuarioComercioQuery rightJoinComercio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Comercio relation
 * @method RUsuarioComercioQuery innerJoinComercio($relationAlias = null) Adds a INNER JOIN clause to the query using the Comercio relation
 *
 * @method RUsuarioComercioQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method RUsuarioComercioQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method RUsuarioComercioQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method RUsuarioComercio findOne(PropelPDO $con = null) Return the first RUsuarioComercio matching the query
 * @method RUsuarioComercio findOneOrCreate(PropelPDO $con = null) Return the first RUsuarioComercio matching the query, or a new RUsuarioComercio object populated from the query conditions when no match is found
 *
 * @method RUsuarioComercio findOneByUsuarioId(int $usuario_id) Return the first RUsuarioComercio filtered by the usuario_id column
 * @method RUsuarioComercio findOneByComercioId(int $comercio_id) Return the first RUsuarioComercio filtered by the comercio_id column
 *
 * @method array findById(int $id) Return RUsuarioComercio objects filtered by the id column
 * @method array findByUsuarioId(int $usuario_id) Return RUsuarioComercio objects filtered by the usuario_id column
 * @method array findByComercioId(int $comercio_id) Return RUsuarioComercio objects filtered by the comercio_id column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseRUsuarioComercioQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRUsuarioComercioQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'RUsuarioComercio';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RUsuarioComercioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RUsuarioComercioQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RUsuarioComercioQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RUsuarioComercioQuery) {
            return $criteria;
        }
        $query = new RUsuarioComercioQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RUsuarioComercio|RUsuarioComercio[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RUsuarioComercioPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RUsuarioComercioPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RUsuarioComercio A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RUsuarioComercio A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `usuario_id`, `comercio_id` FROM `r_usuario_comercio` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RUsuarioComercio();
            $obj->hydrate($row);
            RUsuarioComercioPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RUsuarioComercio|RUsuarioComercio[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RUsuarioComercio[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RUsuarioComercioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RUsuarioComercioPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RUsuarioComercioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RUsuarioComercioPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RUsuarioComercioQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RUsuarioComercioPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RUsuarioComercioPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RUsuarioComercioPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the usuario_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioId(1234); // WHERE usuario_id = 1234
     * $query->filterByUsuarioId(array(12, 34)); // WHERE usuario_id IN (12, 34)
     * $query->filterByUsuarioId(array('min' => 12)); // WHERE usuario_id >= 12
     * $query->filterByUsuarioId(array('max' => 12)); // WHERE usuario_id <= 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RUsuarioComercioQuery The current query, for fluid interface
     */
    public function filterByUsuarioId($usuarioId = null, $comparison = null)
    {
        if (is_array($usuarioId)) {
            $useMinMax = false;
            if (isset($usuarioId['min'])) {
                $this->addUsingAlias(RUsuarioComercioPeer::USUARIO_ID, $usuarioId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioId['max'])) {
                $this->addUsingAlias(RUsuarioComercioPeer::USUARIO_ID, $usuarioId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RUsuarioComercioPeer::USUARIO_ID, $usuarioId, $comparison);
    }

    /**
     * Filter the query on the comercio_id column
     *
     * Example usage:
     * <code>
     * $query->filterByComercioId(1234); // WHERE comercio_id = 1234
     * $query->filterByComercioId(array(12, 34)); // WHERE comercio_id IN (12, 34)
     * $query->filterByComercioId(array('min' => 12)); // WHERE comercio_id >= 12
     * $query->filterByComercioId(array('max' => 12)); // WHERE comercio_id <= 12
     * </code>
     *
     * @see       filterByComercio()
     *
     * @param     mixed $comercioId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RUsuarioComercioQuery The current query, for fluid interface
     */
    public function filterByComercioId($comercioId = null, $comparison = null)
    {
        if (is_array($comercioId)) {
            $useMinMax = false;
            if (isset($comercioId['min'])) {
                $this->addUsingAlias(RUsuarioComercioPeer::COMERCIO_ID, $comercioId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($comercioId['max'])) {
                $this->addUsingAlias(RUsuarioComercioPeer::COMERCIO_ID, $comercioId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RUsuarioComercioPeer::COMERCIO_ID, $comercioId, $comparison);
    }

    /**
     * Filter the query by a related Comercio object
     *
     * @param   Comercio|PropelObjectCollection $comercio The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RUsuarioComercioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByComercio($comercio, $comparison = null)
    {
        if ($comercio instanceof Comercio) {
            return $this
                ->addUsingAlias(RUsuarioComercioPeer::COMERCIO_ID, $comercio->getId(), $comparison);
        } elseif ($comercio instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RUsuarioComercioPeer::COMERCIO_ID, $comercio->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByComercio() only accepts arguments of type Comercio or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Comercio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RUsuarioComercioQuery The current query, for fluid interface
     */
    public function joinComercio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Comercio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Comercio');
        }

        return $this;
    }

    /**
     * Use the Comercio relation Comercio object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   ComercioQuery A secondary query class using the current class as primary query
     */
    public function useComercioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinComercio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Comercio', 'ComercioQuery');
    }

    /**
     * Filter the query by a related Usuario object
     *
     * @param   Usuario|PropelObjectCollection $usuario The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RUsuarioComercioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof Usuario) {
            return $this
                ->addUsingAlias(RUsuarioComercioPeer::USUARIO_ID, $usuario->getId(), $comparison);
        } elseif ($usuario instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RUsuarioComercioPeer::USUARIO_ID, $usuario->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type Usuario or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RUsuarioComercioQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', 'UsuarioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   RUsuarioComercio $rUsuarioComercio Object to remove from the list of results
     *
     * @return RUsuarioComercioQuery The current query, for fluid interface
     */
    public function prune($rUsuarioComercio = null)
    {
        if ($rUsuarioComercio) {
            $this->addUsingAlias(RUsuarioComercioPeer::ID, $rUsuarioComercio->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
