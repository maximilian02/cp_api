<?php


/**
 * Base class that represents a query for the 'r_usuario_tipousuario' table.
 *
 *
 *
 * @method RUsuarioTipousuarioQuery orderById($order = Criteria::ASC) Order by the id column
 * @method RUsuarioTipousuarioQuery orderByUsuarioId($order = Criteria::ASC) Order by the usuario_id column
 * @method RUsuarioTipousuarioQuery orderByTipoUsuarioId($order = Criteria::ASC) Order by the tipo_usuario_id column
 *
 * @method RUsuarioTipousuarioQuery groupById() Group by the id column
 * @method RUsuarioTipousuarioQuery groupByUsuarioId() Group by the usuario_id column
 * @method RUsuarioTipousuarioQuery groupByTipoUsuarioId() Group by the tipo_usuario_id column
 *
 * @method RUsuarioTipousuarioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RUsuarioTipousuarioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RUsuarioTipousuarioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RUsuarioTipousuarioQuery leftJoinTipoUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the TipoUsuario relation
 * @method RUsuarioTipousuarioQuery rightJoinTipoUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the TipoUsuario relation
 * @method RUsuarioTipousuarioQuery innerJoinTipoUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the TipoUsuario relation
 *
 * @method RUsuarioTipousuarioQuery leftJoinUsuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the Usuario relation
 * @method RUsuarioTipousuarioQuery rightJoinUsuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Usuario relation
 * @method RUsuarioTipousuarioQuery innerJoinUsuario($relationAlias = null) Adds a INNER JOIN clause to the query using the Usuario relation
 *
 * @method RUsuarioTipousuario findOne(PropelPDO $con = null) Return the first RUsuarioTipousuario matching the query
 * @method RUsuarioTipousuario findOneOrCreate(PropelPDO $con = null) Return the first RUsuarioTipousuario matching the query, or a new RUsuarioTipousuario object populated from the query conditions when no match is found
 *
 * @method RUsuarioTipousuario findOneByUsuarioId(int $usuario_id) Return the first RUsuarioTipousuario filtered by the usuario_id column
 * @method RUsuarioTipousuario findOneByTipoUsuarioId(int $tipo_usuario_id) Return the first RUsuarioTipousuario filtered by the tipo_usuario_id column
 *
 * @method array findById(int $id) Return RUsuarioTipousuario objects filtered by the id column
 * @method array findByUsuarioId(int $usuario_id) Return RUsuarioTipousuario objects filtered by the usuario_id column
 * @method array findByTipoUsuarioId(int $tipo_usuario_id) Return RUsuarioTipousuario objects filtered by the tipo_usuario_id column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseRUsuarioTipousuarioQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRUsuarioTipousuarioQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'RUsuarioTipousuario';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RUsuarioTipousuarioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RUsuarioTipousuarioQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RUsuarioTipousuarioQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RUsuarioTipousuarioQuery) {
            return $criteria;
        }
        $query = new RUsuarioTipousuarioQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RUsuarioTipousuario|RUsuarioTipousuario[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RUsuarioTipousuarioPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RUsuarioTipousuarioPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RUsuarioTipousuario A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RUsuarioTipousuario A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `usuario_id`, `tipo_usuario_id` FROM `r_usuario_tipousuario` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RUsuarioTipousuario();
            $obj->hydrate($row);
            RUsuarioTipousuarioPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RUsuarioTipousuario|RUsuarioTipousuario[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RUsuarioTipousuario[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RUsuarioTipousuarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RUsuarioTipousuarioPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RUsuarioTipousuarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RUsuarioTipousuarioPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RUsuarioTipousuarioQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(RUsuarioTipousuarioPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(RUsuarioTipousuarioPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RUsuarioTipousuarioPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the usuario_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioId(1234); // WHERE usuario_id = 1234
     * $query->filterByUsuarioId(array(12, 34)); // WHERE usuario_id IN (12, 34)
     * $query->filterByUsuarioId(array('min' => 12)); // WHERE usuario_id >= 12
     * $query->filterByUsuarioId(array('max' => 12)); // WHERE usuario_id <= 12
     * </code>
     *
     * @see       filterByUsuario()
     *
     * @param     mixed $usuarioId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RUsuarioTipousuarioQuery The current query, for fluid interface
     */
    public function filterByUsuarioId($usuarioId = null, $comparison = null)
    {
        if (is_array($usuarioId)) {
            $useMinMax = false;
            if (isset($usuarioId['min'])) {
                $this->addUsingAlias(RUsuarioTipousuarioPeer::USUARIO_ID, $usuarioId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioId['max'])) {
                $this->addUsingAlias(RUsuarioTipousuarioPeer::USUARIO_ID, $usuarioId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RUsuarioTipousuarioPeer::USUARIO_ID, $usuarioId, $comparison);
    }

    /**
     * Filter the query on the tipo_usuario_id column
     *
     * Example usage:
     * <code>
     * $query->filterByTipoUsuarioId(1234); // WHERE tipo_usuario_id = 1234
     * $query->filterByTipoUsuarioId(array(12, 34)); // WHERE tipo_usuario_id IN (12, 34)
     * $query->filterByTipoUsuarioId(array('min' => 12)); // WHERE tipo_usuario_id >= 12
     * $query->filterByTipoUsuarioId(array('max' => 12)); // WHERE tipo_usuario_id <= 12
     * </code>
     *
     * @see       filterByTipoUsuario()
     *
     * @param     mixed $tipoUsuarioId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RUsuarioTipousuarioQuery The current query, for fluid interface
     */
    public function filterByTipoUsuarioId($tipoUsuarioId = null, $comparison = null)
    {
        if (is_array($tipoUsuarioId)) {
            $useMinMax = false;
            if (isset($tipoUsuarioId['min'])) {
                $this->addUsingAlias(RUsuarioTipousuarioPeer::TIPO_USUARIO_ID, $tipoUsuarioId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tipoUsuarioId['max'])) {
                $this->addUsingAlias(RUsuarioTipousuarioPeer::TIPO_USUARIO_ID, $tipoUsuarioId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RUsuarioTipousuarioPeer::TIPO_USUARIO_ID, $tipoUsuarioId, $comparison);
    }

    /**
     * Filter the query by a related TipoUsuario object
     *
     * @param   TipoUsuario|PropelObjectCollection $tipoUsuario The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RUsuarioTipousuarioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByTipoUsuario($tipoUsuario, $comparison = null)
    {
        if ($tipoUsuario instanceof TipoUsuario) {
            return $this
                ->addUsingAlias(RUsuarioTipousuarioPeer::TIPO_USUARIO_ID, $tipoUsuario->getId(), $comparison);
        } elseif ($tipoUsuario instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RUsuarioTipousuarioPeer::TIPO_USUARIO_ID, $tipoUsuario->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByTipoUsuario() only accepts arguments of type TipoUsuario or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the TipoUsuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RUsuarioTipousuarioQuery The current query, for fluid interface
     */
    public function joinTipoUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('TipoUsuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'TipoUsuario');
        }

        return $this;
    }

    /**
     * Use the TipoUsuario relation TipoUsuario object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   TipoUsuarioQuery A secondary query class using the current class as primary query
     */
    public function useTipoUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinTipoUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'TipoUsuario', 'TipoUsuarioQuery');
    }

    /**
     * Filter the query by a related Usuario object
     *
     * @param   Usuario|PropelObjectCollection $usuario The related object(s) to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 RUsuarioTipousuarioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByUsuario($usuario, $comparison = null)
    {
        if ($usuario instanceof Usuario) {
            return $this
                ->addUsingAlias(RUsuarioTipousuarioPeer::USUARIO_ID, $usuario->getId(), $comparison);
        } elseif ($usuario instanceof PropelObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(RUsuarioTipousuarioPeer::USUARIO_ID, $usuario->toKeyValue('PrimaryKey', 'Id'), $comparison);
        } else {
            throw new PropelException('filterByUsuario() only accepts arguments of type Usuario or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Usuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return RUsuarioTipousuarioQuery The current query, for fluid interface
     */
    public function joinUsuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Usuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Usuario');
        }

        return $this;
    }

    /**
     * Use the Usuario relation Usuario object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   UsuarioQuery A secondary query class using the current class as primary query
     */
    public function useUsuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinUsuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Usuario', 'UsuarioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   RUsuarioTipousuario $rUsuarioTipousuario Object to remove from the list of results
     *
     * @return RUsuarioTipousuarioQuery The current query, for fluid interface
     */
    public function prune($rUsuarioTipousuario = null)
    {
        if ($rUsuarioTipousuario) {
            $this->addUsingAlias(RUsuarioTipousuarioPeer::ID, $rUsuarioTipousuario->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
