<?php


/**
 * Base class that represents a query for the 'reputacion' table.
 *
 *
 *
 * @method ReputacionQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ReputacionQuery orderByUsuarioId($order = Criteria::ASC) Order by the usuario_id column
 * @method ReputacionQuery orderByPuntos($order = Criteria::ASC) Order by the puntos column
 * @method ReputacionQuery orderByComentario($order = Criteria::ASC) Order by the comentario column
 *
 * @method ReputacionQuery groupById() Group by the id column
 * @method ReputacionQuery groupByUsuarioId() Group by the usuario_id column
 * @method ReputacionQuery groupByPuntos() Group by the puntos column
 * @method ReputacionQuery groupByComentario() Group by the comentario column
 *
 * @method ReputacionQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ReputacionQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ReputacionQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ReputacionQuery leftJoinRProveedorReputacion($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProveedorReputacion relation
 * @method ReputacionQuery rightJoinRProveedorReputacion($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProveedorReputacion relation
 * @method ReputacionQuery innerJoinRProveedorReputacion($relationAlias = null) Adds a INNER JOIN clause to the query using the RProveedorReputacion relation
 *
 * @method Reputacion findOne(PropelPDO $con = null) Return the first Reputacion matching the query
 * @method Reputacion findOneOrCreate(PropelPDO $con = null) Return the first Reputacion matching the query, or a new Reputacion object populated from the query conditions when no match is found
 *
 * @method Reputacion findOneByUsuarioId(int $usuario_id) Return the first Reputacion filtered by the usuario_id column
 * @method Reputacion findOneByPuntos(int $puntos) Return the first Reputacion filtered by the puntos column
 * @method Reputacion findOneByComentario(string $comentario) Return the first Reputacion filtered by the comentario column
 *
 * @method array findById(int $id) Return Reputacion objects filtered by the id column
 * @method array findByUsuarioId(int $usuario_id) Return Reputacion objects filtered by the usuario_id column
 * @method array findByPuntos(int $puntos) Return Reputacion objects filtered by the puntos column
 * @method array findByComentario(string $comentario) Return Reputacion objects filtered by the comentario column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseReputacionQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseReputacionQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'Reputacion';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ReputacionQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ReputacionQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ReputacionQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ReputacionQuery) {
            return $criteria;
        }
        $query = new ReputacionQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Reputacion|Reputacion[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ReputacionPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ReputacionPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Reputacion A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Reputacion A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `usuario_id`, `puntos`, `comentario` FROM `reputacion` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Reputacion();
            $obj->hydrate($row);
            ReputacionPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Reputacion|Reputacion[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Reputacion[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ReputacionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ReputacionPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ReputacionQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ReputacionPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ReputacionQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ReputacionPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ReputacionPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReputacionPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the usuario_id column
     *
     * Example usage:
     * <code>
     * $query->filterByUsuarioId(1234); // WHERE usuario_id = 1234
     * $query->filterByUsuarioId(array(12, 34)); // WHERE usuario_id IN (12, 34)
     * $query->filterByUsuarioId(array('min' => 12)); // WHERE usuario_id >= 12
     * $query->filterByUsuarioId(array('max' => 12)); // WHERE usuario_id <= 12
     * </code>
     *
     * @param     mixed $usuarioId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ReputacionQuery The current query, for fluid interface
     */
    public function filterByUsuarioId($usuarioId = null, $comparison = null)
    {
        if (is_array($usuarioId)) {
            $useMinMax = false;
            if (isset($usuarioId['min'])) {
                $this->addUsingAlias(ReputacionPeer::USUARIO_ID, $usuarioId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($usuarioId['max'])) {
                $this->addUsingAlias(ReputacionPeer::USUARIO_ID, $usuarioId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReputacionPeer::USUARIO_ID, $usuarioId, $comparison);
    }

    /**
     * Filter the query on the puntos column
     *
     * Example usage:
     * <code>
     * $query->filterByPuntos(1234); // WHERE puntos = 1234
     * $query->filterByPuntos(array(12, 34)); // WHERE puntos IN (12, 34)
     * $query->filterByPuntos(array('min' => 12)); // WHERE puntos >= 12
     * $query->filterByPuntos(array('max' => 12)); // WHERE puntos <= 12
     * </code>
     *
     * @param     mixed $puntos The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ReputacionQuery The current query, for fluid interface
     */
    public function filterByPuntos($puntos = null, $comparison = null)
    {
        if (is_array($puntos)) {
            $useMinMax = false;
            if (isset($puntos['min'])) {
                $this->addUsingAlias(ReputacionPeer::PUNTOS, $puntos['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($puntos['max'])) {
                $this->addUsingAlias(ReputacionPeer::PUNTOS, $puntos['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ReputacionPeer::PUNTOS, $puntos, $comparison);
    }

    /**
     * Filter the query on the comentario column
     *
     * Example usage:
     * <code>
     * $query->filterByComentario('fooValue');   // WHERE comentario = 'fooValue'
     * $query->filterByComentario('%fooValue%'); // WHERE comentario LIKE '%fooValue%'
     * </code>
     *
     * @param     string $comentario The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ReputacionQuery The current query, for fluid interface
     */
    public function filterByComentario($comentario = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($comentario)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $comentario)) {
                $comentario = str_replace('*', '%', $comentario);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ReputacionPeer::COMENTARIO, $comentario, $comparison);
    }

    /**
     * Filter the query by a related RProveedorReputacion object
     *
     * @param   RProveedorReputacion|PropelObjectCollection $rProveedorReputacion  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ReputacionQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProveedorReputacion($rProveedorReputacion, $comparison = null)
    {
        if ($rProveedorReputacion instanceof RProveedorReputacion) {
            return $this
                ->addUsingAlias(ReputacionPeer::ID, $rProveedorReputacion->getReputacionId(), $comparison);
        } elseif ($rProveedorReputacion instanceof PropelObjectCollection) {
            return $this
                ->useRProveedorReputacionQuery()
                ->filterByPrimaryKeys($rProveedorReputacion->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProveedorReputacion() only accepts arguments of type RProveedorReputacion or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProveedorReputacion relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ReputacionQuery The current query, for fluid interface
     */
    public function joinRProveedorReputacion($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProveedorReputacion');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProveedorReputacion');
        }

        return $this;
    }

    /**
     * Use the RProveedorReputacion relation RProveedorReputacion object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProveedorReputacionQuery A secondary query class using the current class as primary query
     */
    public function useRProveedorReputacionQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProveedorReputacion($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProveedorReputacion', 'RProveedorReputacionQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Reputacion $reputacion Object to remove from the list of results
     *
     * @return ReputacionQuery The current query, for fluid interface
     */
    public function prune($reputacion = null)
    {
        if ($reputacion) {
            $this->addUsingAlias(ReputacionPeer::ID, $reputacion->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
