<?php


/**
 * Base class that represents a row from the 'usuario' table.
 *
 *
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseUsuario extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'UsuarioPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        UsuarioPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinite loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the id field.
     * @var        int
     */
    protected $id;

    /**
     * The value for the username field.
     * @var        string
     */
    protected $username;

    /**
     * The value for the password field.
     * @var        string
     */
    protected $password;

    /**
     * @var        PropelObjectCollection|RUsuarioComercio[] Collection to store aggregation of RUsuarioComercio objects.
     */
    protected $collRUsuarioComercios;
    protected $collRUsuarioComerciosPartial;

    /**
     * @var        PropelObjectCollection|RUsuarioProducto[] Collection to store aggregation of RUsuarioProducto objects.
     */
    protected $collRUsuarioProductos;
    protected $collRUsuarioProductosPartial;

    /**
     * @var        PropelObjectCollection|RUsuarioProveedor[] Collection to store aggregation of RUsuarioProveedor objects.
     */
    protected $collRUsuarioProveedors;
    protected $collRUsuarioProveedorsPartial;

    /**
     * @var        PropelObjectCollection|RUsuarioTipousuario[] Collection to store aggregation of RUsuarioTipousuario objects.
     */
    protected $collRUsuarioTipousuarios;
    protected $collRUsuarioTipousuariosPartial;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rUsuarioComerciosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rUsuarioProductosScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rUsuarioProveedorsScheduledForDeletion = null;

    /**
     * An array of objects scheduled for deletion.
     * @var		PropelObjectCollection
     */
    protected $rUsuarioTipousuariosScheduledForDeletion = null;

    /**
     * Get the [id] column value.
     *
     * @return int
     */
    public function getId()
    {

        return $this->id;
    }

    /**
     * Get the [username] column value.
     *
     * @return string
     */
    public function getUsername()
    {

        return $this->username;
    }

    /**
     * Get the [password] column value.
     *
     * @return string
     */
    public function getPassword()
    {

        return $this->password;
    }

    /**
     * Set the value of [id] column.
     *
     * @param  int $v new value
     * @return Usuario The current object (for fluent API support)
     */
    public function setId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->id !== $v) {
            $this->id = $v;
            $this->modifiedColumns[] = UsuarioPeer::ID;
        }


        return $this;
    } // setId()

    /**
     * Set the value of [username] column.
     *
     * @param  string $v new value
     * @return Usuario The current object (for fluent API support)
     */
    public function setUsername($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->username !== $v) {
            $this->username = $v;
            $this->modifiedColumns[] = UsuarioPeer::USERNAME;
        }


        return $this;
    } // setUsername()

    /**
     * Set the value of [password] column.
     *
     * @param  string $v new value
     * @return Usuario The current object (for fluent API support)
     */
    public function setPassword($v)
    {
        if ($v !== null) {
            $v = (string) $v;
        }

        if ($this->password !== $v) {
            $this->password = $v;
            $this->modifiedColumns[] = UsuarioPeer::PASSWORD;
        }


        return $this;
    } // setPassword()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which resultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->id = ($row[$startcol + 0] !== null) ? (int) $row[$startcol + 0] : null;
            $this->username = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->password = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);

            return $startcol + 3; // 3 = UsuarioPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Usuario object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(UsuarioPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = UsuarioPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

            $this->collRUsuarioComercios = null;

            $this->collRUsuarioProductos = null;

            $this->collRUsuarioProveedors = null;

            $this->collRUsuarioTipousuarios = null;

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(UsuarioPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = UsuarioQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(UsuarioPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                UsuarioPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            if ($this->rUsuarioComerciosScheduledForDeletion !== null) {
                if (!$this->rUsuarioComerciosScheduledForDeletion->isEmpty()) {
                    RUsuarioComercioQuery::create()
                        ->filterByPrimaryKeys($this->rUsuarioComerciosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rUsuarioComerciosScheduledForDeletion = null;
                }
            }

            if ($this->collRUsuarioComercios !== null) {
                foreach ($this->collRUsuarioComercios as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rUsuarioProductosScheduledForDeletion !== null) {
                if (!$this->rUsuarioProductosScheduledForDeletion->isEmpty()) {
                    RUsuarioProductoQuery::create()
                        ->filterByPrimaryKeys($this->rUsuarioProductosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rUsuarioProductosScheduledForDeletion = null;
                }
            }

            if ($this->collRUsuarioProductos !== null) {
                foreach ($this->collRUsuarioProductos as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rUsuarioProveedorsScheduledForDeletion !== null) {
                if (!$this->rUsuarioProveedorsScheduledForDeletion->isEmpty()) {
                    RUsuarioProveedorQuery::create()
                        ->filterByPrimaryKeys($this->rUsuarioProveedorsScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rUsuarioProveedorsScheduledForDeletion = null;
                }
            }

            if ($this->collRUsuarioProveedors !== null) {
                foreach ($this->collRUsuarioProveedors as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            if ($this->rUsuarioTipousuariosScheduledForDeletion !== null) {
                if (!$this->rUsuarioTipousuariosScheduledForDeletion->isEmpty()) {
                    RUsuarioTipousuarioQuery::create()
                        ->filterByPrimaryKeys($this->rUsuarioTipousuariosScheduledForDeletion->getPrimaryKeys(false))
                        ->delete($con);
                    $this->rUsuarioTipousuariosScheduledForDeletion = null;
                }
            }

            if ($this->collRUsuarioTipousuarios !== null) {
                foreach ($this->collRUsuarioTipousuarios as $referrerFK) {
                    if (!$referrerFK->isDeleted() && ($referrerFK->isNew() || $referrerFK->isModified())) {
                        $affectedRows += $referrerFK->save($con);
                    }
                }
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $modifiedColumns = array();
        $index = 0;

        $this->modifiedColumns[] = UsuarioPeer::ID;
        if (null !== $this->id) {
            throw new PropelException('Cannot insert a value for auto-increment primary key (' . UsuarioPeer::ID . ')');
        }

         // check the columns in natural order for more readable SQL queries
        if ($this->isColumnModified(UsuarioPeer::ID)) {
            $modifiedColumns[':p' . $index++]  = '`id`';
        }
        if ($this->isColumnModified(UsuarioPeer::USERNAME)) {
            $modifiedColumns[':p' . $index++]  = '`username`';
        }
        if ($this->isColumnModified(UsuarioPeer::PASSWORD)) {
            $modifiedColumns[':p' . $index++]  = '`password`';
        }

        $sql = sprintf(
            'INSERT INTO `usuario` (%s) VALUES (%s)',
            implode(', ', $modifiedColumns),
            implode(', ', array_keys($modifiedColumns))
        );

        try {
            $stmt = $con->prepare($sql);
            foreach ($modifiedColumns as $identifier => $columnName) {
                switch ($columnName) {
                    case '`id`':
                        $stmt->bindValue($identifier, $this->id, PDO::PARAM_INT);
                        break;
                    case '`username`':
                        $stmt->bindValue($identifier, $this->username, PDO::PARAM_STR);
                        break;
                    case '`password`':
                        $stmt->bindValue($identifier, $this->password, PDO::PARAM_STR);
                        break;
                }
            }
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute INSERT statement [%s]', $sql), $e);
        }

        try {
            $pk = $con->lastInsertId();
        } catch (Exception $e) {
            throw new PropelException('Unable to get autoincrement id.', $e);
        }
        $this->setId($pk);

        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggregated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objects otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = UsuarioPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }


                if ($this->collRUsuarioComercios !== null) {
                    foreach ($this->collRUsuarioComercios as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRUsuarioProductos !== null) {
                    foreach ($this->collRUsuarioProductos as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRUsuarioProveedors !== null) {
                    foreach ($this->collRUsuarioProveedors as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }

                if ($this->collRUsuarioTipousuarios !== null) {
                    foreach ($this->collRUsuarioTipousuarios as $referrerFK) {
                        if (!$referrerFK->validate($columns)) {
                            $failureMap = array_merge($failureMap, $referrerFK->getValidationFailures());
                        }
                    }
                }


            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = UsuarioPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getId();
                break;
            case 1:
                return $this->getUsername();
                break;
            case 2:
                return $this->getPassword();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     * @param     boolean $includeForeignObjects (optional) Whether to include hydrated related objects. Default to FALSE.
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array(), $includeForeignObjects = false)
    {
        if (isset($alreadyDumpedObjects['Usuario'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Usuario'][$this->getPrimaryKey()] = true;
        $keys = UsuarioPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getId(),
            $keys[1] => $this->getUsername(),
            $keys[2] => $this->getPassword(),
        );
        $virtualColumns = $this->virtualColumns;
        foreach ($virtualColumns as $key => $virtualColumn) {
            $result[$key] = $virtualColumn;
        }

        if ($includeForeignObjects) {
            if (null !== $this->collRUsuarioComercios) {
                $result['RUsuarioComercios'] = $this->collRUsuarioComercios->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRUsuarioProductos) {
                $result['RUsuarioProductos'] = $this->collRUsuarioProductos->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRUsuarioProveedors) {
                $result['RUsuarioProveedors'] = $this->collRUsuarioProveedors->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
            if (null !== $this->collRUsuarioTipousuarios) {
                $result['RUsuarioTipousuarios'] = $this->collRUsuarioTipousuarios->toArray(null, true, $keyType, $includeLazyLoadColumns, $alreadyDumpedObjects);
            }
        }

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = UsuarioPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setId($value);
                break;
            case 1:
                $this->setUsername($value);
                break;
            case 2:
                $this->setPassword($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = UsuarioPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setUsername($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setPassword($arr[$keys[2]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(UsuarioPeer::DATABASE_NAME);

        if ($this->isColumnModified(UsuarioPeer::ID)) $criteria->add(UsuarioPeer::ID, $this->id);
        if ($this->isColumnModified(UsuarioPeer::USERNAME)) $criteria->add(UsuarioPeer::USERNAME, $this->username);
        if ($this->isColumnModified(UsuarioPeer::PASSWORD)) $criteria->add(UsuarioPeer::PASSWORD, $this->password);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(UsuarioPeer::DATABASE_NAME);
        $criteria->add(UsuarioPeer::ID, $this->id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return int
     */
    public function getPrimaryKey()
    {
        return $this->getId();
    }

    /**
     * Generic method to set the primary key (id column).
     *
     * @param  int $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Usuario (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setUsername($this->getUsername());
        $copyObj->setPassword($this->getPassword());

        if ($deepCopy && !$this->startCopy) {
            // important: temporarily setNew(false) because this affects the behavior of
            // the getter/setter methods for fkey referrer objects.
            $copyObj->setNew(false);
            // store object hash to prevent cycle
            $this->startCopy = true;

            foreach ($this->getRUsuarioComercios() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRUsuarioComercio($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRUsuarioProductos() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRUsuarioProducto($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRUsuarioProveedors() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRUsuarioProveedor($relObj->copy($deepCopy));
                }
            }

            foreach ($this->getRUsuarioTipousuarios() as $relObj) {
                if ($relObj !== $this) {  // ensure that we don't try to copy a reference to ourselves
                    $copyObj->addRUsuarioTipousuario($relObj->copy($deepCopy));
                }
            }

            //unflag object copy
            $this->startCopy = false;
        } // if ($deepCopy)

        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Usuario Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return UsuarioPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new UsuarioPeer();
        }

        return self::$peer;
    }


    /**
     * Initializes a collection based on the name of a relation.
     * Avoids crafting an 'init[$relationName]s' method name
     * that wouldn't work when StandardEnglishPluralizer is used.
     *
     * @param string $relationName The name of the relation to initialize
     * @return void
     */
    public function initRelation($relationName)
    {
        if ('RUsuarioComercio' == $relationName) {
            $this->initRUsuarioComercios();
        }
        if ('RUsuarioProducto' == $relationName) {
            $this->initRUsuarioProductos();
        }
        if ('RUsuarioProveedor' == $relationName) {
            $this->initRUsuarioProveedors();
        }
        if ('RUsuarioTipousuario' == $relationName) {
            $this->initRUsuarioTipousuarios();
        }
    }

    /**
     * Clears out the collRUsuarioComercios collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Usuario The current object (for fluent API support)
     * @see        addRUsuarioComercios()
     */
    public function clearRUsuarioComercios()
    {
        $this->collRUsuarioComercios = null; // important to set this to null since that means it is uninitialized
        $this->collRUsuarioComerciosPartial = null;

        return $this;
    }

    /**
     * reset is the collRUsuarioComercios collection loaded partially
     *
     * @return void
     */
    public function resetPartialRUsuarioComercios($v = true)
    {
        $this->collRUsuarioComerciosPartial = $v;
    }

    /**
     * Initializes the collRUsuarioComercios collection.
     *
     * By default this just sets the collRUsuarioComercios collection to an empty array (like clearcollRUsuarioComercios());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRUsuarioComercios($overrideExisting = true)
    {
        if (null !== $this->collRUsuarioComercios && !$overrideExisting) {
            return;
        }
        $this->collRUsuarioComercios = new PropelObjectCollection();
        $this->collRUsuarioComercios->setModel('RUsuarioComercio');
    }

    /**
     * Gets an array of RUsuarioComercio objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Usuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RUsuarioComercio[] List of RUsuarioComercio objects
     * @throws PropelException
     */
    public function getRUsuarioComercios($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioComerciosPartial && !$this->isNew();
        if (null === $this->collRUsuarioComercios || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioComercios) {
                // return empty collection
                $this->initRUsuarioComercios();
            } else {
                $collRUsuarioComercios = RUsuarioComercioQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRUsuarioComerciosPartial && count($collRUsuarioComercios)) {
                      $this->initRUsuarioComercios(false);

                      foreach ($collRUsuarioComercios as $obj) {
                        if (false == $this->collRUsuarioComercios->contains($obj)) {
                          $this->collRUsuarioComercios->append($obj);
                        }
                      }

                      $this->collRUsuarioComerciosPartial = true;
                    }

                    $collRUsuarioComercios->getInternalIterator()->rewind();

                    return $collRUsuarioComercios;
                }

                if ($partial && $this->collRUsuarioComercios) {
                    foreach ($this->collRUsuarioComercios as $obj) {
                        if ($obj->isNew()) {
                            $collRUsuarioComercios[] = $obj;
                        }
                    }
                }

                $this->collRUsuarioComercios = $collRUsuarioComercios;
                $this->collRUsuarioComerciosPartial = false;
            }
        }

        return $this->collRUsuarioComercios;
    }

    /**
     * Sets a collection of RUsuarioComercio objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rUsuarioComercios A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Usuario The current object (for fluent API support)
     */
    public function setRUsuarioComercios(PropelCollection $rUsuarioComercios, PropelPDO $con = null)
    {
        $rUsuarioComerciosToDelete = $this->getRUsuarioComercios(new Criteria(), $con)->diff($rUsuarioComercios);


        $this->rUsuarioComerciosScheduledForDeletion = $rUsuarioComerciosToDelete;

        foreach ($rUsuarioComerciosToDelete as $rUsuarioComercioRemoved) {
            $rUsuarioComercioRemoved->setUsuario(null);
        }

        $this->collRUsuarioComercios = null;
        foreach ($rUsuarioComercios as $rUsuarioComercio) {
            $this->addRUsuarioComercio($rUsuarioComercio);
        }

        $this->collRUsuarioComercios = $rUsuarioComercios;
        $this->collRUsuarioComerciosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RUsuarioComercio objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RUsuarioComercio objects.
     * @throws PropelException
     */
    public function countRUsuarioComercios(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioComerciosPartial && !$this->isNew();
        if (null === $this->collRUsuarioComercios || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioComercios) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRUsuarioComercios());
            }
            $query = RUsuarioComercioQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collRUsuarioComercios);
    }

    /**
     * Method called to associate a RUsuarioComercio object to this object
     * through the RUsuarioComercio foreign key attribute.
     *
     * @param    RUsuarioComercio $l RUsuarioComercio
     * @return Usuario The current object (for fluent API support)
     */
    public function addRUsuarioComercio(RUsuarioComercio $l)
    {
        if ($this->collRUsuarioComercios === null) {
            $this->initRUsuarioComercios();
            $this->collRUsuarioComerciosPartial = true;
        }

        if (!in_array($l, $this->collRUsuarioComercios->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRUsuarioComercio($l);

            if ($this->rUsuarioComerciosScheduledForDeletion and $this->rUsuarioComerciosScheduledForDeletion->contains($l)) {
                $this->rUsuarioComerciosScheduledForDeletion->remove($this->rUsuarioComerciosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RUsuarioComercio $rUsuarioComercio The rUsuarioComercio object to add.
     */
    protected function doAddRUsuarioComercio($rUsuarioComercio)
    {
        $this->collRUsuarioComercios[]= $rUsuarioComercio;
        $rUsuarioComercio->setUsuario($this);
    }

    /**
     * @param	RUsuarioComercio $rUsuarioComercio The rUsuarioComercio object to remove.
     * @return Usuario The current object (for fluent API support)
     */
    public function removeRUsuarioComercio($rUsuarioComercio)
    {
        if ($this->getRUsuarioComercios()->contains($rUsuarioComercio)) {
            $this->collRUsuarioComercios->remove($this->collRUsuarioComercios->search($rUsuarioComercio));
            if (null === $this->rUsuarioComerciosScheduledForDeletion) {
                $this->rUsuarioComerciosScheduledForDeletion = clone $this->collRUsuarioComercios;
                $this->rUsuarioComerciosScheduledForDeletion->clear();
            }
            $this->rUsuarioComerciosScheduledForDeletion[]= clone $rUsuarioComercio;
            $rUsuarioComercio->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related RUsuarioComercios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RUsuarioComercio[] List of RUsuarioComercio objects
     */
    public function getRUsuarioComerciosJoinComercio($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RUsuarioComercioQuery::create(null, $criteria);
        $query->joinWith('Comercio', $join_behavior);

        return $this->getRUsuarioComercios($query, $con);
    }

    /**
     * Clears out the collRUsuarioProductos collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Usuario The current object (for fluent API support)
     * @see        addRUsuarioProductos()
     */
    public function clearRUsuarioProductos()
    {
        $this->collRUsuarioProductos = null; // important to set this to null since that means it is uninitialized
        $this->collRUsuarioProductosPartial = null;

        return $this;
    }

    /**
     * reset is the collRUsuarioProductos collection loaded partially
     *
     * @return void
     */
    public function resetPartialRUsuarioProductos($v = true)
    {
        $this->collRUsuarioProductosPartial = $v;
    }

    /**
     * Initializes the collRUsuarioProductos collection.
     *
     * By default this just sets the collRUsuarioProductos collection to an empty array (like clearcollRUsuarioProductos());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRUsuarioProductos($overrideExisting = true)
    {
        if (null !== $this->collRUsuarioProductos && !$overrideExisting) {
            return;
        }
        $this->collRUsuarioProductos = new PropelObjectCollection();
        $this->collRUsuarioProductos->setModel('RUsuarioProducto');
    }

    /**
     * Gets an array of RUsuarioProducto objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Usuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RUsuarioProducto[] List of RUsuarioProducto objects
     * @throws PropelException
     */
    public function getRUsuarioProductos($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioProductosPartial && !$this->isNew();
        if (null === $this->collRUsuarioProductos || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioProductos) {
                // return empty collection
                $this->initRUsuarioProductos();
            } else {
                $collRUsuarioProductos = RUsuarioProductoQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRUsuarioProductosPartial && count($collRUsuarioProductos)) {
                      $this->initRUsuarioProductos(false);

                      foreach ($collRUsuarioProductos as $obj) {
                        if (false == $this->collRUsuarioProductos->contains($obj)) {
                          $this->collRUsuarioProductos->append($obj);
                        }
                      }

                      $this->collRUsuarioProductosPartial = true;
                    }

                    $collRUsuarioProductos->getInternalIterator()->rewind();

                    return $collRUsuarioProductos;
                }

                if ($partial && $this->collRUsuarioProductos) {
                    foreach ($this->collRUsuarioProductos as $obj) {
                        if ($obj->isNew()) {
                            $collRUsuarioProductos[] = $obj;
                        }
                    }
                }

                $this->collRUsuarioProductos = $collRUsuarioProductos;
                $this->collRUsuarioProductosPartial = false;
            }
        }

        return $this->collRUsuarioProductos;
    }

    /**
     * Sets a collection of RUsuarioProducto objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rUsuarioProductos A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Usuario The current object (for fluent API support)
     */
    public function setRUsuarioProductos(PropelCollection $rUsuarioProductos, PropelPDO $con = null)
    {
        $rUsuarioProductosToDelete = $this->getRUsuarioProductos(new Criteria(), $con)->diff($rUsuarioProductos);


        $this->rUsuarioProductosScheduledForDeletion = $rUsuarioProductosToDelete;

        foreach ($rUsuarioProductosToDelete as $rUsuarioProductoRemoved) {
            $rUsuarioProductoRemoved->setUsuario(null);
        }

        $this->collRUsuarioProductos = null;
        foreach ($rUsuarioProductos as $rUsuarioProducto) {
            $this->addRUsuarioProducto($rUsuarioProducto);
        }

        $this->collRUsuarioProductos = $rUsuarioProductos;
        $this->collRUsuarioProductosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RUsuarioProducto objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RUsuarioProducto objects.
     * @throws PropelException
     */
    public function countRUsuarioProductos(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioProductosPartial && !$this->isNew();
        if (null === $this->collRUsuarioProductos || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioProductos) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRUsuarioProductos());
            }
            $query = RUsuarioProductoQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collRUsuarioProductos);
    }

    /**
     * Method called to associate a RUsuarioProducto object to this object
     * through the RUsuarioProducto foreign key attribute.
     *
     * @param    RUsuarioProducto $l RUsuarioProducto
     * @return Usuario The current object (for fluent API support)
     */
    public function addRUsuarioProducto(RUsuarioProducto $l)
    {
        if ($this->collRUsuarioProductos === null) {
            $this->initRUsuarioProductos();
            $this->collRUsuarioProductosPartial = true;
        }

        if (!in_array($l, $this->collRUsuarioProductos->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRUsuarioProducto($l);

            if ($this->rUsuarioProductosScheduledForDeletion and $this->rUsuarioProductosScheduledForDeletion->contains($l)) {
                $this->rUsuarioProductosScheduledForDeletion->remove($this->rUsuarioProductosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RUsuarioProducto $rUsuarioProducto The rUsuarioProducto object to add.
     */
    protected function doAddRUsuarioProducto($rUsuarioProducto)
    {
        $this->collRUsuarioProductos[]= $rUsuarioProducto;
        $rUsuarioProducto->setUsuario($this);
    }

    /**
     * @param	RUsuarioProducto $rUsuarioProducto The rUsuarioProducto object to remove.
     * @return Usuario The current object (for fluent API support)
     */
    public function removeRUsuarioProducto($rUsuarioProducto)
    {
        if ($this->getRUsuarioProductos()->contains($rUsuarioProducto)) {
            $this->collRUsuarioProductos->remove($this->collRUsuarioProductos->search($rUsuarioProducto));
            if (null === $this->rUsuarioProductosScheduledForDeletion) {
                $this->rUsuarioProductosScheduledForDeletion = clone $this->collRUsuarioProductos;
                $this->rUsuarioProductosScheduledForDeletion->clear();
            }
            $this->rUsuarioProductosScheduledForDeletion[]= clone $rUsuarioProducto;
            $rUsuarioProducto->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related RUsuarioProductos from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RUsuarioProducto[] List of RUsuarioProducto objects
     */
    public function getRUsuarioProductosJoinProducto($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RUsuarioProductoQuery::create(null, $criteria);
        $query->joinWith('Producto', $join_behavior);

        return $this->getRUsuarioProductos($query, $con);
    }

    /**
     * Clears out the collRUsuarioProveedors collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Usuario The current object (for fluent API support)
     * @see        addRUsuarioProveedors()
     */
    public function clearRUsuarioProveedors()
    {
        $this->collRUsuarioProveedors = null; // important to set this to null since that means it is uninitialized
        $this->collRUsuarioProveedorsPartial = null;

        return $this;
    }

    /**
     * reset is the collRUsuarioProveedors collection loaded partially
     *
     * @return void
     */
    public function resetPartialRUsuarioProveedors($v = true)
    {
        $this->collRUsuarioProveedorsPartial = $v;
    }

    /**
     * Initializes the collRUsuarioProveedors collection.
     *
     * By default this just sets the collRUsuarioProveedors collection to an empty array (like clearcollRUsuarioProveedors());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRUsuarioProveedors($overrideExisting = true)
    {
        if (null !== $this->collRUsuarioProveedors && !$overrideExisting) {
            return;
        }
        $this->collRUsuarioProveedors = new PropelObjectCollection();
        $this->collRUsuarioProveedors->setModel('RUsuarioProveedor');
    }

    /**
     * Gets an array of RUsuarioProveedor objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Usuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RUsuarioProveedor[] List of RUsuarioProveedor objects
     * @throws PropelException
     */
    public function getRUsuarioProveedors($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioProveedorsPartial && !$this->isNew();
        if (null === $this->collRUsuarioProveedors || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioProveedors) {
                // return empty collection
                $this->initRUsuarioProveedors();
            } else {
                $collRUsuarioProveedors = RUsuarioProveedorQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRUsuarioProveedorsPartial && count($collRUsuarioProveedors)) {
                      $this->initRUsuarioProveedors(false);

                      foreach ($collRUsuarioProveedors as $obj) {
                        if (false == $this->collRUsuarioProveedors->contains($obj)) {
                          $this->collRUsuarioProveedors->append($obj);
                        }
                      }

                      $this->collRUsuarioProveedorsPartial = true;
                    }

                    $collRUsuarioProveedors->getInternalIterator()->rewind();

                    return $collRUsuarioProveedors;
                }

                if ($partial && $this->collRUsuarioProveedors) {
                    foreach ($this->collRUsuarioProveedors as $obj) {
                        if ($obj->isNew()) {
                            $collRUsuarioProveedors[] = $obj;
                        }
                    }
                }

                $this->collRUsuarioProveedors = $collRUsuarioProveedors;
                $this->collRUsuarioProveedorsPartial = false;
            }
        }

        return $this->collRUsuarioProveedors;
    }

    /**
     * Sets a collection of RUsuarioProveedor objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rUsuarioProveedors A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Usuario The current object (for fluent API support)
     */
    public function setRUsuarioProveedors(PropelCollection $rUsuarioProveedors, PropelPDO $con = null)
    {
        $rUsuarioProveedorsToDelete = $this->getRUsuarioProveedors(new Criteria(), $con)->diff($rUsuarioProveedors);


        $this->rUsuarioProveedorsScheduledForDeletion = $rUsuarioProveedorsToDelete;

        foreach ($rUsuarioProveedorsToDelete as $rUsuarioProveedorRemoved) {
            $rUsuarioProveedorRemoved->setUsuario(null);
        }

        $this->collRUsuarioProveedors = null;
        foreach ($rUsuarioProveedors as $rUsuarioProveedor) {
            $this->addRUsuarioProveedor($rUsuarioProveedor);
        }

        $this->collRUsuarioProveedors = $rUsuarioProveedors;
        $this->collRUsuarioProveedorsPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RUsuarioProveedor objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RUsuarioProveedor objects.
     * @throws PropelException
     */
    public function countRUsuarioProveedors(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioProveedorsPartial && !$this->isNew();
        if (null === $this->collRUsuarioProveedors || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioProveedors) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRUsuarioProveedors());
            }
            $query = RUsuarioProveedorQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collRUsuarioProveedors);
    }

    /**
     * Method called to associate a RUsuarioProveedor object to this object
     * through the RUsuarioProveedor foreign key attribute.
     *
     * @param    RUsuarioProveedor $l RUsuarioProveedor
     * @return Usuario The current object (for fluent API support)
     */
    public function addRUsuarioProveedor(RUsuarioProveedor $l)
    {
        if ($this->collRUsuarioProveedors === null) {
            $this->initRUsuarioProveedors();
            $this->collRUsuarioProveedorsPartial = true;
        }

        if (!in_array($l, $this->collRUsuarioProveedors->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRUsuarioProveedor($l);

            if ($this->rUsuarioProveedorsScheduledForDeletion and $this->rUsuarioProveedorsScheduledForDeletion->contains($l)) {
                $this->rUsuarioProveedorsScheduledForDeletion->remove($this->rUsuarioProveedorsScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RUsuarioProveedor $rUsuarioProveedor The rUsuarioProveedor object to add.
     */
    protected function doAddRUsuarioProveedor($rUsuarioProveedor)
    {
        $this->collRUsuarioProveedors[]= $rUsuarioProveedor;
        $rUsuarioProveedor->setUsuario($this);
    }

    /**
     * @param	RUsuarioProveedor $rUsuarioProveedor The rUsuarioProveedor object to remove.
     * @return Usuario The current object (for fluent API support)
     */
    public function removeRUsuarioProveedor($rUsuarioProveedor)
    {
        if ($this->getRUsuarioProveedors()->contains($rUsuarioProveedor)) {
            $this->collRUsuarioProveedors->remove($this->collRUsuarioProveedors->search($rUsuarioProveedor));
            if (null === $this->rUsuarioProveedorsScheduledForDeletion) {
                $this->rUsuarioProveedorsScheduledForDeletion = clone $this->collRUsuarioProveedors;
                $this->rUsuarioProveedorsScheduledForDeletion->clear();
            }
            $this->rUsuarioProveedorsScheduledForDeletion[]= clone $rUsuarioProveedor;
            $rUsuarioProveedor->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related RUsuarioProveedors from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RUsuarioProveedor[] List of RUsuarioProveedor objects
     */
    public function getRUsuarioProveedorsJoinProveedor($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RUsuarioProveedorQuery::create(null, $criteria);
        $query->joinWith('Proveedor', $join_behavior);

        return $this->getRUsuarioProveedors($query, $con);
    }

    /**
     * Clears out the collRUsuarioTipousuarios collection
     *
     * This does not modify the database; however, it will remove any associated objects, causing
     * them to be refetched by subsequent calls to accessor method.
     *
     * @return Usuario The current object (for fluent API support)
     * @see        addRUsuarioTipousuarios()
     */
    public function clearRUsuarioTipousuarios()
    {
        $this->collRUsuarioTipousuarios = null; // important to set this to null since that means it is uninitialized
        $this->collRUsuarioTipousuariosPartial = null;

        return $this;
    }

    /**
     * reset is the collRUsuarioTipousuarios collection loaded partially
     *
     * @return void
     */
    public function resetPartialRUsuarioTipousuarios($v = true)
    {
        $this->collRUsuarioTipousuariosPartial = $v;
    }

    /**
     * Initializes the collRUsuarioTipousuarios collection.
     *
     * By default this just sets the collRUsuarioTipousuarios collection to an empty array (like clearcollRUsuarioTipousuarios());
     * however, you may wish to override this method in your stub class to provide setting appropriate
     * to your application -- for example, setting the initial array to the values stored in database.
     *
     * @param boolean $overrideExisting If set to true, the method call initializes
     *                                        the collection even if it is not empty
     *
     * @return void
     */
    public function initRUsuarioTipousuarios($overrideExisting = true)
    {
        if (null !== $this->collRUsuarioTipousuarios && !$overrideExisting) {
            return;
        }
        $this->collRUsuarioTipousuarios = new PropelObjectCollection();
        $this->collRUsuarioTipousuarios->setModel('RUsuarioTipousuario');
    }

    /**
     * Gets an array of RUsuarioTipousuario objects which contain a foreign key that references this object.
     *
     * If the $criteria is not null, it is used to always fetch the results from the database.
     * Otherwise the results are fetched from the database the first time, then cached.
     * Next time the same method is called without $criteria, the cached collection is returned.
     * If this Usuario is new, it will return
     * an empty collection or the current collection; the criteria is ignored on a new object.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @return PropelObjectCollection|RUsuarioTipousuario[] List of RUsuarioTipousuario objects
     * @throws PropelException
     */
    public function getRUsuarioTipousuarios($criteria = null, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioTipousuariosPartial && !$this->isNew();
        if (null === $this->collRUsuarioTipousuarios || null !== $criteria  || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioTipousuarios) {
                // return empty collection
                $this->initRUsuarioTipousuarios();
            } else {
                $collRUsuarioTipousuarios = RUsuarioTipousuarioQuery::create(null, $criteria)
                    ->filterByUsuario($this)
                    ->find($con);
                if (null !== $criteria) {
                    if (false !== $this->collRUsuarioTipousuariosPartial && count($collRUsuarioTipousuarios)) {
                      $this->initRUsuarioTipousuarios(false);

                      foreach ($collRUsuarioTipousuarios as $obj) {
                        if (false == $this->collRUsuarioTipousuarios->contains($obj)) {
                          $this->collRUsuarioTipousuarios->append($obj);
                        }
                      }

                      $this->collRUsuarioTipousuariosPartial = true;
                    }

                    $collRUsuarioTipousuarios->getInternalIterator()->rewind();

                    return $collRUsuarioTipousuarios;
                }

                if ($partial && $this->collRUsuarioTipousuarios) {
                    foreach ($this->collRUsuarioTipousuarios as $obj) {
                        if ($obj->isNew()) {
                            $collRUsuarioTipousuarios[] = $obj;
                        }
                    }
                }

                $this->collRUsuarioTipousuarios = $collRUsuarioTipousuarios;
                $this->collRUsuarioTipousuariosPartial = false;
            }
        }

        return $this->collRUsuarioTipousuarios;
    }

    /**
     * Sets a collection of RUsuarioTipousuario objects related by a one-to-many relationship
     * to the current object.
     * It will also schedule objects for deletion based on a diff between old objects (aka persisted)
     * and new objects from the given Propel collection.
     *
     * @param PropelCollection $rUsuarioTipousuarios A Propel collection.
     * @param PropelPDO $con Optional connection object
     * @return Usuario The current object (for fluent API support)
     */
    public function setRUsuarioTipousuarios(PropelCollection $rUsuarioTipousuarios, PropelPDO $con = null)
    {
        $rUsuarioTipousuariosToDelete = $this->getRUsuarioTipousuarios(new Criteria(), $con)->diff($rUsuarioTipousuarios);


        $this->rUsuarioTipousuariosScheduledForDeletion = $rUsuarioTipousuariosToDelete;

        foreach ($rUsuarioTipousuariosToDelete as $rUsuarioTipousuarioRemoved) {
            $rUsuarioTipousuarioRemoved->setUsuario(null);
        }

        $this->collRUsuarioTipousuarios = null;
        foreach ($rUsuarioTipousuarios as $rUsuarioTipousuario) {
            $this->addRUsuarioTipousuario($rUsuarioTipousuario);
        }

        $this->collRUsuarioTipousuarios = $rUsuarioTipousuarios;
        $this->collRUsuarioTipousuariosPartial = false;

        return $this;
    }

    /**
     * Returns the number of related RUsuarioTipousuario objects.
     *
     * @param Criteria $criteria
     * @param boolean $distinct
     * @param PropelPDO $con
     * @return int             Count of related RUsuarioTipousuario objects.
     * @throws PropelException
     */
    public function countRUsuarioTipousuarios(Criteria $criteria = null, $distinct = false, PropelPDO $con = null)
    {
        $partial = $this->collRUsuarioTipousuariosPartial && !$this->isNew();
        if (null === $this->collRUsuarioTipousuarios || null !== $criteria || $partial) {
            if ($this->isNew() && null === $this->collRUsuarioTipousuarios) {
                return 0;
            }

            if ($partial && !$criteria) {
                return count($this->getRUsuarioTipousuarios());
            }
            $query = RUsuarioTipousuarioQuery::create(null, $criteria);
            if ($distinct) {
                $query->distinct();
            }

            return $query
                ->filterByUsuario($this)
                ->count($con);
        }

        return count($this->collRUsuarioTipousuarios);
    }

    /**
     * Method called to associate a RUsuarioTipousuario object to this object
     * through the RUsuarioTipousuario foreign key attribute.
     *
     * @param    RUsuarioTipousuario $l RUsuarioTipousuario
     * @return Usuario The current object (for fluent API support)
     */
    public function addRUsuarioTipousuario(RUsuarioTipousuario $l)
    {
        if ($this->collRUsuarioTipousuarios === null) {
            $this->initRUsuarioTipousuarios();
            $this->collRUsuarioTipousuariosPartial = true;
        }

        if (!in_array($l, $this->collRUsuarioTipousuarios->getArrayCopy(), true)) { // only add it if the **same** object is not already associated
            $this->doAddRUsuarioTipousuario($l);

            if ($this->rUsuarioTipousuariosScheduledForDeletion and $this->rUsuarioTipousuariosScheduledForDeletion->contains($l)) {
                $this->rUsuarioTipousuariosScheduledForDeletion->remove($this->rUsuarioTipousuariosScheduledForDeletion->search($l));
            }
        }

        return $this;
    }

    /**
     * @param	RUsuarioTipousuario $rUsuarioTipousuario The rUsuarioTipousuario object to add.
     */
    protected function doAddRUsuarioTipousuario($rUsuarioTipousuario)
    {
        $this->collRUsuarioTipousuarios[]= $rUsuarioTipousuario;
        $rUsuarioTipousuario->setUsuario($this);
    }

    /**
     * @param	RUsuarioTipousuario $rUsuarioTipousuario The rUsuarioTipousuario object to remove.
     * @return Usuario The current object (for fluent API support)
     */
    public function removeRUsuarioTipousuario($rUsuarioTipousuario)
    {
        if ($this->getRUsuarioTipousuarios()->contains($rUsuarioTipousuario)) {
            $this->collRUsuarioTipousuarios->remove($this->collRUsuarioTipousuarios->search($rUsuarioTipousuario));
            if (null === $this->rUsuarioTipousuariosScheduledForDeletion) {
                $this->rUsuarioTipousuariosScheduledForDeletion = clone $this->collRUsuarioTipousuarios;
                $this->rUsuarioTipousuariosScheduledForDeletion->clear();
            }
            $this->rUsuarioTipousuariosScheduledForDeletion[]= clone $rUsuarioTipousuario;
            $rUsuarioTipousuario->setUsuario(null);
        }

        return $this;
    }


    /**
     * If this collection has already been initialized with
     * an identical criteria, it returns the collection.
     * Otherwise if this Usuario is new, it will return
     * an empty collection; or if this Usuario has previously
     * been saved, it will retrieve related RUsuarioTipousuarios from storage.
     *
     * This method is protected by default in order to keep the public
     * api reasonable.  You can provide public methods for those you
     * actually need in Usuario.
     *
     * @param Criteria $criteria optional Criteria object to narrow the query
     * @param PropelPDO $con optional connection object
     * @param string $join_behavior optional join type to use (defaults to Criteria::LEFT_JOIN)
     * @return PropelObjectCollection|RUsuarioTipousuario[] List of RUsuarioTipousuario objects
     */
    public function getRUsuarioTipousuariosJoinTipoUsuario($criteria = null, $con = null, $join_behavior = Criteria::LEFT_JOIN)
    {
        $query = RUsuarioTipousuarioQuery::create(null, $criteria);
        $query->joinWith('TipoUsuario', $join_behavior);

        return $this->getRUsuarioTipousuarios($query, $con);
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->id = null;
        $this->username = null;
        $this->password = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volume/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;
            if ($this->collRUsuarioComercios) {
                foreach ($this->collRUsuarioComercios as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRUsuarioProductos) {
                foreach ($this->collRUsuarioProductos as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRUsuarioProveedors) {
                foreach ($this->collRUsuarioProveedors as $o) {
                    $o->clearAllReferences($deep);
                }
            }
            if ($this->collRUsuarioTipousuarios) {
                foreach ($this->collRUsuarioTipousuarios as $o) {
                    $o->clearAllReferences($deep);
                }
            }

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

        if ($this->collRUsuarioComercios instanceof PropelCollection) {
            $this->collRUsuarioComercios->clearIterator();
        }
        $this->collRUsuarioComercios = null;
        if ($this->collRUsuarioProductos instanceof PropelCollection) {
            $this->collRUsuarioProductos->clearIterator();
        }
        $this->collRUsuarioProductos = null;
        if ($this->collRUsuarioProveedors instanceof PropelCollection) {
            $this->collRUsuarioProveedors->clearIterator();
        }
        $this->collRUsuarioProveedors = null;
        if ($this->collRUsuarioTipousuarios instanceof PropelCollection) {
            $this->collRUsuarioTipousuarios->clearIterator();
        }
        $this->collRUsuarioTipousuarios = null;
    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(UsuarioPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
