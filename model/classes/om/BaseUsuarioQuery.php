<?php


/**
 * Base class that represents a query for the 'usuario' table.
 *
 *
 *
 * @method UsuarioQuery orderById($order = Criteria::ASC) Order by the id column
 * @method UsuarioQuery orderByUsername($order = Criteria::ASC) Order by the username column
 * @method UsuarioQuery orderByPassword($order = Criteria::ASC) Order by the password column
 *
 * @method UsuarioQuery groupById() Group by the id column
 * @method UsuarioQuery groupByUsername() Group by the username column
 * @method UsuarioQuery groupByPassword() Group by the password column
 *
 * @method UsuarioQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method UsuarioQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method UsuarioQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method UsuarioQuery leftJoinRUsuarioComercio($relationAlias = null) Adds a LEFT JOIN clause to the query using the RUsuarioComercio relation
 * @method UsuarioQuery rightJoinRUsuarioComercio($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RUsuarioComercio relation
 * @method UsuarioQuery innerJoinRUsuarioComercio($relationAlias = null) Adds a INNER JOIN clause to the query using the RUsuarioComercio relation
 *
 * @method UsuarioQuery leftJoinRUsuarioProducto($relationAlias = null) Adds a LEFT JOIN clause to the query using the RUsuarioProducto relation
 * @method UsuarioQuery rightJoinRUsuarioProducto($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RUsuarioProducto relation
 * @method UsuarioQuery innerJoinRUsuarioProducto($relationAlias = null) Adds a INNER JOIN clause to the query using the RUsuarioProducto relation
 *
 * @method UsuarioQuery leftJoinRUsuarioProveedor($relationAlias = null) Adds a LEFT JOIN clause to the query using the RUsuarioProveedor relation
 * @method UsuarioQuery rightJoinRUsuarioProveedor($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RUsuarioProveedor relation
 * @method UsuarioQuery innerJoinRUsuarioProveedor($relationAlias = null) Adds a INNER JOIN clause to the query using the RUsuarioProveedor relation
 *
 * @method UsuarioQuery leftJoinRUsuarioTipousuario($relationAlias = null) Adds a LEFT JOIN clause to the query using the RUsuarioTipousuario relation
 * @method UsuarioQuery rightJoinRUsuarioTipousuario($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RUsuarioTipousuario relation
 * @method UsuarioQuery innerJoinRUsuarioTipousuario($relationAlias = null) Adds a INNER JOIN clause to the query using the RUsuarioTipousuario relation
 *
 * @method Usuario findOne(PropelPDO $con = null) Return the first Usuario matching the query
 * @method Usuario findOneOrCreate(PropelPDO $con = null) Return the first Usuario matching the query, or a new Usuario object populated from the query conditions when no match is found
 *
 * @method Usuario findOneByUsername(string $username) Return the first Usuario filtered by the username column
 * @method Usuario findOneByPassword(string $password) Return the first Usuario filtered by the password column
 *
 * @method array findById(int $id) Return Usuario objects filtered by the id column
 * @method array findByUsername(string $username) Return Usuario objects filtered by the username column
 * @method array findByPassword(string $password) Return Usuario objects filtered by the password column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseUsuarioQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseUsuarioQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'Usuario';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new UsuarioQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   UsuarioQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return UsuarioQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof UsuarioQuery) {
            return $criteria;
        }
        $query = new UsuarioQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Usuario|Usuario[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = UsuarioPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(UsuarioPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Usuario A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Usuario A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `username`, `password` FROM `usuario` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Usuario();
            $obj->hydrate($row);
            UsuarioPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Usuario|Usuario[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Usuario[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return UsuarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(UsuarioPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return UsuarioQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(UsuarioPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UsuarioQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(UsuarioPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(UsuarioPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(UsuarioPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the username column
     *
     * Example usage:
     * <code>
     * $query->filterByUsername('fooValue');   // WHERE username = 'fooValue'
     * $query->filterByUsername('%fooValue%'); // WHERE username LIKE '%fooValue%'
     * </code>
     *
     * @param     string $username The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UsuarioQuery The current query, for fluid interface
     */
    public function filterByUsername($username = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($username)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $username)) {
                $username = str_replace('*', '%', $username);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UsuarioPeer::USERNAME, $username, $comparison);
    }

    /**
     * Filter the query on the password column
     *
     * Example usage:
     * <code>
     * $query->filterByPassword('fooValue');   // WHERE password = 'fooValue'
     * $query->filterByPassword('%fooValue%'); // WHERE password LIKE '%fooValue%'
     * </code>
     *
     * @param     string $password The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return UsuarioQuery The current query, for fluid interface
     */
    public function filterByPassword($password = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($password)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $password)) {
                $password = str_replace('*', '%', $password);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(UsuarioPeer::PASSWORD, $password, $comparison);
    }

    /**
     * Filter the query by a related RUsuarioComercio object
     *
     * @param   RUsuarioComercio|PropelObjectCollection $rUsuarioComercio  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UsuarioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRUsuarioComercio($rUsuarioComercio, $comparison = null)
    {
        if ($rUsuarioComercio instanceof RUsuarioComercio) {
            return $this
                ->addUsingAlias(UsuarioPeer::ID, $rUsuarioComercio->getUsuarioId(), $comparison);
        } elseif ($rUsuarioComercio instanceof PropelObjectCollection) {
            return $this
                ->useRUsuarioComercioQuery()
                ->filterByPrimaryKeys($rUsuarioComercio->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRUsuarioComercio() only accepts arguments of type RUsuarioComercio or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RUsuarioComercio relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UsuarioQuery The current query, for fluid interface
     */
    public function joinRUsuarioComercio($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RUsuarioComercio');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RUsuarioComercio');
        }

        return $this;
    }

    /**
     * Use the RUsuarioComercio relation RUsuarioComercio object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RUsuarioComercioQuery A secondary query class using the current class as primary query
     */
    public function useRUsuarioComercioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRUsuarioComercio($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RUsuarioComercio', 'RUsuarioComercioQuery');
    }

    /**
     * Filter the query by a related RUsuarioProducto object
     *
     * @param   RUsuarioProducto|PropelObjectCollection $rUsuarioProducto  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UsuarioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRUsuarioProducto($rUsuarioProducto, $comparison = null)
    {
        if ($rUsuarioProducto instanceof RUsuarioProducto) {
            return $this
                ->addUsingAlias(UsuarioPeer::ID, $rUsuarioProducto->getUsuarioId(), $comparison);
        } elseif ($rUsuarioProducto instanceof PropelObjectCollection) {
            return $this
                ->useRUsuarioProductoQuery()
                ->filterByPrimaryKeys($rUsuarioProducto->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRUsuarioProducto() only accepts arguments of type RUsuarioProducto or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RUsuarioProducto relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UsuarioQuery The current query, for fluid interface
     */
    public function joinRUsuarioProducto($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RUsuarioProducto');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RUsuarioProducto');
        }

        return $this;
    }

    /**
     * Use the RUsuarioProducto relation RUsuarioProducto object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RUsuarioProductoQuery A secondary query class using the current class as primary query
     */
    public function useRUsuarioProductoQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRUsuarioProducto($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RUsuarioProducto', 'RUsuarioProductoQuery');
    }

    /**
     * Filter the query by a related RUsuarioProveedor object
     *
     * @param   RUsuarioProveedor|PropelObjectCollection $rUsuarioProveedor  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UsuarioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRUsuarioProveedor($rUsuarioProveedor, $comparison = null)
    {
        if ($rUsuarioProveedor instanceof RUsuarioProveedor) {
            return $this
                ->addUsingAlias(UsuarioPeer::ID, $rUsuarioProveedor->getUsuarioId(), $comparison);
        } elseif ($rUsuarioProveedor instanceof PropelObjectCollection) {
            return $this
                ->useRUsuarioProveedorQuery()
                ->filterByPrimaryKeys($rUsuarioProveedor->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRUsuarioProveedor() only accepts arguments of type RUsuarioProveedor or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RUsuarioProveedor relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UsuarioQuery The current query, for fluid interface
     */
    public function joinRUsuarioProveedor($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RUsuarioProveedor');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RUsuarioProveedor');
        }

        return $this;
    }

    /**
     * Use the RUsuarioProveedor relation RUsuarioProveedor object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RUsuarioProveedorQuery A secondary query class using the current class as primary query
     */
    public function useRUsuarioProveedorQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRUsuarioProveedor($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RUsuarioProveedor', 'RUsuarioProveedorQuery');
    }

    /**
     * Filter the query by a related RUsuarioTipousuario object
     *
     * @param   RUsuarioTipousuario|PropelObjectCollection $rUsuarioTipousuario  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 UsuarioQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRUsuarioTipousuario($rUsuarioTipousuario, $comparison = null)
    {
        if ($rUsuarioTipousuario instanceof RUsuarioTipousuario) {
            return $this
                ->addUsingAlias(UsuarioPeer::ID, $rUsuarioTipousuario->getUsuarioId(), $comparison);
        } elseif ($rUsuarioTipousuario instanceof PropelObjectCollection) {
            return $this
                ->useRUsuarioTipousuarioQuery()
                ->filterByPrimaryKeys($rUsuarioTipousuario->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRUsuarioTipousuario() only accepts arguments of type RUsuarioTipousuario or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RUsuarioTipousuario relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return UsuarioQuery The current query, for fluid interface
     */
    public function joinRUsuarioTipousuario($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RUsuarioTipousuario');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RUsuarioTipousuario');
        }

        return $this;
    }

    /**
     * Use the RUsuarioTipousuario relation RUsuarioTipousuario object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RUsuarioTipousuarioQuery A secondary query class using the current class as primary query
     */
    public function useRUsuarioTipousuarioQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRUsuarioTipousuario($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RUsuarioTipousuario', 'RUsuarioTipousuarioQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   Usuario $usuario Object to remove from the list of results
     *
     * @return UsuarioQuery The current query, for fluid interface
     */
    public function prune($usuario = null)
    {
        if ($usuario) {
            $this->addUsingAlias(UsuarioPeer::ID, $usuario->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
