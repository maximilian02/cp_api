<?php


/**
 * Base class that represents a query for the 'zona_entrega' table.
 *
 *
 *
 * @method ZonaEntregaQuery orderById($order = Criteria::ASC) Order by the id column
 * @method ZonaEntregaQuery orderByNombre($order = Criteria::ASC) Order by the nombre column
 *
 * @method ZonaEntregaQuery groupById() Group by the id column
 * @method ZonaEntregaQuery groupByNombre() Group by the nombre column
 *
 * @method ZonaEntregaQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ZonaEntregaQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ZonaEntregaQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ZonaEntregaQuery leftJoinRProveedorZonaentrega($relationAlias = null) Adds a LEFT JOIN clause to the query using the RProveedorZonaentrega relation
 * @method ZonaEntregaQuery rightJoinRProveedorZonaentrega($relationAlias = null) Adds a RIGHT JOIN clause to the query using the RProveedorZonaentrega relation
 * @method ZonaEntregaQuery innerJoinRProveedorZonaentrega($relationAlias = null) Adds a INNER JOIN clause to the query using the RProveedorZonaentrega relation
 *
 * @method ZonaEntrega findOne(PropelPDO $con = null) Return the first ZonaEntrega matching the query
 * @method ZonaEntrega findOneOrCreate(PropelPDO $con = null) Return the first ZonaEntrega matching the query, or a new ZonaEntrega object populated from the query conditions when no match is found
 *
 * @method ZonaEntrega findOneByNombre(string $nombre) Return the first ZonaEntrega filtered by the nombre column
 *
 * @method array findById(int $id) Return ZonaEntrega objects filtered by the id column
 * @method array findByNombre(string $nombre) Return ZonaEntrega objects filtered by the nombre column
 *
 * @package    propel.generator.cp.om
 */
abstract class BaseZonaEntregaQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseZonaEntregaQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = null, $modelName = null, $modelAlias = null)
    {
        if (null === $dbName) {
            $dbName = 'cp';
        }
        if (null === $modelName) {
            $modelName = 'ZonaEntrega';
        }
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ZonaEntregaQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ZonaEntregaQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ZonaEntregaQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ZonaEntregaQuery) {
            return $criteria;
        }
        $query = new ZonaEntregaQuery(null, null, $modelAlias);

        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ZonaEntrega|ZonaEntrega[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ZonaEntregaPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ZonaEntregaPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ZonaEntrega A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneById($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ZonaEntrega A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT `id`, `nombre` FROM `zona_entrega` WHERE `id` = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ZonaEntrega();
            $obj->hydrate($row);
            ZonaEntregaPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ZonaEntrega|ZonaEntrega[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ZonaEntrega[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ZonaEntregaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ZonaEntregaPeer::ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ZonaEntregaQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ZonaEntregaPeer::ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id >= 12
     * $query->filterById(array('max' => 12)); // WHERE id <= 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ZonaEntregaQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(ZonaEntregaPeer::ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(ZonaEntregaPeer::ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ZonaEntregaPeer::ID, $id, $comparison);
    }

    /**
     * Filter the query on the nombre column
     *
     * Example usage:
     * <code>
     * $query->filterByNombre('fooValue');   // WHERE nombre = 'fooValue'
     * $query->filterByNombre('%fooValue%'); // WHERE nombre LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nombre The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ZonaEntregaQuery The current query, for fluid interface
     */
    public function filterByNombre($nombre = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nombre)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nombre)) {
                $nombre = str_replace('*', '%', $nombre);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ZonaEntregaPeer::NOMBRE, $nombre, $comparison);
    }

    /**
     * Filter the query by a related RProveedorZonaentrega object
     *
     * @param   RProveedorZonaentrega|PropelObjectCollection $rProveedorZonaentrega  the related object to use as filter
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return                 ZonaEntregaQuery The current query, for fluid interface
     * @throws PropelException - if the provided filter is invalid.
     */
    public function filterByRProveedorZonaentrega($rProveedorZonaentrega, $comparison = null)
    {
        if ($rProveedorZonaentrega instanceof RProveedorZonaentrega) {
            return $this
                ->addUsingAlias(ZonaEntregaPeer::ID, $rProveedorZonaentrega->getZonaEntregaId(), $comparison);
        } elseif ($rProveedorZonaentrega instanceof PropelObjectCollection) {
            return $this
                ->useRProveedorZonaentregaQuery()
                ->filterByPrimaryKeys($rProveedorZonaentrega->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByRProveedorZonaentrega() only accepts arguments of type RProveedorZonaentrega or PropelCollection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the RProveedorZonaentrega relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return ZonaEntregaQuery The current query, for fluid interface
     */
    public function joinRProveedorZonaentrega($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('RProveedorZonaentrega');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'RProveedorZonaentrega');
        }

        return $this;
    }

    /**
     * Use the RProveedorZonaentrega relation RProveedorZonaentrega object
     *
     * @see       useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return   RProveedorZonaentregaQuery A secondary query class using the current class as primary query
     */
    public function useRProveedorZonaentregaQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinRProveedorZonaentrega($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'RProveedorZonaentrega', 'RProveedorZonaentregaQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ZonaEntrega $zonaEntrega Object to remove from the list of results
     *
     * @return ZonaEntregaQuery The current query, for fluid interface
     */
    public function prune($zonaEntrega = null)
    {
        if ($zonaEntrega) {
            $this->addUsingAlias(ZonaEntregaPeer::ID, $zonaEntrega->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
