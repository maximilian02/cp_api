<?php
// Include the main Propel script
require_once 'propel/Propel.php';
 
// Initialize Propel with the runtime configuration
Propel::init("model/conf/cp-conf.php");
 
// Add the generated 'classes' directory to the include path
set_include_path("model/classes" . PATH_SEPARATOR . get_include_path());
?>